/**
 * This class is the controller for the main view for bravo application.
 *
 */
Ext.define('bravo.MainController.js', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.bravo-main',

	/*
        Must include all bravo classes here for them to be imported.  Do not include /view/
        in the classpath.
    */
    requires: [
        
        'bravo.global.Actions',
        'bravo.global.Cache',
        'bravo.global.Data',
        'bravo.global.Encryption',
        'bravo.global.Entity',
        'bravo.global.Environment',
        'bravo.global.Native',
        'bravo.global.Rules',
        'bravo.global.Feedback',
        'bravo.global.Render',
        'bravo.global.Server',
        'bravo.global.Timer',
        'bravo.global.Util',
        'bravo.Dashboard',
        'bravo.Selector',
        
        
        //'bravo.form.Checkbox',
        'bravo.form.Combobox',
        'bravo.form.Datefield',
        'bravo.form.Displayfield',
        //'bravo.form.Fieldset',
        //'bravo.form.Form',
        'bravo.form.Radio',
        'bravo.form.Textfield',
        'bravo.form.Textarea',
        'bravo.form.Timefield',
        //'bravo.form.Upload',
        
        'bravo.layout.Button',
        'bravo.layout.Container',
        'bravo.layout.Dataview',
        'bravo.layout.ComboNavigator',
        //'bravo.layout.Grid',
        'bravo.layout.Panel',
        //'bravo.layout.Tab',
        //'bravo.layout.Tabpanel',
        'bravo.layout.Template',
        'bravo.layout.Thumbstrip',
        'bravo.layout.Toolbar',
        'bravo.layout.ToolbarSpacer',
        'bravo.layout.ToolbarTextItem',
    
        //'bravo.layout.Window',
        
        'bravo.widget.Barcode',
        'bravo.widget.Loginpanel'
       // 'bravo.widget.Signaturepad'
        
    ],


    init: function(application) {
    
        //Helpers
        this.build([
            'controller.actions',
            'controller.cache',
            'controller.data',
            'controller.rules',
            'controller.encryption',
            'controller.entity',
            'controller.environment',
            'controller.feedback',
            'controller.wrapper',
            'controller.render',
            'controller.server',
            'controller.timer',
            'controller.util',
            'custom'		
        ]);
        
        //Listeners
        /*
        Document Listeners needed for: click,resume,online,offline,
        Window: readerconnected,readerdisconnected,readergavetrackstring
        Fileselector.dom: change,dragover,drop
        TextField.field.el: keypress
        Chooser.dom: dragover, drop
        */
        
        
        //Hyperlinks
        //document.addEventListener('click', Ext.bind(this.hrefHandler,this));
        //TODO: Move this to only apply to text fields and text area fields so that this is ignored for all other components.
    
        //Component events
        this.control({
            //Classic
            "xbutton": {
                click: this.buttonHandler,
                tap: this.buttonHandler
            },
            "xgrid": {
                select: this.gridHandler
            },
            "xcombobox": {
                change: this.comboboxHandler
            },
            "xcombonavigator": {
                change: this.combonavigatorHandler
            }//,
            //"xdataview": {
                //itemclick: this.dataviewHandler
            //},
            //"xtextfield": {
                //blur: this.textfieldHandler
            //}
            //Modern
            /*
            "xlist": {
                itemtap: this.listHandler
            },
            "xselect": {
                change: this.selectHandler
            }
            */
        });
        
        //Boot -- App is booted after database is successfully initialized.
        Ext.defer(function() {this.boot()}, 1, this);
        
    },
    
    
    build: function (arr) {
        for (var i=0,l=arr.length;i<l;i++) {
            var classpath = arr[i];
            var classname = classpath.split('.')[1]||classpath;
            var obj = Ext.create(classpath,{main:this});
            this[classname] = obj;
        }
    },
    
    boot: function () {
    
        //Build the app environment.
    
        //Environment.
        this.environment.setup();
        
        //Encryption
        this.encryption.setup();
        
        //Native.
        this.wrapper.setup();
        
        //Data
        this.data.setup();
        
        //Cache
        this.cache.setup();
        
        //Events
        this.rules.setup();
        
        //Entity
        this.entity.setup();
        
        //Idle timer.
        this.timer.setup(this.environment.get("IDLE_SECONDS"));
        
        //Main
        this.setup();
        
           
        this.view.getEl().on("swipe",function(ev,el,options,eOpts) {
            if ((ev.distance>250)&&(ev.direction=="right")&&(ev.touch.pageY<70)) {
                this.actions.perform({cmd:"dashboard"});
            }
        },this);
    },
    
    setup: function () {
    
    
    },
    
    //-------------------------------------------------------------------------------------
    // Listeners
    //-------------------------------------------------------------------------------------
    /* Listens for specific events from all components in the view stack and responds accordingly. */
    
    //Bravo internal actions
    actionHandler: function (sp) {
        try {
            if (!sp.cmd) {
                console.log('missing action directive.');
                return;
            }
            this.actions.perform(sp);
        } catch (e) {
            //debugger;
        }
    },
    
    //Grids
    //TODO: move to grid component.
    gridHandler: function (sel,rec) {
        //grids are different in classic and modern.  Need to account for that.
        var cmp = (sel.view) ? sel.view.grid : sel;
        this.actions.perform(Ext.apply({},{
            component: cmp,
            rec: rec,
            cmd: cmp.action,
            target: this.util.get(cmp.target),
            window: this.util.get(cmp.window)
        }));
    },
    

    //Buttons
    //TODO: move to button component
    buttonHandler: function (cmp,ev) {
    
        //Data provider
        //if (cmp.provider) this.rules.setProvider(cmp.provider,cmp.alias);
        
        //Perform action for this button.
        var sp = this.actions.getSettingsProvider(cmp);
        
        if (cmp.action) {
            this.actions.perform(Ext.apply(sp,{
                component: cmp,
                cmd: cmp.action
            }));
           
           
           console.log("clicked button " + cmp.l + " and did action: " + cmp.action);
        }
        
        
        
        //Broadcast events.
        this.rules.broadcast({
            component: cmp
        });
    },
    
    
    //Combobox
    //TODO: move to combobox component
    comboboxHandler: function (cmp,nv,ov) {
        //This function is called anytime a combobox/select item is changed.
        
        //Data provider
        //if (cmp.provider) this.rules.setProvider(cmp.provider,nv);
        
        //Action
        if (cmp.action) {
            this.actions.perform(Ext.apply(this.actions.getSettingsProvider(cmp),{
                component: cmp,
                cmd: cmp.action,
                n: nv,
                rec: {
                    data: {
                        old: ov,
                        value: nv
                    }
                }
            }));
           
            console.log("chose item " + nv + " on combobox " + cmp.l + " and did action: " + cmp.action);
        }
           
    
        //Broadcast events.
        this.rules.broadcast({
            component: cmp
        });
    },
    
    //ComboNavigator
    //TODO: Move to combonavigator component
    combonavigatorHandler: function (cmp,nv,ov) {
        //This function is called anytime a combonavigator/select item is changed.
        
        //Action
        if (cmp.action) {
            this.actions.perform(Ext.apply(this.actions.getSettingsProvider(cmp),{
                component: cmp,
                cmd: cmp.action,
                n: nv,
                rec: {
                    data: {
                        old: ov,
                        value: nv
                    }
                }
            }));
        }
           
        //Broadcast events.
        this.rules.broadcast({
            component: cmp,
            provider: nv
        });
    }
    
    //Textfields
    //Moved to textfield component.
    /*
    textfieldHandler: function (cmp) {
        //This function is called anytime a textfield loses focus.  Might need to change to buffered key handler.
        
        //Action: Data change only if there is a provider and the value is valid.
        if ((cmp.provider)&&(cmp.isValid())) {
            var prv = this.data.providers[cmp.provider];
            var o = this.data.getObject(prv);
            if (o) {
                var sp = {
                    cmd: 'put',
                    objects: [o]
                };
                if (cmp.n) sp[cmp.n] = cmp.getValue();
                this.actions.perform(sp);
            }
        }
        
        //Broadcast events.
        this.rules.broadcast({
            component: cmp
        });
    }*/
});
