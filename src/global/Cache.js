/**
 * This class is a helper class for the MainController.
 *
 * Maintains all functions related to iDB and the main data cache.
 */

Ext.define('bravo.global.Cache', {
    extend: 'Ext.app.Controller',

    alias: 'controller.cache',
    
    requires: [
        
    ],
    
    
    setup: function () {
        //Setup IndexedDB and object stores
        window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
        window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
        window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
        
        var me = this;
        
        request = window.indexedDB.open("Bravo",this.main.environment.get("DATABASE_VERSION"));
        
        
        request.onerror = function(event) {
            //debugger;
            alert("IndexedDB not supported.  Please contact support.");
        };

        request.onupgradeneeded = function(event) {
            me.db = event.target.result;
           
            //console.log("clearing database.");
           
            //Data: Stores latest versions of all objects for all read/write operations
            if (me.db.objectStoreNames.contains("data")) {
                me.db.deleteObjectStore("data");
            }
            var data = me.db.createObjectStore("data", { keyPath: "a" });
            data.createIndex("parent", "p", { unique: false });
            data.createIndex("classid", "cid", { unique: false });
            data.createIndex("name", "n", { unique: false });
            data.createIndex("ca","ca");
           
           
            //Cache: Stores a record of requests so they can be recalled from iDB instead of server for offline/faster requests.
            if (me.db.objectStoreNames.contains("cache")) {
                me.db.deleteObjectStore("cache");
            }
            var cache = me.db.createObjectStore("cache", { keyPath : "key" });
            cache.createIndex("alias", "a",{unique: false});
            cache.createIndex("timestamp", "timestamp",{unique:false});
           
           
            //Queue: Stores an original of any object that is changed and not committed to server.  Once
            //successfully committed, this version is deleted.  Used for rollbacks to original state and comparison when changes
            //are written to the server.
            if (me.db.objectStoreNames.contains("queue")) {
                me.db.deleteObjectStore("queue");
            }
            var queue = me.db.createObjectStore("queue", { keyPath: "aq" });
           
           
            //Log: Tracks all actions of the user.  They are dumped on every interaction with the server and scrubbed.
            if (me.db.objectStoreNames.contains("log")) {
                me.db.deleteObjectStore("log");
            }
            var log = me.db.createObjectStore("log", { keyPath : "key" });

        };
        
        
        request.onsuccess = function(event) {
        
            if (me.main.environment.get("cache")=="destroy") {
                var request = window.indexedDB.deleteDatabase("Bravo");
            }
           
            me.db = event.target.result;
           
            me.db.onerror = function (event) {
                //debugger;
                console.log("Database error: " + event.target.error.message);
            }
           
            //Startup options.
            //Deferred to give request.onupgradeneeded a chance to fire before proceeding.
            Ext.defer(function() {
                if (me.main.environment.get("cache")==="reset") me.reset();
                me.boot();
            }, 100, this);
        };
        
    },
    
    
    
    //--------------------------------------
    // Read data from iDB
    //--------------------------------------
    
    boot: function () {
    
        //Boot: Request the default window which should be the complete user interface for the app.
        //Use 0 for cache so that if the interface has been loaded at all it will only pull it from the cache.
        //Then subsequent requests for UI elements and pages as the cache expires will keep the UI fresh.
        
        //App is ready to run.  Load the initial item.
        this.main.actions.perform({
            cmd:"request",
            n:this.main.environment.get("DEFAULT_WINDOW"),
            relationship: "descendants"
        });
        

    },
    
    get: function (sp) {
        //Request for data from the cache.
        
        
        if ((sp.datasource=="server")&&(this.main.environment.getOnline())) {
        
            //Request from server only.
           
            //datasource = server (do not use cache), cache (do not use server), default = use both.
            //cache=# (in seconds) for age to use it. 0 = ignore expiration and load everything.
           
            //Skip cache and request from server.
            this.buildRequest(sp);
           
        } else {
        
            //Request from cache/server combination.
        
            try {
            
                //Pull any item in the db that starts with the main key.
                var key = this.buildCacheKey(sp,this.buildParamString(sp));
                var keyRangeValue = IDBKeyRange.bound(key, key + '\uffff');
               
                var cacheTransaction = this.db.transaction(["cache"], "readonly");
                var cacheObjectStore = cacheTransaction.objectStore("cache");
                
                var arr = [];
                var arr2 = [];
                var me = this;
                var cacheseconds;
    
                //Determine expiration date.
                if (Ext.isDefined(sp.cache)) {
                    cacheseconds = sp.cache;
                } else {
                    cacheseconds = this.main.environment.get("CACHE_SECONDS")||30;
                }
                var expires = new Date().valueOf() - (1000*(cacheseconds));
           
                //Ignore expiration if offline.
                if (!this.main.environment.getOnline()) sp.cache = 0;
           
           
                //Get data from the cache.
                var request = cacheObjectStore.openCursor(keyRangeValue).onsuccess = function(event) {
                    var cursor = event.target.result;

                    if (cursor) {
                        //Timestamp = when item was cached. Expired = time in past.  Once timestamp greater than expired it needs to be reloaded if possible.
                        if ((sp.cache==0)||(cursor.value.timestamp>expires)) {
           
                            //Log that you are pulling data out of the cache.
                            me.log(Ext.apply(cursor,{
                                cmd: 'cached'
                            }));
                            arr.push(cursor.value.a);
                            arr2[cursor.value.idx] = cursor.value.a;
               
                            //Role assignment from cache requests.
                            me.main.data.roles[cursor.value.a] = cursor.value.role;
                        }
                        //TODO: add a feature to idle that removes expired items.
                        cursor['continue']();
                        //cursor['continue'].call();
                    } else {
                        if ((arr.length)||(sp.datasource=='cache')) {
                            //Get data from cache.
                            me.getData(sp,arr,arr2);
                        } else {
                            //No data found and datasource is anything except "cache" Get data from server.
                            //console.log("b:" + new Date());
                            if (me.main.environment.getOnline()) {
                                me.buildRequest(sp);
                            } else {
                                Ext.callback(sp.callback,sp.scope,[]);
                            }
                        }
                   
                    }
                };
            } catch (e) {
                //console.log("error accessing iDB - go to server instead.");
                //console.log("c:" + new Date());
           
                if (this.main.environment.getOnline()) {
                    //Online= Request from server
                    this.buildRequest(sp);
                } else {
                    //Error occurred and offline = must just return to callback with no data.
                    Ext.callback(sp.callback,sp.scope,[]);
                }
            }
        }
    },
    
    getData: function (sp,arr,arr2) {
    
        //All of the indexedDB interaction is sorted by alias for retrieval.  A second array preserves the sort order of
        //the original request.  At the end of the retrieval from the database, the array is resorted into this order.
        
        
        var objects = [];
        var lookup = {};
        var me = this;
        
        var dataTransaction = this.db.transaction(["data"],"readonly");
        var dataObjectStore = dataTransaction.objectStore("data");
        
        dataObjectStore.onerror = function (event) {
            //console.log("error in getData. Go to prerender");
            me.main.data.restore(objects,sp);
        }
        
        arr.sort();
        var x = 0;
        
        var request = dataObjectStore.openCursor().onsuccess = function (event) {
            var cursor = event.target.result;
            if (cursor) {
                var key = cursor.key;
                if (x<arr.length) {
                    while (key > arr[x]) {
                        ++x;
                        if (x==arr.length) {
                            //end of arr reached.
                            //console.log("getData: last item of arr - don't do anything");
                            //me.main.data.restore(objects,sp);
                            //this is currently disabled.  Theory is that the no cursor at the bottom is going
                            //to fire no matter if there were items found in the cache or not, so it should be
                            //the only one to fire the exit.
                            break;
                        }
                    }
                    if (key==arr[x]) {
                        lookup[cursor.key] = cursor.value;
                        cursor['continue']();
                    } else {
                        if (arr[x]) {
                            cursor['continue'](arr[x]);
                        } else {
                            cursor['continue']();
                        }
                    }
                } else {
                    //end of arr reached.
                    //console.log("getData: end of arr 2 - does it hit this point everytime?");
                    for (var i=0,l=arr2.length;i<l;i++) {
                        objects[i] = lookup[me.decrypt(arr2[i])];
                    }
                    me.main.data.restore(objects,sp);
           
                    //this is currently enabled.  Need to check if it is still duplicating sometimes but it is definitely necessary.
                    //this is currently disabled.  Theory is that the no cursor at the bottom is going
                    //to fire no matter if there were items found in the cache or not, so it should be
                    //the only one to fire the exit.
                }
            } else {
                //end of cursor reached. Either nothing was found or at the end of the set.
                //duplicating after end of arr reached is fired.
                //console.log("getData: no cursor - go to prerender");
                for (var i=0,l=arr2.length;i<l;i++) {
                    objects[i] = lookup[me.decrypt(arr2[i])];
                }
                me.main.data.restore(objects,sp);
            }
        }
    
    },
    
    decrypt: function (o) {
        return (this.main.encryption.doEncryption(o.cid)) ? this.main.encryption.decrypt(o) : o;
        //return o;
    },
    
    
    //--------------------------------------
    // Manage iDB
    //--------------------------------------
    
    expire: function (sp) {
    
        //Removes items from the cache that are expired.  They are already skipped by retrieval, so this is just a cleanup.
        //disabled temporarily.
        /*

        var transaction = this.db.transaction(["cache"], "readwrite");
        var objectStore = transaction.objectStore("cache");
        var tsIndex = objectStore.index("timestamp");
        var expires = new Date().valueOf() - (1000*this.main.environment.get("CACHE_SECONDS"));
        var keyRangeValue = IDBKeyRange.bound(0,expires);
        
        console.log("manage cache " + new Date().valueOf() - (1000*this.main.environment.get("CACHE_SECONDS")));
        
        var request = tsIndex.openCursor(keyRangeValue).onsuccess = function(event) {
            var cursor = event.target.result;
            if (cursor) {
                console.log('removed ' + cursor.primaryKey + ' ' + cursor.value.timestamp);
                //objectStore['delete'](cursor.primaryKey);
           
                cursor['continue']();
            }
        }
        */
        
    
    },
    
    invalidate: function (sp) {
    
        //Removes all items from the cache that were loaded by requesting alias of specific item.
        var transaction = this.db.transaction(["cache"], "readwrite");
        var cacheObjectStore = transaction.objectStore("cache");
        
        var key = sp.a;
        var keyRangeValue = IDBKeyRange.bound(key, key + '\uffff');
        
        var request = cacheObjectStore.openCursor(keyRangeValue).onsuccess = function(event) {
            var cursor = event.target.result;
            if (cursor) {
                //cacheObjectStore['delete'].call(cursor.primaryKey);
                cacheObjectStore['delete'](cursor.primaryKey);
                cursor['continue']();
            }
        }
    },
    
    
    /* Possible removal.  Harvesting role assignments from requests into RAM so they can be used immediately.
    getPermission: function (sp) {
    
        //Lookup this object from all versions in the cache no matter
        //which user retrieved it.  Get the specific one loaded by this user
        //and get the role.
        var me = this;
        
        var transaction = this.db.transaction(["cache"], "readonly");
        var cacheObjectStore = transaction.objectStore("cache");
        var aliasIndex = cacheObjectStore.index("alias");
        var key = sp.a;
        var keyRangeValue = IDBKeyRange.only(key);
        var request = aliasIndex.openCursor(keyRangeValue).onsuccess = function(event) {
            var cursor = event.target.result;
            if (cursor) {
                if (cursor.value.userid==me.main.entity.getUser().i) {
                    Ext.callback(sp.callback,sp.scope,[cursor.value]);
                } else {
                    cursor['continue']();
                }
            } else {
                Ext.callback(sp.callback,sp.scope,[{role: 2}]);
            }
        }
    },
    */

    reset: function (sp) {
        this.resetStore("cache");
        this.resetStore("data");
        this.resetStore("queue");
    },
        
    resetStore: function (s) {
        var transaction = this.db.transaction([s], "readwrite");
        var objectStore = transaction.objectStore(s);
    
        var request = objectStore.openCursor().onsuccess = function(event) {
            var cursor = event.target.result;
            if (cursor) {
                //objectStore.delete(cursor.primaryKey);
                objectStore['delete'](cursor.primaryKey);
                cursor['continue']();
                //cursor['continue'].call();
            }
        }
    },
    
    
    
    //--------------------------------------
    // Write Data to iDB
    //--------------------------------------
    
    
    store: function (objects,sp) {
    
        var me = this;
    
        /* 
        store: This function runs any time a new object is read from the database.  New items created internally will be
        added using the put fuction. It is supposed to put data into two iDB object stores: data and cache.  
        Cache contains a key that represents a group of items loaded as a group and shows which items were 
        cached at the same time  The data represents the actual object
        and the real data that is used when reading items from the cache.
        */
        
        //Currently need to force ALL data to be cached because it is required for making changes.
        //Theory is that either the cache or the server is always up-to-date.
        //Better solution is to use invalidate so that data objectstore is always active.
        //If there are storage problems, need a way to purge the cache silently.
        //if (sp.cache>0) {
    
            //Objects coming from the server requests are put into objectstores.
            if (Ext.isArray(objects)) {
                if (objects.length>0) {
            
                    //Timestamp
                    var ts = new Date().valueOf();
                    
                    var arr = [];
                    
                    var empty = true;
                    
                    //Data
                    var dataTransaction = this.db.transaction(["data"], "readwrite");
                    var dataObjectStore = dataTransaction.objectStore("data");
                    
                    for (var i=0,l=objects.length;i<l;i++) {
                        //console.log("queue for storage: " + objects[i].a);
                        try {
                            if (objects[i].a) arr.push(objects[i]);
                        } catch (e) {
                            console.log(e.message);
                        }
                    }
               
                    var x = 0;
                    arr.sort(function (a,b) {
                        if (a.a < b.a) return -1;
                        if (a.a > b.a) return 1;
                        return 0;
                    });
                    
                    var keyRangeValue = IDBKeyRange.bound(arr[0].a, arr[arr.length-1].a);
               
                    var request = dataObjectStore.openCursor(keyRangeValue).onsuccess = function(event) {
                        var cursor = event.target.result;
                        if (cursor) {
                            empty = false;
                            for (var i=x,l=arr.length;i<l;i++) {
           
                                if (arr[i].a < cursor.key) {
                                    //New items with keys smaller than cursor.
                                    //Note: Firefox still has a constrainterror raised even though it is a put function.
                                    //console.log("new item with key smaller than first item in existing data " + arr[i].a);
                                    x++;
           
                                    dataObjectStore.put(me.encrypt(arr[i]));
                                    //console.log("storing object at new key " + arr[i].a);
                                } else if (arr[i].a===cursor.key) {
                                    x++;
                                    //Existing items with keys matching cursor.
                                    if ((arr[i].utc > cursor.value.utc)||(arr[i].utc==0)) {
                                        //console.log("storing object at existing key " + arr[i].a + " " + cursor.key);
                                        dataObjectStore.put(me.encrypt(arr[i]));
                                    } else {
                                        //console.log("skip storing object (same utc) " + arr[i].a + " " + cursor.key + " " + arr[i].utc + " " + cursor.value.utc);
                                    }
                                    break;  //Can move to next item.
                                } else {
                                    //currently disabled because nothing is supposed to happen here???
                                    //console.log(arr[i].a + " is > " + cursor.key + "= can move on to next item. Do nothing? Need to store something?");
                                    x++;
                                    //New item with a key that is greater than the one in the data. go ahead and store it.
               
           
                                    //dataObjectStore.put(arr[i]);
                                    if ((arr[i].utc > cursor.value.utc)||(arr[i].utc==0)) {
                                        //console.log("storing object at existing key " + arr[i].a + " " + cursor.key);
                                        dataObjectStore.put(me.encrypt(arr[i]));
                                    } else {
                                        //console.log("skip storing object (same utc) " + arr[i].a + " " + cursor.key + " " + arr[i].utc + " " + cursor.value.utc);
                                    }
               
                                    break;
                                }
                            }
                            cursor['continue']();
                        } else {
                            //end of cursor reached.
               
                            if (empty) {
                                //console.log("db is empty");
                                //All new items--no need to match.
                                for (var i=0,l=arr.length;i<l;i++) {
                                    //console.log("storing new item " + arr[i].a);
                                    try {
                                        //console.log("storing object at new key " + arr[i].a);
                                        dataObjectStore.put(me.encrypt(arr[i]));
                                    } catch(e) {
                                        //console.log(e.message);
                                    }
                                }
                            } else {
                                //end of cursor.  process any remaining items.
                                //console.log("storing objects after all existing keys");
                                for (var i=x,l=arr.length;i<l;i++) {
                                    //New items with keys greater than last cursor.
                                    //console.log("store new item " + arr[i].a);
                                    try {
                                        //console.log("storing object at new key " + arr[i].a);
                                        dataObjectStore.put(me.encrypt(arr[i]));
                                    } catch (e) {
                                        //debugger;
                                    }
                                }
                            }
                        }
                    }
               
                    //console.log("ready to do cache");

                    //Cache
                    var cacheTransaction = this.db.transaction(["cache"], "readwrite");
                    var cacheObjectStore = cacheTransaction.objectStore("cache");
                    
                    for (var i=0,l=objects.length;i<l;i++) {
                        var cobj = objects[i];
                        var k = [this.buildCacheKey(sp,this.buildParamString(sp)),cobj.a].join(".");
                        try {
                            var request = cacheObjectStore.put({
                                key: k,
                                a: cobj.a,
                                role: cobj.r,
                                timestamp: ts,
                                pa: cobj.pa,
                                userid: this.main.entity.getUser().i,
                                idx: i
                            });
                        } catch (e) {
                            console.log(e.message);
                        }
                    }
                }
            }
        //}
    },
    
    put: function (o) {
    
        /*
        put: This function adds any changes to the queue to be processed the next time a server request is made.
        It only adds an original version into the queue objectstore.  The real data change is stored in the data objectstore.
        Then when the request is made the data is compared to the original and only changes are sent to server.  If you want to cancel
        changes before sending to the server, this queue item can be removed.
        */
    
        //console.log("put");
    
 
        //Commit data changes from the application to the cache for offline and faster requests.
        var me = this;
        
        //Get item from data storage.
        var dataTransaction = this.db.transaction(["data"],"readwrite");
        var dataObjectStore = dataTransaction.objectStore("data");

        //Update the item in the data.
        var request = dataObjectStore.get(o.a).onsuccess = function(event) {
            var item = event.target.result;
            if (item) {
           
                //Does it exist already in the queue?
                var queueTransaction = me.db.transaction(["queue"], "readwrite");
                var queueObjectStore = queueTransaction.objectStore("queue");
                var queuerequest = queueObjectStore.get("queue"+"."+o.a).onsuccess = function(event) {
                    var queueitem = event.target.result;
                    if (!queueitem) {
                        //console.log("add to queue");
                        var obj = Ext.apply({},item);
                        obj.aq = "queue"+"."+o.a; //Work around for safari bug.  Cannot have two datastores with the same key.
                        queueObjectStore.put(me.encrypt(obj));
                    }
           
                    var now = new Date();
                    var utc = Date.UTC(now.getUTCFullYear(),now.getUTCMonth(), now.getUTCDate(),
                          now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
                    o.utc = utc;
       
                    //console.log("update data " + item.utc);
                    //Must re-establish transaction because the original one is closed.
                    me.db.transaction(["data"],"readwrite").objectStore("data").put(o);
        
                }
           
            } else {
                //console.log("new item added to the queue");
           
                var obj = Ext.apply({},o);
                obj.aq = "queue"+"."+o.a; //Work around for safari bug.  Cannot have two datastores with the same key.
           
                //Create new item.
                me.db.transaction(["data"],"readwrite").objectStore("data").put(me.encrypt(obj));
                me.db.transaction(["queue"],"readwrite").objectStore("queue").put(me.encrypt(obj));
            }
        }
        
    },
    
    encrypt: function (o) {
        return (this.main.encryption.doEncryption(o.cid)) ? this.main.encryption.encrypt(o) : o;
        //return o;
    },
    
    log: function (sp) {
    
        /*
        Log: Store a log of every action that is performed by the application.  Every time a request is made to the server,
        dump the log to a json node that will be stored in the transactions table.  Every time you get a success message back from the server, scrub the log.
        */
        
        if (this.main.util.isLogAction(sp.cmd)) {
        
            var me = this;
            var user = me.main.entity.getUser();
            var now = new Date();
            var ds = [now.getUTCFullYear(),now.getUTCMonth()+1,now.getUTCDate(),
            now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds()].join('.');
            var a = [user.i,ds].join('.');
            
            var obj = {key:a};
            for (var item in sp) {
                if (Ext.isObject(sp[item])||Ext.isArray(sp[item])||Ext.isFunction(sp[item])) {
                    //skip.
                } else {
                    if (!/^(password)$/i.test(item)) obj[item] = sp[item];
                }
            }
            try {
                var logTransaction = me.db.transaction(["log"], "readwrite");
                var logObjectStore = logTransaction.objectStore("log");
                logObjectStore.put(obj);
            } catch (e) {
                //debugger;
            }
           
        }
           
        
    },
    
    

    buildRequest: function (sp) {
    
        //Starting point for a server request.
        //Get all pending changes list in queue, get the data objects that have been updated in data, then proceed to server.
        //If this is invoked from request the arr should be empty array.
        
        var arr = []; //This will be used to store all changes that also need to be passed to the server.
        var me = this;
        var objects = {};
        var log = [];
        
        //Log - shows all actions the user has performed in app.  Can be turned on/off at configuration.
        var logTransaction = me.db.transaction(["log"], "readonly");
        var logObjectStore = logTransaction.objectStore("log");
            
        //Get all items in the log.
        var logrequest = logObjectStore.openCursor().onsuccess = function(event) {
            var cursor = event.target.result;
            if (cursor) {
                log.push(cursor.value);
                cursor['continue']();
            } else {
           
                //end of log reached or no items in log.
        
                //Queue
                var queueTransaction = me.db.transaction(["queue"], "readonly");
                var queueObjectStore = queueTransaction.objectStore("queue");
                    
                //Get all items in the queue.
                var queuerequest = queueObjectStore.openCursor().onsuccess = function(event) {
                    var cursor = event.target.result;
                    if (cursor) {
                        //console.log("found " + cursor.value.a + " in queue");
                        //console.log(Ext.encode(cursor.value.aq));
                        arr.push(cursor.value);
                        cursor['continue']();
                    } else {
                        
                        var dataTransaction = me.db.transaction(["data"],"readonly");
                        var dataObjectStore = dataTransaction.objectStore("data");
                        
                        dataObjectStore.onerror = function (event) {
                            console.log("error writing server request.");
                        }
                   
                        if (arr.length>0) {
                            arr.sort(function (a,b) {
                                if (a.a < b.a) return -1;
                                if (a.a > b.a) return 1;
                                return 0;
                            });
                            
                            var keyRangeValue = IDBKeyRange.bound(arr[0].a, arr[arr.length-1].a);
                  
                   
                            var x = 0;
                            
                            var datarequest = dataObjectStore.openCursor(keyRangeValue).onsuccess = function(event) {
                                var cursor = event.target.result;
                                if (cursor) {
                                    empty = false;
                                    //console.log("changes found to save");
                                    for (var i=x,l=arr.length;i<l;i++) {
                                        if (arr[i].a < cursor.key) {
                                            //New items with keys smaller than cursor
                                        } else if (arr[i].a===cursor.key) {
                                            x++;
                                            //Existing items with keys matching cursor.
                                            objects[cursor.key] = cursor.value;
                                            break;
                                        } else {
                                            break;
                                        }
                                    }
                                    cursor['continue']();
                                } else {
                                    //end of cursor reached.
                                    //console.log("done - building the request");
                                    //Pass to the server the request settings provider, the array of items that need to be changed, and
                                    //the objects is the payload of changed objects.
                                    if (!sp.options.action) {
                                        if (arr.length>0) {
                                            sp = Ext.apply(sp,{
                                                options: {action: 'write'}
                                            });
                                        }
                                    }
                                    me.main.server.request(sp,arr,objects,log);
                                }
                            }
                        } else {
                            //No data to save, just make normal request.
                            me.main.server.request(sp,arr,objects,log);
                        }
                    }
                }
            }
        }
    },
    
    remove: function (k) {
        var queueTransaction = this.db.transaction(["queue"], "readwrite");
        var queueObjectStore = queueTransaction.objectStore("queue");
        queueObjectStore['delete']("queue" + "." + k);
    },

        
    //--------------------------------------
    // Utilities
    //--------------------------------------
    
    buildParamString: function(sp) {
        
        var params = [],s='';
        for (var item in sp) {
            if (item.substr(0,1)=="$") {
                if (sp[item]) params.push(item+'.'+sp[item]);
            }
        }
        if (params.length>0) {
           s = params.sort().join(".");
        }
        return s;
    },
    
    buildCacheKey: function (sp,ps) {
        //TODO: change to active user.
        
        var userid = this.main.entity.getUser().i; //temp

        //This forces the key to a specific order so it is always predictable.
        if (ps) {
            return [(sp.redirectsource||sp.redirectalias||sp.source||sp.n||sp.sourcealias||sp.a),(sp.descendants||100),(sp.page||1),(sp.pagesize||1000),(sp.low||0),(sp.high||1),ps,userid].join(".");
        } else {
            return [(sp.redirectsource||sp.redirectalias||sp.source||sp.n||sp.sourcealias||sp.a),(sp.descendants||100),(sp.page||1),(sp.pagesize||1000),(sp.low||0),(sp.high||1),userid].join(".");
        }
    }
    
});

