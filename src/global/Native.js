/**
 * This class is a helper class for the MainController.
 *
 * Manages all interactions with the native wrapper.
 */

Ext.define('bravo.global.Native', {
    extend: 'Ext.app.Controller',

    alias: 'controller.wrapper',
    
    requires: [
        
    ],
    
    config: {

	},
    
    setup: function () {
       if (this.main.environment.isNative()) this.startNative();
    },
	
    startNative: function () {
        //These only run for native version of the app.
        
        //Hide the native splash screen. -- need to add here.
        setTimeout(function() {
            navigator.splashscreen.hide();
        }, 4000);
        
        //Auto log out if you leave the app.
        if (this.main.environment.get("RESTART_ON_EXIT")) {
            document.addEventListener("resume", Ext.bind(this.resumeHandler,this), false);
        }
        
        document.addEventListener("online", Ext.bind(this.nativeOnline,this), false);
        document.addEventListener("offline", Ext.bind(this.nativeOffline,this), false);
        
        // Added for IDScan plugin
        window.addEventListener("readerconnected", Ext.bind(this.readerConnect,this), false);
        window.addEventListener("readerdisconnected", Ext.bind(this.readerDisConnect,this), false);
        window.addEventListener("readergavetrackstring", Ext.bind(this.readerGaveTrackString,this), false);
        
    },
    
    readerConnect: function() {
        //Custom Actions
        testFn = eval("this.main.custom.readerconnectedHandler");
        if (Ext.isFunction(testFn)) {
            fn = Ext.bind(eval("this.main.custom.readerconnectedHandler"),this.main.custom,arguments);
            fn.call();
        }
    },
    
    readerDisConnect: function() {
        //Custom Actions
        testFn = eval("this.main.custom.readerdisconnectedHandler");
        if (Ext.isFunction(testFn)) {
            fn = Ext.bind(eval("this.main.custom.readerdisconnectedHandler"),this.main.custom,arguments);
            fn.call();
        }
    },
    
    readerGaveTrackString: function() {
        //Custom Actions
        testFn = eval("this.main.custom.readergavetrackstringHandler");
        if (Ext.isFunction(testFn)) {
            fn = Ext.bind(eval("this.main.custom.readergavetrackstringHandler"),this.main.custom,arguments);
            fn.call();
        }
    },
        
    nativeOnline: function() {
        this.main.environment.setOnline(true);
    },
    
    nativeOffline: function() {
        this.main.environment.setOnline(false);
    },

    resumeHandler: function () {
        this.main.actions.perform({
            cmd: 'idle'
        });
    }
    
});