/**
 * This class is a helper class for the MainController.
 *
 * Maintains utilities.
 */

Ext.define('bravo.global.Util', {
    extend: 'Ext.app.Controller',

    alias: 'controller.util',
    
    requires: [
        
    ],
    
    get: function (mx) {
        //Locate any component currently rendered in the view using id, name, or data object reference.
        var cmp = null;
        if (!mx) return;
        var v = this.main.view;
        if (Ext.isObject(mx)) {
            if (mx.i) cmp = v.down("[i="+mx.i+"]");
        } else {
            if (Ext.isNumeric(mx)) {
                cmp = v.down("[i="+mx+"]");
            } else {
                cmp = v.down("*[name='" + mx + "']");
            }
        }
        if (!cmp) console.log("can't find " + mx);
        return cmp;
	},
    
    getRelationshipRequest: function (s) {
        return {
            children: {descendants: 1, low:0, high:1},
            ancestors: {descendants: 0, low:0, high:0},
            item: {descendants: 0, low:0, high:0},
            descendants: {descendants: 100, low: 0, high: 1},
            stepparents: {descendants: 100, low:1, high:1},
            users: {descendants: 100, low: 2, high: 6},
            favorites: {descendants: 1, low: 7, high: 7},
            objects: {descendants: 100, low: 0, high: 0}
        }[s];
    },

    getId: function (sp) {
        //Resolve differences between different components and requests and how they can pass the id to be requested.
        //Currently is not used -- probably can remove this function.
        if (sp) {
            if (sp.rec){
                if (sp.rec.data) {
                    if (sp.rec.data.i) return sp.rec.data.i;
                }
            }
            if (sp.component) {
                if (sp.component.i) return sp.component.i;
            }
            if (sp.i) return sp.i;
        }
        return '';
    },
    
    buildArray: function (s,d) {
		//Take a comma-delimited list of strings and return an array.
		if (!s) return [];
        d = d||',';
		s = s.toString();
		if (s.indexOf(d)<0) return [s];
		return s.split(d);
	},
    
    revalue: function (s) {
        //The json data in the webservice is always stored as strings.  This is necessary for searching.
        //revalue changes some data to native data types for app convenience.
        try {
            //Change numbers to raw numbers instead of strings.
            if(parseFloat(s)+''== s) return parseFloat(s); //eval as a number.
            if(/^(True|False|Null|TRUE|FALSE)$/.test(s)) return s; //eval as a string.
            if(/^(null||NULL||NaN||nan)$/.test(s)) return ''; //eval as empty string
            if(/^(true|false|undefined)$/i.test(s)) return eval(s); //eval as native
           
            //Dates converted to native dates.  Date string must be Y-m-d H:i:s
            //Possible that the data will come in as Y-m-d H:i:s.u  with fractions of seconds added.
            if (!s) return;
            if (s.match(/([0-9]{4})-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])\ ([0-9]{2}):([0-9]{2}):([0-9]{2})/g)) {
                var parts = s.match(/\d+/g);
                return new Date(parts[0], parts[1] - 1, parts[2], parts[3], parts[4], parts[5]);
            }

        } catch (e) {
            //debugger;
        }
    	return s;
	},
    
    isLogAction: function (s) {
        if (!s) return;
        return !/^(setbusy|idle|provider|listen)$/i.test(String(s));
    },
    
    isReadOnlyProperty: function (s) {
        //Properties that are not used in any requests.
        if (!s) return;
        return /^(aq|i|g|p|r|upd|upb|cr|crb|leaf|sd|ed)$/i.test(String(s));
    },
    
    isMetaProperty: function (s) {
        //meta properties in bravo are always preceeded by an underscore.
        if (!s) return;
        s = s.toString();
        return s.substr(0,1)=='_';
    },
    
    isTreeProperty: function (s) {
        if (!s) return;
        return /^(a|z|pa|t|ca|sd|ed)$/i.test(String(s));
    },
    
    isChangeableTreeProperty: function (s) {
        if (!s) return;
        return /^(z|t|sd|ed)$/i.test(String(s));
    },
    
    isObjectProperty: function (s) {
        if (!s) return;
        return /^(n|l|cid|utc|ct|ix1|ix2|ix3)$/i.test(String(s));
    },
    
    isReservedProperty: function (s) {
        if (!s) return;
        return /^(id|cmd|aq|usecache|cache|inventory|webservice|relationship)$/i.test(String(s));
    },
    
    isReadProperty: function (s) {
        //Properties that are used in read requests.
        if (!s) return;
        if (s.substr(0,1)=='$') return true;
        return /^(redirectalias|redirectsource|sourcealias|source|n|a|descendants|high|low|page|pagesize)$/i.test(String(s));
    },
    
    isNotProperty: function (mx) {
        return ((Ext.isArray(mx))||(Ext.isObject(mx))||(Ext.isFunction(mx)));
    },
    
    ISO8601ToDateTime: function(s) {
		//Note: must be in ISO format or will return null.
		if (!s) return;
        //fails if s is a date.
        if (s.match(/([0-9]{4})-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])\ ([0-9]{2}):([0-9]{2}):([0-9]{2})/g)) {
            var parts = s.match(/\d+/g);
            return new Date(parts[0], parts[1] - 1, parts[2], parts[3], parts[4], parts[5]);
        }
        return;
	},
    
    dateToISO8601 : function (d) {
		return (Ext.isDate(d)) ? Ext.util.Format.date(d,"Y-m-d H:i:s") : d;
	},
    
    stringToDate: function (s) {
        //Given a string that should represent a date, convert it to a valid date and return that date.
        //Y-m-d H:i:s
        
        //Only allow strings.
        if (!s) return s;
        if (Ext.isNumeric(s)) return s;
        if (!Ext.isString(s)) return s;
        if (s.match(/([0-9]{4})-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])\ ([0-9]{2}):([0-9]{2}):([0-9]{2})/g)) {
            var parts = s.match(/\d+/g);
            return new Date(parts[0], parts[1] - 1, parts[2], parts[3], parts[4], parts[5]);
        }
        //Y-m-d
        if (s.match(/([0-9]{4})-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])/g)) {
            var parts = s.match(/\d+/g);
            return new Date(parts[0], parts[1] - 1, parts[2]);
        }
        
        //Any other recognizable date formatted string.
        var dt = new Date(s);
        if ((!Ext.isDate(dt))||(dt.toString()=="Invalid Date")) {
            //Nothing passes, return null.
            return;
        } else {
            return dt;
        }

    },
    
    
    createCORSRequest: function (method, url) {
        var xhr = new XMLHttpRequest();
        if ("withCredentials" in xhr) {

        // Check if the XMLHttpRequest object has a "withCredentials" property.
        // "withCredentials" only exists on XMLHTTPRequest2 objects.
        xhr.open(method, url, true);

        } else if (typeof XDomainRequest != "undefined") {

        // Otherwise, check if XDomainRequest.
        // XDomainRequest only exists in IE, and is IE's way of making CORS requests.
        xhr = new XDomainRequest();
        xhr.open(method, url);

        } else {

        // Otherwise, CORS is not supported by the browser.
        xhr = null;

        }
        return xhr;
    },
    
    
    gridToExcel: function (cmp) {
        return 'data:application/vnd.ms-excel;base64,' +
        Ext.util.Base64.encode(this.getExcelXml(cmp,true));
    },
    
    getExcelXml: function(cmp,includeHidden) {
        //Converts a grid to excel format.
        var worksheet = this.createWorksheet(cmp,includeHidden);
        //var totalWidth = cmp.getColumnModel().getTotalWidth(includeHidden);
        var totalWidth = 1000;
        return '<?xml version="1.0" encoding="utf-8"?>' + '<ss:Workbook xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:o="urn:schemas-microsoft-com:office:office">' + '<o:DocumentProperties><o:Title>' + cmp.title + '</o:Title></o:DocumentProperties>' + '<ss:ExcelWorkbook>' + '<ss:WindowHeight>' + worksheet.height + '</ss:WindowHeight>' + '<ss:WindowWidth>' + worksheet.width + '</ss:WindowWidth>' + '<ss:ProtectStructure>False</ss:ProtectStructure>' + '<ss:ProtectWindows>False</ss:ProtectWindows>' + '</ss:ExcelWorkbook>' + '<ss:Styles>' + '<ss:Style ss:ID="Default">' + '<ss:Alignment ss:Vertical="Top" ss:WrapText="1" />' + '<ss:Font ss:FontName="arial" ss:Size="10" />' + '<ss:Borders>' + '<ss:Border ss:Color="#e4e4e4" ss:Weight="1" ss:LineStyle="Continuous" ss:Position="Top" />' + '<ss:Border ss:Color="#e4e4e4" ss:Weight="1" ss:LineStyle="Continuous" ss:Position="Bottom" />' + '<ss:Border ss:Color="#e4e4e4" ss:Weight="1" ss:LineStyle="Continuous" ss:Position="Left" />' + '<ss:Border ss:Color="#e4e4e4" ss:Weight="1" ss:LineStyle="Continuous" ss:Position="Right" />' + '</ss:Borders>' + '<ss:Interior />' + '<ss:NumberFormat />' + '<ss:Protection />' + '</ss:Style>' + '<ss:Style ss:ID="title">' + '<ss:Borders />' + '<ss:Font />' + '<ss:Alignment ss:WrapText="1" ss:Vertical="Center" ss:Horizontal="Center" />' + '<ss:NumberFormat ss:Format="@" />' + '</ss:Style>' + '<ss:Style ss:ID="headercell">' + '<ss:Font ss:Bold="1" ss:Size="10" />' + '<ss:Alignment ss:WrapText="1" ss:Horizontal="Center" />' + '<ss:Interior ss:Pattern="Solid" ss:Color="#A3C9F1" />' + '</ss:Style>' + '<ss:Style ss:ID="even">' + '<ss:Interior ss:Pattern="Solid" ss:Color="#CCFFFF" />' + '</ss:Style>' + '<ss:Style ss:Parent="even" ss:ID="evendate">' + '<ss:NumberFormat ss:Format="[ENG][$-409]dd\-mmm\-yyyy;@" />' + '</ss:Style>' + '<ss:Style ss:Parent="even" ss:ID="evenint">' + '<ss:NumberFormat ss:Format="0" />' + '</ss:Style>' + '<ss:Style ss:Parent="even" ss:ID="evenfloat">' + '<ss:NumberFormat ss:Format="0.00" />' + '</ss:Style>' + '<ss:Style ss:ID="odd">' + '<ss:Interior ss:Pattern="Solid" ss:Color="#CCCCFF" />' + '</ss:Style>' + '<ss:Style ss:Parent="odd" ss:ID="odddate">' + '<ss:NumberFormat ss:Format="[ENG][$-409]dd\-mmm\-yyyy;@" />' + '</ss:Style>' + '<ss:Style ss:Parent="odd" ss:ID="oddint">' + '<ss:NumberFormat ss:Format="0" />' + '</ss:Style>' + '<ss:Style ss:Parent="odd" ss:ID="oddfloat">' + '<ss:NumberFormat ss:Format="0.00" />' + '</ss:Style>' + '</ss:Styles>' + worksheet.xml + '</ss:Workbook>';
    },

    createWorksheet: function(cmp,includeHidden) {

//      Calculate cell data types and extra class names which affect formatting
        var cellType = [];
        var cellTypeClass = [];
        var cm = cmp.getColumns();
        var totalWidthInPixels = 0;
        var colXml = '';
        var headerXml = '';
        for (var i = 0; i < cm.length; i++) {
            if (includeHidden || !cm[i].hidden) {
                var w = cm[i].width;
                totalWidthInPixels += w;
                colXml += '<ss:Column ss:AutoFitWidth="1" ss:Width="' + w + '" />';
                headerXml += '<ss:Cell ss:StyleID="headercell">' +
                    '<ss:Data ss:Type="String">' + cm[i].text + '</ss:Data>' +
                    '<ss:NamedCell ss:Name="Print_Titles" /></ss:Cell>';
                cellType.push("String");
                cellTypeClass.push("");
                //var fld = cmp.store.recordType.prototype.fields.get(cm[i].dataIndex);
                /*
                switch(fld.type) {
                    case "int":
                        cellType.push("Number");
                        cellTypeClass.push("int");
                        break;
                    case "float":
                        cellType.push("Number");
                        cellTypeClass.push("float");
                        break;
                    case "bool":
                    case "boolean":
                        cellType.push("String");
                        cellTypeClass.push("");
                        break;
                    case "date":
                        cellType.push("DateTime");
                        cellTypeClass.push("date");
                        break;
                    default:
                        cellType.push("String");
                        cellTypeClass.push("");
                        break;
                }
                */
            }
        }
        var visibleColumnCount = cellType.length;

        var result = {
            height: 9000,
            width: Math.floor(totalWidthInPixels * 30) + 50
        };

//      Generate worksheet header details.
        var t = '<ss:Worksheet ss:Name="' + cmp.title + '">' + '<ss:Names>' + '<ss:NamedRange ss:Name="Print_Titles" ss:RefersTo="=\'' + cmp.title + '\'!R1:R2" />' + '</ss:Names>' + '<ss:Table x:FullRows="1" x:FullColumns="1"' + ' ss:ExpandedColumnCount="' + visibleColumnCount + '" ss:ExpandedRowCount="' + (cmp.store.getCount() + 2) + '">' + colXml + '<ss:Row ss:Height="38">' + '<ss:Cell ss:StyleID="title" ss:MergeAcross="' + (visibleColumnCount - 1) + '">' + '<ss:Data xmlns:html="http://www.w3.org/TR/REC-html40" ss:Type="String">' + '<html:B><html:U><html:Font html:Size="15">' + cmp.title + '</html:Font></html:U></html:B>Generated by ExtJs</ss:Data><ss:NamedCell ss:Name="Print_Titles" />' + '</ss:Cell>' + '</ss:Row>' + '<ss:Row ss:AutoFitHeight="1">' + headerXml + '</ss:Row>';

//      Generate the data rows from the data in the Store
        for (var i = 0, it = cmp.store.data.items, l = it.length; i < l; i++) {
            t += '<ss:Row>';
            var cellClass = (i & 1) ? 'odd' : 'even';
            r = it[i].data;
            var k = 0;
            for (var j = 0; j < cm.length; j++) {
                if (includeHidden || !cm[j].hidden) {
                    var v = r[cm[j].dataIndex];
                    t += '<ss:Cell ss:StyleID="' + cellClass + cellTypeClass[k] + '"><ss:Data ss:Type="' + cellType[k] + '">';
                        if (cellType[k] == 'DateTime') {
                            t += v.format('Y-m-d');
                        } else {
                            t += v;
                        }
                    t +='</ss:Data></ss:Cell>';
                    k++;
                }
            }
            t += '</ss:Row>';
        }

        result.xml = t + '</ss:Table>' + '<x:WorksheetOptions>' + '<x:PageSetup>' + '<x:Layout x:CenterHorizontal="1" x:Orientation="Landscape" />' + '<x:Footer x:Data="Page &amp;P of &amp;N" x:Margin="0.5" />' + '<x:PageMargins x:Top="0.5" x:Right="0.5" x:Left="0.5" x:Bottom="0.8" />' + '</x:PageSetup>' + '<x:FitToPage />' + '<x:Print>' + '<x:PrintErrors>Blank</x:PrintErrors>' + '<x:FitWidth>1</x:FitWidth>' + '<x:FitHeight>32767</x:FitHeight>' + '<x:ValidPrinterInfo />' + '<x:VerticalResolution>600</x:VerticalResolution>' + '</x:Print>' + '<x:Selected />' + '<x:DoNotDisplayGridlines />' + '<x:ProtectObjects>False</x:ProtectObjects>' + '<x:ProtectScenarios>False</x:ProtectScenarios>' + '</x:WorksheetOptions>' + '</ss:Worksheet>';
        return result;
    }

});