/**
 * Rules
 
 This class is a helper class for the MainController.  Its role is to provide business logic and data logic for all form-based data collection components.
 *
 There are multiple layers of events.
 DOM events - bubble through the browser
 Sencha events - bubble inside and through sencha components that have been rendered to the screen.  (Doesn't work for controllers, etc)
 Bravo rules (rules) - Loosely-based topic messaging system.  Designed to not be tied to DOM render but still provide communication between various components or sections of app.  Provides business logic.
 
 
 1. When rule objects are loaded into memory, subscribe to them.
 2. When components are added to display stack, find all of its rules and assign component to it.
 3. When user interacts with components broacast any rules marked for broadcast and perform action associated with it (optional)
 4. When the rule is broadcast, all rules marked for subscribe with that topic are found. Each action is performed in order of priority (optional)
 5. When any action is performed, if there is no component attached to it, it skips the action.
 6. When components are removed from display stack, find all of its rules and remove component from it.
 7. Optionally can clear subscribers by topic, component or rule.
 8. Containers can be identified as managers to manage a set of data-entry components and decide whether they are complete or not.

 
 
 Rule:
    Topic:  A developer-generated topic used to broadcast/subscribe to various types of messages.
    Type:
        1) Broadcast = when user interacts with this component, broadcast this message
        2) Subscribe = when message with this topic has been broadcast, perform an action.
    Action: Name of action to perform when broadcasting or hearing message.
    Filter: check data provider for filter and only do action if filter is passed.
    Property: used for setValue/getValue
    Value: used for setValue/getValue
    
    
    Priority: 
        10 = data providers
        20 = managers
        can organize custom rules at priority 20+ to make sure that all data providers have been updated and managers have
        calculated the completeness of the form before determining whether to disable/enable based on completeness.
 
 
 
 */

Ext.define('bravo.global.Rules', {
    extend: 'Ext.app.Controller',

    alias: 'controller.rules',
    
    requires: [
    ],
    
    setup: function () {
        this.subscribers = new Ext.util.MixedCollection({});
        this.broadcasters = new Ext.util.MixedCollection({});
    },
    
    //--------------------------------------
    // General Rules Functions
    //--------------------------------------
    //These functions provide global rules support for creating, subscriptions, and broadcasts.
    
    subscribe: function (rule,cmp) {
        //Given a rule, and optionally component object that contains it, subscribe to this rule.
        //If the rule has no component, look up it's parent and consider that to be its component.
        var o = this.main.data.getObject(rule.pa);
        
        if (rule&&o) {
            if (rule.type=='broadcast') {
                this.broadcasters.add(rule.a,{
                    rule: rule.a,
                    target: o.a,
                    component: cmp,
                    topic: rule.topic,
                    type: rule.type,
                    action: rule.action,
                    priority: rule.priority||1,
                    filter: rule.filter
                });
            } else {
                this.subscribers.add(rule.a,{
                    rule: rule.a,
                    target: o.a,
                    component: cmp,
                    topic: rule.topic,
                    type: rule.type,
                    action: rule.action,
                    priority: rule.priority||1,
                    filter: rule.filter
                });
            }
        }
    },
    
    listen: function (sp) {
        //Given a settings provider with a component, find all children of that component with class type "rule" and assign it to the rule
        
        var cmp = sp.component;
        var o = this.main.data.getObject(cmp);
        
        var rules = this.main.data.filter(this.main.data.getChildren(o),"cid EQ rule");
        for (var i=0,l=rules.length;i<l;i++) {
            var rule = rules[i];
           
            //Step 1. Connect component to rule.
            this.subscribe(rule,cmp);
           
            //Step 2. Listen to all rules associated with this component.
            if (rule.type=='subscribe') {
                if (rule.action) {
                    this.main.actions.perform({
                        component: cmp,
                        cmd: rule.action,
                        rule: rule,
                        callback: Ext.emptyFn,
                        scope: this
                    });
                }
            }
        }
    },
    
	broadcast: function (sp) {
		
        var cmp = sp.component;
        
        if (cmp) {
        
            //The user interacts with a component.
            
            //Step 1.  Given that component, find all children of that component with class "rule" and type "broadcast".
            var o = this.main.data.getObject(sp.component);
            var broadcastrules = this.main.data.filter(this.main.data.getChildren(o),"cid EQ rule AND type EQ broadcast");
            
            //Step 2. For each of these rules...
            for (var i=0,l=broadcastrules.length;i<l;i++) {
            
                var brule = broadcastrules[i];

                //A. Fire any actions associated with this topic.
                if (brule.action) {
                    this.main.actions.perform(Ext.apply(sp,{
                        cmd: brule.action,
                        callback: Ext.emptyFn,
                        scope: this
                    }));
                }
               
            
                //B. Find all the components that are also subscribed to the same topic and fire their rules.  But not the initiating component.
                //The main sp represents the subscribing rule.  The rule property represents the broadcast rule that intiated the action.
                var col = this.subscribers.filter('topic',brule.topic);
                col.sort([{property:'priority',direction:'ASC'}]);
               
                col.each(function(rule) {
                    if ((rule.type=='subscribe')&&(rule.topic)&&(rule.component)) {
                        //add protection here to prevent circular logic.
                            this.main.actions.perform(Ext.apply(rule,{
                                rule: brule,
                                cmd: rule.action,
                                scope:this,
                                callback: Ext.emptyFn
                            }));
                    }
                },this);
            }
        } else {
        
            //Bravo has performed an event and broadcast a topic.  Find all components subscribed and fire this rule.
            var col = this.subscribers.filter('topic',sp.topic);
            col.sort([{property:'priority',direction:'ASC'}]);
           
            col.each(function(rule) {
                if ((rule.type=='subscribe')&&(rule.topic)&&(rule.component)) {
                    if (rule.component.a!=cmp.a) {
                        this.main.actions.perform(Ext.apply(rule,{
                            cmd: rule.action,
                            scope:this,
                            callback: Ext.emptyFn
                        }));
                    }
                }
            },this);
           
        }
    },
    
    ignore: function (sp) {
        //Given a settings provider with a component, find all rules this component is subscribed to and disconnect it from each one.
        var cmp = sp.component;
        var o = this.main.data.getObject(sp.component);
        var rules = this.main.data.filter(this.main.data.getChildren(o),"cid EQ rule");
        for (var i=0,l=rules.length;i<l;i++) {
            if (/^(complete)$/.test(rules[i].action)) {
                //Leave these rules intact even if the component is removed.
                this.subscribe(rules[i],null);
            } else {
                this.unsubscribe(rules[i]);
            }
        }
    },
    
    broadcastTopic: function (sp) {
        //Send out a message programmatically without reference to a specific component.
        
        var col = this.subscribers.filter('topic',sp.topic);
        col.sort([{property:'priority',direction:'ASC'}]);
       
        col.each(function(rule) {
            if ((rule.type=='subscribe')&&(rule.topic)&&(rule.component)) {
                if (rule.component.a!=cmp.a) {
                    this.main.actions.perform(Ext.apply(rule,{
                        rule: sp,
                        cmd: rule.action,
                        scope:this,
                        callback: Ext.emptyFn
                    }));
                }
            }
        },this);
    },
    
    unsubscribe: function (rule) {
        //Completely remove rule from subscribers.
        if (rule.component) delete rule.component;
        
        if (rule.type=='broadcast') {
            this.broadcasters.removeAtKey(rule.a);
        } else {
            this.subscribers.removeAtKey(rule.a);
        }
	},
    
    addRule: function (sp) {
        //Subscribe component to this custom provider rule.
        var me = sp.scope;
        me.subscribe(sp.objects[0],sp.component);
    },
    
    
    //--------------------------------------
    // Data Component Functions
    //--------------------------------------
    //Functions that support components that send data to a data storage.
    
    addProvider: function (sp) {
        //Create a custom rule that broadcasts the provider name any time the component is interacted with.

        var cmp = sp.component;
        
        this.main.data.create({
            temporary: true,
            component: cmp,
            scope:this,
            callback: this.addRule,
            data: [{
                cid: 'rule',
                a: "provider." + cmp.a,
                l: "provider." + cmp.provider,
                pa: cmp.a,
                t: 0,
                topic: cmp.provider,
                type: 'broadcast',
                action: 'setprovider'
            }]
        });
    },
    

    provider: function (sp) {
        //Given a settings provider with a component, go up the ancestry of this item and find the first object with a provider property.  Register that provider name with this component.
        
        //Find the provider name for this component.
        var cmp = sp.component;
        var o = this.main.data.getObject(sp.component);

        var anc = this.main.data.getAncestors(o);
        //Evaluate ancestors top->bottom.  Closest item to cmp that has provider becomes provider.
        for (var i=0,l=anc.length;i<l;i++) {
            if (anc[i].provider) cmp.provider = anc[i].provider;
        }
        
        if (cmp.provider) {
        
            //Create a rule that will listen for this provider.  If provider is changed, it will update this component.
            this.main.data.create({
                temporary: true,
                component: cmp,
                scope:this,
                callback: this.addRule,
                data: [{
                    cid: 'rule',
                    a: "provider." + cmp.a,
                    l: "provider." + cmp.provider,
                    pa: cmp.a,
                    t: 0,
                    topic: cmp.provider,
                    type: 'subscribe',
                    priority: 10,
                    action: 'getprovider'
                }]
            });
            
            //Create a rule that will broadcast any changes this component makes.
            this.main.data.create({
                temporary: true,
                component: cmp,
                scope:this,
                callback: this.addRule,
                data: [{
                    cid: 'rule',
                    a: "put." + cmp.a,
                    l: "put." + cmp.provider,
                    pa: cmp.a,
                    t: 0,
                    topic: cmp.provider,
                    priority: 10,
                    type: 'broadcast'
                }]
            });
        
        }
    },
    
    
    setProvider: function (sp) {
        //Maps a provider name to an alias.
        this.main.data.providers[sp.component.provider] = sp.provider;
    },
    
    getProvider: function (sp) {
        //The component needs to be populated with data from the dataprovider.
        var cmp = sp.component;
        
        //Get the alias of the provider from the subscriptions.
        var a = this.main.data.providers[cmp.provider];
        
        if (a) {
        
            //Look up the data object.
            var o = this.main.data.getObject(a);
           
            if (o) {
            
                //The name of the component corresponds with the property name that provides the value.
                if (cmp.n) {
                    sp.component.setValue(o[cmp.n]);
                } else {
                    console.log(cmp.l + " doesn't have a name assigned.");
                }
           
            }
        
        }
        
    },
    
    
        
    
    //--------------------------------------
    // Manage Functions
    //--------------------------------------
    //Containers marked with manage: providername will listen to changes in the dataprovider.  It will check to see if
    //that section of the dataprovider is completed/started.
    
    require: function (sp) {
        //Given a settings provider with a component, if the component is required, create a temporary rule to track items that are required.
        
        var cmp = sp.component;
        var o = this.main.data.getObject(sp.component);
        
        //Create a rule that will listen for this provider.  If provider is changed, manage it.
        this.main.data.create({
            temporary: true,
            component: cmp,
            scope:this,
            callback: this.addRule,
            data: [{
                cid: 'rule',
                a: "required." + cmp.a,
                l: "required." + cmp.provider,
                pa: cmp.a,
                t: 0,
                topic: 'required',
                type: 'subscribe'
            }]
        });
        
    },
    
    manage: function (sp) {
        //Given a settings provider with a component, this component will manage a set of components that provide data to the
        //dataprovider.  It will determine and manage whether a form is started/completed and store that state in the dataprovider.
        
        var cmp = sp.component;
        var o = this.main.data.getObject(sp.component);
        
        cmp.required = this.buildRequired(o);

        //Create a rule that will listen for this provider.  If provider is changed, manage it.
        this.main.data.create({
            temporary: true,
            component: cmp,
            scope:this,
            callback: this.addRule,
            data: [{
                cid: 'rule',
                a: "manage." + cmp.a,
                l: "manage." + cmp.provider,
                pa: cmp.a,
                t: 0,
                topic: cmp.manage,  //name of a provider
                type: 'subscribe',
                priority: 20,
                action: 'manageprovider'
            }]
        });
        
        //Create a second rule that will listen for a rebuild.  If it hears this broadcast, it will rebuild all required items.
        //Necessary if items are dynamically set to be required / not required based on data.
        this.main.data.create({
            temporary: true,
            component: cmp,
            scope:this,
            callback: this.addRule,
            data: [{
                cid: 'rule',
                a: "refreshmanager." + cmp.a,
                l: "refreshmanager." + cmp.provider,
                pa: cmp.a,
                t: 0,
                topic: 'refresh',
                type: 'subscribe',
                priority: 20,
                action: 'refreshmanager'
            }]
        });
        
        
    },
    
    refreshManager: function (sp) {
        //Examine a component's descendants again and find all items that are required.
        var cmp = sp.component;
        var o = this.main.data.getObject(sp.component);
        
        cmp.required = this.buildRequired(o);
    },
    
    
    buildRequired: function (o) {
    
        //Build a collection of all of the required properties in the descendancy of this component.
        //If a descendant has a manage property, then do not include it in the set.
        var arr = this.main.data.getDescendants(o,{exclude:'manage'});
        
        //Required = a temporary rule which is the child of a component with topic set to required and disabled is not false.
        var required=[];
        for (var i=0,l=arr.length;i<l;i++) {
            if ((arr[i].topic=='required')&&(!arr[i].disabled)) {
                if (arr[i].cmp) {
                    if (arr[i].cmp.n) {
                        required.push(arr[i].cmp.n);
                    }
                } else {
                    var parent = this.main.data.getObject(arr[i].pa);
                    if ((parent)&&(parent.n)) required.push(parent.n);
                }
            }
        }
        return required;
    },
    
    manageProvider: function (sp) {
        //Evaluates a set of properties to make sure they are all started or completed.
        
        var cmp = sp.component;
        var required = cmp.required||[];
        var prv = sp.rule.topic;
        
        var a = this.main.data.providers[sp.rule.topic];
        
        var o = this.main.data.getObject(a);
        
        if (o) {
        
            var started = false;
            var completed = true;
        
            for (var i=0,l=required.length;i<l;i++) {
                if (o[required[i]]) {
                    started = true;
                } else {
                    completed = false;
                }
            }
            
            //Save results to this data provider to store state information about required properties this component manages.
            //e.g. registration_customerinfo_completed = true
            var obj = {
                cmd: 'put',
                objects: [o]
            };
            obj[[cmp.n,'started'].join("_")] = started;
            obj[[cmp.n,'completed'].join("_")] = completed;
            this.main.actions.perform(obj);
        
        } else {
            //No dataprovider has been assigned yet.
        }
        
        
    },
    
    
    
    //--------------------------------------
    // DataView Functions
    //--------------------------------------
    //Components that load a list of various possible dataproviders to choose from.
    

    loadData: function (sp) {
        //Load data into data-aware components such as dataviews and comboboxes.
        
        var captions,vals;
        var cmp = sp.component;
        
        //Raw config from the object.
        var o = this.main.data.getObject(cmp);
        
        //Modified properties to check for templates. 
        var ap = this.main.actions.getSettingsProvider(cmp);
        
        //Apply to main settings provider
        Ext.apply(sp,ap);
        
        //Load data...
        switch (o.cid) {
            case "template":
    
                //Templates
                var alias = this.main.data.providers[cmp.provider];
                var obj = this.main.data.getObject(alias);
                if (obj) {
                    var tpl = new Ext.XTemplate(cmp.ct);
                    cmp.setHtml(tpl.apply(obj));
                } else {
                    var tpl = new Ext.XTemplate(cmp.ct);
                    cmp.setHtml(tpl.apply(cmp));
                }
                break;
           
            default:
           
                //Dataviews, comboboxes, etc.
               
                //Must exclude redirectsource or redirectalias when making a loadData request.  They are only used later when an item is selected.
                if (sp.redirectsource) delete sp.redirectsource;
                if (sp.redirectalias) delete sp.redirectalias;
                   
                
                //1. local values and captions from the component itself.
                //vals and captions can be used for simple hard-coded selections.
                //names and labels can be used for finding items by name.
                //aliases and labels can be used for finding items by alias.
                if ((o.vals)||(o.names)||(o.aliases)) {
                    if (o.vals) vals = this.main.util.buildArray(o.vals);
                    if (o.names) vals = this.main.util.buildArray(o.names);
                    if (o.aliases) vals = this.main.util.buildArray(o.aliases);
                    if (o.captions) captions = this.main.util.buildArray(o.captions);
                    if (o.labels) captions = this.main.util.buildArray(o.labels);
                    if ((vals.length>0)&&(vals.length==captions.length)) {
                        var arr = [];
                        for (var i=0,l=vals.length;i<l;i++) {
                            if (cmp.vals) {
                                arr.push({
                                    value: vals[i],
                                    l: captions[i]
                                });
                            }
                            if (cmp.names) {
                                arr.push({
                                    n: vals[i],
                                    l: captions[i]
                                });
                            }
                            if (cmp.aliases) {
                                arr.push({
                                    a: vals[i],
                                    l: captions[i]
                                });
                            }
                        }
                        Ext.callback(sp.callback,sp.component,[{
                            desc: arr
                        }]);
                    }
                    return;
                }
                
                
                //2. remote data = find this source and make a request with alias,relationship,filter,sort
                //Note: if you want a specific class- just use filter to filter out a specific class and set source to project
                if ((o.source)||(o.sourcealias)) {
           
                    var rq = {
                        cmd: 'request',
                        page: cmp.page||1,
                        pagesize: cmp.pagesize||1000,
                        callback: sp.callback,
                        scope: cmp
                    };
           
                    //Insert secondary filter logic here.
                    if (cmp.secondary) {
                        var alias = this.main.data.providers[cmp.provider];
                        var obj = this.main.data.getObject(alias);
                        var pval = obj[cmp.secondary];
                        if (pval) {
                            cmp.enable();
                            rq['$'+cmp.secondary] = pval;
                        } else {
                            cmp.disable();
                        }
                    }
           
                    this.main.actions.perform(Ext.apply(sp,rq));
                    return;
                }
           
           
                
                this.main.feedback.say({
                    title: 'Info',
                    message: 'Error occurred getting data for ' + o.l
                });
           
                break;
        
        }
    },
    
    add: function (sp) {
    
        //Create a new data item.
    
        var cmp = sp.component;
        var pa;
    
        //Get the alias of the provider from the subscriptions.
        var a = this.main.data.providers[cmp.provider];
        
        //Get the parent context.
        if (cmp.parentprovider) {
            pa = this.main.data.providers[cmp.provider]||this.main.environment.get(cmp.parentprovider)||this.main.entity.getUser().a;
        }
           
        if ((a)&&(pa)) {
        
            //Look up the data object.
            var o = this.main.data.getObject(a);
           
            //Do the new creation here.
            this.main.actions.perform({
                cmd: "create",
                callback: sp.callback,
                component: cmp,
                scope: cmp,
                data: [Ext.applyIf(cmp.template||{},{
                    cid: cmp.$cid,
                    l: cmp.$cid,
                    pa: pa,
                    t: 0,
                    z: 0
                })]
            });
           
            Ext.callback(sp.callback,sp.component,[o]);
           
        } else {
            this.main.feedback.say({
                title: 'Error',
                message: 'Cannot add item.  Required information is missing.'
            });
            Ext.callback(sp.callback,sp.component,[]);
        }
    
    },
    
    remove: function (sp) {
    
        //Remove a data item.
    
        var cmp = sp.component;
    
        //Get the alias of the provider from the subscriptions.
        var a = this.main.data.providers[cmp.provider];
        if (a) {
        
            //Look up the data object.
            var o = this.main.data.getObject(a);
           
            this.main.actions.perform({
                cmd: "put",
                objects: [o],
                pa: this.main.environment.get('RECYCLE_BIN')
            });
           
            Ext.callback(sp.callback,sp.component,[o]);
           
        } else {
            this.main.feedback.say({
                title: 'Error',
                message: 'Please select an item first.'
            });
            Ext.callback(sp.callback,sp.component,[]);
        }
        
    },
    
    
    //--------------------------------------
    // Primary/Secondary Functions
    //--------------------------------------
    //
    primary: function (sp) {
        //Create a custom rule that broadcasts the component name any time the component is interacted with.
        //Use this when you want other components to react when this component's data is changed.

        var cmp = sp.component;
        
        this.main.data.create({
            temporary: true,
            component: cmp,
            scope:this,
            callback: this.addRule,
            data: [{
                cid: 'rule',
                a: "primary." + cmp.a,
                l: "primary." + cmp.primary,
                pa: cmp.a,
                t: 0,
                topic: cmp.n,
                type: 'broadcast'
            }]
        });
    },
    
    secondary: function (sp) {
        //Create a custom rule that subscribes to a topic (a fieldname).
        //Reload the data of this component when it hears that topic broadcast.

        var cmp = sp.component;
        
        this.main.data.create({
            temporary: true,
            component: cmp,
            scope:this,
            callback: this.addRule,
            data: [{
                cid: 'rule',
                a: "secondary." + cmp.a,
                l: "secondary." + cmp.secondary,
                pa: cmp.a,
                t: 0,
                topic: cmp.n,
                type: 'subscribe',
                action: 'loadsecondary'
            }]
        });
    },
    
    loadSecondary: function (sp) {
        var cmp = sp.component;
        
        //insert code here to force a refresh of the data of this component when it hears a primary component is changed.
        cmp.loadData();
    },


    
    //--------------------------------------
    // Helper Functions
    //--------------------------------------
    //
    
    getValue: function (sp) {
        //Get a value from the bravo environment.
        var cmp = sp.component;
        var v = this.main.data.getValue(sp.value);
        
        //Set the value of the component to this.
        cmp.setValue(v);
        
        //Commit data.
        cmp.commitValue();
    
    },
    
    disable: function (sp) {
    
        //Disable/enable a component based on specific information in the dataprovider.
    
        var cmp = sp.component;
        
        if (cmp.provider) {
        
            //Get the alias of the provider from the subscriptions.
            var a = this.main.data.providers[cmp.provider];
            
            //Filter
            if (sp.filter) {
                var arr = this.main.data.filter([this.main.data.getObject(a)],sp.filter);
               
                //if it passes the filter then disable.  If it fails the filter, enable.
                if (arr.length>0) {
                    sp.component.disable();
                } else {
                    sp.component.enable();
                }
            } else {
                sp.component.enable();
            }
        
        }
        
    },
    
    hide: function (sp) {
    
        //Hide/show a component based on specific information in the dataprovider.
    
        var cmp = sp.component;
        
        if (cmp.provider) {
        
            //Get the alias of the provider from the subscriptions.
            var a = this.main.data.providers[cmp.provider];
            
            //Filter
            if (sp.filter) {
                var arr = this.main.data.filter([this.main.data.getObject(a)],sp.filter);
               
                //if it passes the filter then disable.  If it fails the filter, enable.
                if (arr.length>0) {
                    sp.component.hide();
                } else {
                    sp.component.show();
                }
            } else {
                sp.component.enable();
            }
        
        }
        
    }
    
    
    
});