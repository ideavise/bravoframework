/**
 * This class is a helper class for the MainController.
 *
 * Manages user identification, authentication, registration, etc..
 */

Ext.define('bravo.global.Entity', {
    extend: 'Ext.app.Controller',

    alias: 'controller.entity',
    
    requires: [
        
    ],
    
    config: {
		user:'',
        session: '',
        privateKey: '',
        localPassword: ''
	},
    
    setup: function () {
        this.favorites = {};
        
        this.setUser({
            a: 'bravo.public',
            l: 'Public',
            i: 8,
            FirstName: '',
            LastName: ''
        });
    },
    
    getFullName: function () {
        return (this.getUser().FirstName||'') + ' ' + (this.getUser().LastName||'');
    },
    
    receive: function (rsp,arr) {
        if (Ext.isArray(arr)) {
            var d = this.main.data;
            if (arr.length>0) this.setUser(d.revalue(d.merge(arr[0])));
        }
        
        this.setLocalPassword(this.main.encryption.sjcl.hash.sha256.hash(rsp.password)[0]);
        this.setPrivateKey(rsp.password);
    }
    
});