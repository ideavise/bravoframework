/**
 *  This class manages all application settings, environment variables.
 *  Includes a custom lookup of various places for settings to be stored and returns lowest valid option in a cascading list.
 *  
 0. Nothing found -- default passed from code
//add appsettings back in here.
 1. Custom/Local Settings - index.html
 2. Local Storage (from a previous session) - local storage
 3. Database Top Folder
 4. Database Project Folder
 5. QueryString - browser query string (only available for browser-based usage) - query string.
 6. Personalization / Dynamic settings (set during app execution via this.environment.set)
 
 *  
 */
Ext.define('bravo.global.Environment', {

    extend: 'Ext.app.Controller',

    alias: 'controller.environment',
    
    requires: [
        
    ],
	
	config: {
		version: '',
		online: '',
		cache: '',
		cms: '',
		profile: '',
		settings: ''
	},
	
    setup: function () {
   
		this.settings = {};
		this.topsettings = {};
		this.projectsettings = {};
		
		//build environment.
		this.setOnline((this.get("online")));
		this.setCache(this.get("cache"));
		this.setCms(this.get("cms"));
		this.setProfile(Ext.os.deviceType.toLowerCase());
		
		//version management.
		var v = appsettings.version;
		this.setVersion(v);
		if (localStorage.getItem('version')!=v) this.upgrade(v);
        
        //Location services
        var locationsetting = this.get("LOCATION_SERVICE");
        if (locationsetting) this.locate();

        //Address services
        var addresssetting = this.get("ADDRESS_SERVICE");
        if (addresssetting) this.locateaddress();
		
	},
	
	put: function (k, v) {
		localStorage.put(k,v);
	},
	
	
	get: function(k, d) {
		var v;
		try {
		v = this.settings[k]||this.getQueryString(k)||this.projectsettings[k]||this.topsettings[k]||localStorage.getItem(k)||customsettings[k]||localsettings[k]||d||'';
		} catch (e) {
			console.log('missing configuration in index.html for customsetting or localsetting');
		}
		v = (/^(true|yes|on)$/i.test(String(v))) ? true : v;
		v = (/^(false|no|off)$/i.test(String(v))) ? false : v;
		return v;
	},
	
	set: function (k, v, l) {
		if (v==false) v = "false";
		if (v==true) v = "true";
		switch (l) {
			case 'top':
				this.topsettings[k] = v;
				break;
			case 'project':
				this.projectsettings[k] = v;
				break;
			default:
				this.settings[k] = v;
		}
	},
	
	getQueryString: function(key) {
		//Query string.
		var val=null;
		key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
		var regex = new RegExp("[\\?&]"+key+"=([^&#]*)");
		var qsRe = regex.exec(window.location.href);
		if (qsRe) val = qsRe[1];
		return val;
	},
	
	setOnline: function (v) {
		v = (Ext.isDefined(v)&&(!Ext.isEmpty(v))) ? v : window.navigator.onLine;
		this.online = v;
	},
	
	getOnline: function () {
		return window.navigator.onLine&&this.online;
	},
	
	isNative: function () {
		return (window.Cordova || window.cordova) ? true : false;
	},
	
	upgrade: function (v) {
		//Detect if the version number has changed.
		if (window.navigator.onLine) {
			localStorage.setItem('version',v);
		}
	},
	
	refresh: function () {
		//When the user logs in and sets client, check for any personalization for environment values.
	},
    
    locate: function () {
        //Using the device capabilities, identify the location of the device and make those available as environment
        //variables.
        
        var cb = Ext.bind(this.locationsuccessCallback,this);
        var ecb = Ext.bind(this.locationerrorCallback,this);
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(cb,ecb);
        } else {
            console.log("Location not supported on this browser");
        }
    },
    
    locationsuccessCallback: function (sp) {
    
        if (sp.coords) {
        
            //Altitude
            if (sp.coords.altitude) this.settings["altitude"] = Math.round(sp.coords.altitude||0); // Round to the nearest meter
            
            //Latitude
            if (sp.coords.latitude) this.settings["latitude"] = sp.coords.latitude;
            
            //Longitude
            if (sp.coords.longitude) this.settings["longitude"] = sp.coords.longitude;
            
            if (sp.coords.latitude) {
                this.main.actions.perform({
                    cmd: "broadcast",
                    topic: "locate"
                });
            }
            
        } else {
            console.log("Cannot determine location");
        }
    
    },

    locateaddress: function () {
        //Using the device capabilities, identify the location of the device and make those available as environment
        //variables.
        
        var cb = Ext.bind(this.locationaddresssuccessCallback,this);
        var ecb = Ext.bind(this.locationerrorCallback,this);
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(cb,ecb);
        } else {
            console.log("Location not supported on this browser");
        }
    },
    
    locationaddresssuccessCallback: function (sp) {
    
        if (sp.coords) {
        
            //Altitude
            if (sp.coords.altitude) this.settings["altitude"] = Math.round(sp.coords.altitude||0); // Round to the nearest meter
            
            //Latitude
            if (sp.coords.latitude) this.settings["latitude"] = sp.coords.latitude;
            
            //Longitude
            if (sp.coords.longitude) this.settings["longitude"] = sp.coords.longitude;
            
        
            //Physical address.
            if (this.getOnline()) {
                var xhr = this.main.util.createCORSRequest('GET', "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + sp.coords.latitude + "," + sp.coords.longitude + "&sensor=false");
                
                if (xhr) {
                    var cb = Ext.bind(this.addressCallback,this);
               
                    xhr.onload = function() {
                        cb(xhr.responseText);
                    };
                   
                    xhr.onerror = function() {
                        console.log("Error occurred getting location.  Please try again.");
                    };
                   
                    xhr.send();
                    
                } else {
                    
                    //Broadcast a topic that location has changed.
                    if (sp.coords.latitude) {
                        this.main.actions.perform({
                            cmd: "broadcast",
                            topic: "locateaddress"
                        });
                    }
                
                    console.log("CORS not supported. Cannot resolve location.");
                    return;
                }
               
            } else {
                //Broadcast a topic that location has changed.
                if (sp.coords.latitude) {
                    this.main.actions.perform({
                        cmd: "broadcast",
                        topic: "locateaddress"
                    });
                }
                console.log("Cannot determine address if offline.");
            }
        } else {
            console.log("Cannot determine location");
        }
    
    },

    
    
    
    addressCallback: function (rsp) {
        
        var data = JSON.parse(rsp);
        if (data.status=="OK") {
            var arr = data.results[0].address_components;
            for (var i=0,l=arr.length;i<l;i++) {
                var part = arr[i];
                var atype = part.types[0];
                this.settings[atype] = part.long_name;
                if (atype=="locality") this.settings["city"] = part.long_name;
                if (atype=="administrative_area_level_1") this.settings["state"] = part.long_name;
            }
            
            //Log it.
            this.main.cache.log(this.settings);

            //Broadcast a topic that location has changed.
            this.main.actions.perform({
                cmd: "broadcast",
                topic: "location"
            });
        }
    
    },
    
    
    locationerrorCallback: function (error) {
        /*------------------------
        // PositionError.PERMISSION_DENIED = 1;
        // PositionError.POSITION_UNAVAILABLE = 2;
        // PositionError.TIMEOUT = 3;
        */
        
        var errmessage = '';
        
        switch (error.code) {
            case 1:
               errmessage = "Permission denied.";
               break;
           case 2:
               errmessage = "Position unavailable.";
               break;
           case 3:
               errmessage = "Timeout waiting for position.";
               break;
       }
        
        console.log(errmessage);
        
    }
		
});