/**
 * This class is a helper class for the MainController.
 *
 * Maintains all functions related to sending data to the server.
 */

Ext.define('bravo.global.Server', {
    extend: 'Ext.app.Controller',

    alias: 'controller.server',
    
    requires: [
        
    ],
    
    setup: function () {
    
    
    },
    
    request: function (sp,arr,objects,log) {
        
        console.log("server request");
        
        if (sp.options) {
        
            //Options action tells how to build the request.
        
            switch (sp.options.action) {
                case 'get':
                    //Get actions that do not require writing data and must not be sent via ajax
                    this.get(sp,this.buildRequest(sp,log));
                    break;
                case 'task':
                    //Normal request with both get/put combined.  (task, read, write)
                    if (arr.length>0) {
                        //Write request with a valid payload.
                        this.read(sp,this.buildRequest(sp,log,this.put(arr,objects)));
                    } else {
                        //Don't do anything unless there is a valid read request.
                        if (sp.options.action=="task") this.read(sp,this.buildRequest(sp,log));
                    }
                    break;
                default:  //read or write
                    //Normal request with both get/put combined.  (task, read, write)
                    if (arr.length>0) {
                        //Write request with a valid payload.
                        this.read(sp,this.buildRequest(sp,log,this.put(arr,objects)));
                    } else {
                        //Don't do anything unless there is a valid read request.
                        if (sp.options.action=="read") this.read(sp,this.buildRequest(sp,log));
                    }
                    break;
            }
        
        }
    },
    
    
    read: function (sp,rq) {
        //Make a request to the server for data.
        
        console.log("server read");
        
        sp.start = new Date().valueOf();
        
        console.log(Ext.encode(rq));
        
        Ext.Ajax.request({
			url: sp.webservice||this.main.environment.get("WEB_SERVICE"),
            params: {
                data: Ext.encode(rq),
                format: 'json'
            },
            options: sp,
            timeout: this.main.environment.get("REQUEST_TIMEOUT"),
			method: "POST",
			success: function(rsp,req) {
                //console.log(rsp.responseText);  //good spot for permanent log
                console.log("success");
                this.successHandler(rsp,req);
            },
			failure: function(rsp,req) {
                //console.log(rsp.responseText);  //good spot for permanent log
				this.failureHandler(rsp,req);
			},
			scope: this
        });

    },
    
    get: function (sp,rq) {
        //debugger;
        var dh = Ext.DomHelper;
        var o = {
            tag: 'form',
            method: 'post',
            action: this.main.environment.get("WEB_SERVICE"),
            cn: [
                {
                    name: 'data',
                    type: 'hidden',
                    tag: 'input'
                },
                {
                    name: 'format',
                    type: 'hidden',
                    tag: 'input'
                }
            ]
        };
        var f = dh.append(Ext.getBody(),o);
        f.childNodes[0].value = Ext.encode(rq);
        f.childNodes[1].value = 'json';
        f.submit();
        Ext.getBody().dom.removeChild(f);
    },
    
    successHandler: function (rsp,req) {
        var sp = req.options;
        sp.finish = new Date().valueOf();
        var json = Ext.decode(rsp.responseText);
        this.receive(json,sp);
    },
    
    failureHandler: function (rsp,req) {
        var sp = req.options;
        this.main.feedback.say({
            title: 'Error',
            message: 'Unable to connect to server.'
        });
    },
    
    buildRequest: function (sp,log,payload) {
    
        console.log("server build request");

        //insert way to turn sp into a formal json request object.
        var obj = {
            data:{
            }
        };
        
        var o = {};

        /*
        var o = {};
        for (var item in sp) {
            if (this.main.util.isNotProperty(sp[item])) {
                //skip
            } else {
                //reserved parameters that should never be sent to server in read request.
                //set one is built-in object properties, set two is reserved client properties.
                if (this.main.util.isReadProperty(item)) {
                    //if (!/^(id|cmd|aq|usecache|cache|inventory|webservice|relationship)$/i.test(item)) {
                    if (!this.main.util.isReservedProperty(item)) {
                        //if (item.indexOf('_')==0) {
                        //if (this.main.util.isMetaProperty(item)) {
                            //meta properties in bravo are always preceeded by an underscore.
                            //if (!o.mt) o.mt = {};
                            //o.mt[item.substring(1)] = sp[item].toString();
                        //} else {
                            if ((Ext.isDefined(sp[item]))&&(sp[item])) o[item] = sp[item].toString();
                        //}
                    }
                }
            }
        }
        */
        
        //assign action to the request.
        switch (sp.options.action) {
            case 'get':
           
                //Parameters
                for (var item in sp) {
                    if ((!this.main.util.isReservedProperty(item))&&(!this.main.util.isNotProperty(sp[item]))) {
                        if ((Ext.isDefined(sp[item]))&&(sp[item])) o[item] = sp[item].toString();
                    }
                }
           
                //get action operations.
                obj.data.get = {object:[o]};
                break;
           
            case 'task':
           
                //Parameters
                for (var item in sp) {
                    if ((!this.main.util.isReservedProperty(item))&&(!this.main.util.isNotProperty(sp[item]))) {
                        if ((Ext.isDefined(sp[item]))&&(sp[item])) o[item] = sp[item].toString();
                    }
                }
           
                //write
                //must be added first so that data is written first.
                if (payload) {
                    if (payload.length>0) {
                        obj.data.write = {object: payload};
                    }
                }
           
                //task action operations.
                obj.data.task = {object:[o]};
           
                break;
            default:
           
                //Build request.
                for (var item in sp) {
                    if (!this.main.util.isNotProperty(sp[item])) {
                        if (this.main.util.isReadProperty(item)) {
                            if (Ext.isNumeric(sp[item])) {
                                o[item] = sp[item].toString();
                            } else {
                                if ((Ext.isDefined(sp[item]))&&(sp[item])) o[item] = sp[item].toString();
                            }
                        }
                    }
                }
           
                //write
                //must be added first so that data is written first.
                if (payload) {
                    if (payload.length>0) {
                        obj.data.write = {object: payload};
                    }
                }
           
                //read and write combo operations.
                if (sp.redirectsource||sp.redirectalias||sp.source||sp.sourcealias||sp.n||sp.a) obj.data.read = {object:[o]};
           
        }
        
        
        obj.data.session = this.main.entity.getSession();
        obj.data.device = "";  //insert native device id here.
        obj.data.latitude = this.main.environment.get("latitude");
        obj.data.longitude = this.main.environment.get("longitude");
        
        if (this.main.environment.get("LOG")) {
            if (log) obj.data.log = log;
        } else {
            obj.data.log = "off";
        }
        
        return obj;
    },
    
    
    put: function (arr,objects) {

        /*
        commit these items to the server.  If the item has no id, it is assumed to be new item and all properties are saved.
        if there is an id, the original in the queue table is compared with the latest one in the data table.  Only differences are saved.
        */
        
        //console.log("queue:" + Ext.encode(arr));
        //console.log("data:" + Ext.encode(objects));
        
        var changes = [];
        for (var i=0,l=arr.length;i<l;i++) {
            if (arr[i].i) {
                //Updates to existing objects. Only differences between cached version and queued version will be saved.
                changes.push(this.compare(arr[i],objects[arr[i].a]));
            } else {
                //New objects or existing objects that have not been round tripped to server yet.  Keep writing everything
                //back to make sure it gets saved before doing the differences.
                changes.push(this.build(objects[arr[i].a]));
            }
        }
        
        //console.log("differences:" + Ext.encode(changes));
        return changes;
    },
    
    
    
    
    compare: function (q,o) {
    
        //Return only properties in o that are different from q.  Return properly formatted object to write to the server.
        var obj = {a:o.a};
    
        for (var item in o) {
            if (!this.main.util.isReadOnlyProperty(item)) {
                
                //Meta properties
                if (this.main.util.isMetaProperty(item)) {
                    if (!obj.mt) obj.mt = {};
                    if (!this.compareItem(obj.mt[item.substring(1)],o[item])) {
                        if (obj.mt[item.substring(1)]!= o[item]) {
                            obj.mt[item.substring(1)] = this.revalue(o[item]);
                        }
                    }
                } else {
                
                    //Built-in tree properties
                    if (this.main.util.isTreeProperty(item)) {
                        if (this.main.util.isChangeableTreeProperty(item)) {
                            if (!this.compareItem(o[item],q[item])) {
                                obj[item] = this.revalue(o[item]);
                            }
                        } else {
                            //Some tree properties are required for creating/updating links whether they are changing or not.
                            obj[item] = this.revalue(o[item]);
                        }
                    } else {
                    
                        //Built-in object properties
                        if (this.main.util.isObjectProperty(item)) {
                            if (!this.compareItem(o[item],q[item])) {
                                obj[item] = this.revalue(o[item]);
                            }
                        } else {
                        
                            //Custom object properties
                            if (o.t==0) {
                                if (!this.compareItem(o[item],q[item])) {
                                    if (!obj.dt) obj.dt={};
                                    obj.dt[item] = this.revalue(o[item]);
                                }
                            }
                        }
                    }
                }
            }
        }
        return obj;
    },
    
    build: function (o) {
    
        //Return a properly formatted object to write to server.
        var obj = {a:o.a};
        
        for (var item in o) {
            if (!this.main.util.isReadOnlyProperty(item)) {
                
                //Meta properties
                if (this.main.util.isMetaProperty(item)) {
                    if (!obj.mt) obj.mt = {};
                    if (obj.mt[item.substring(1)]!= o[item]) {
                        obj.mt[item.substring(1)] = this.revalue(o[item]);
                    }
                } else {
                
                    //Built-in tree properties
                    if (this.main.util.isTreeProperty(item)) {
                        obj[item] = this.revalue(o[item]);
                    } else {
                    
                        //Built-in object properties
                        if (this.main.util.isObjectProperty(item)) {
                            obj[item] = this.revalue(o[item]);
                        } else {
                        
                            //Custom object properties
                            if (o.t==0) {
                                if (!obj.dt) obj.dt={};
                                obj.dt[item] = this.revalue(o[item]);
                            }
           
                        }
                    }
                }
            }
        }
 
        return obj;
    },
    
    revalue: function (mx) {
        //JSON values being sent to the server in a request should always be in string format.
        //Convert everything going back out to the server to a string.
        //Dates should be converted to iso 8601 format.
        //empty strings should be sent back as null
        
        if (Ext.isEmpty(mx)) mx="";
        if (mx=='null') return null;
        return (Ext.isDate(mx)) ? Ext.util.Format.date(mx,"Y-m-d H:i:s") : mx.toString();
    },
    
    compareItem: function (mx1,mx2) {
        if (Ext.isDate(mx1)) mx1 = mx1.getTime();
        if (Ext.isDate(mx2)) mx2 = mx2.getTime();
        return (mx1==mx2);
    },
    
    receive: function (json,rsp) {
        //Divide the response json into objects and events and handle appropriately.
    
        var objects = [];
        var events = [];

        //Gather events from the response for event handling and notification.
        rsp["success"] = "success";
        
        //Clear the log.
        this.main.cache.resetStore("log");
        
        //Read results
        if (json.data.read) {
            if (json.data.read.object) {
                objects = json.data.read.object;
            }
            if (json.data.read.event) {
                events = Ext.Array.merge(events,json.data.read.event);
            }
        }
        
        //Write results
        if (json.data.write) {
            if (json.data.write.event) {
                var arr = json.data.write.event;
                for (var i=0,l=arr.length;i<l;i++) {
                    this.main.cache.remove(arr[i].id);
                }
                events = Ext.Array.merge(events,json.data.write.event);
            }
        }
        
        //Task results
        if (json.data.task) {
            if (json.data.task.event) {
                events = Ext.Array.merge(events,json.data.task.event);
            }
        }
        
        //Login results
        if (json.data.login) {
            this.main.entity.receive(rsp,json.data.login.object);
            this.main.entity.setSession(json.data.session);
        }
        
        //Log server performance.
        if (this.main.environment.get("LOG")) {
            this.main.cache.log({
                cmd: 'performance',
                server: Math.floor(json.data.elapsed*10000)/10000,
                transport: Math.floor((((rsp.finish - rsp.start)/1000) - json.data.elapsed)*10000)/10000
            });
        }
        
        //Save all events for future reference.
        rsp.events = events;
        
        //Handle Events.
        this.main.render.handleEvents(events,rsp);
    
        //Handle Objects.
        this.main.data.receive(objects,rsp);
        
    }
    
});