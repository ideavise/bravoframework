/**
 * This class is a helper class for the MainController.
 *
 * Render engine.
 */

Ext.define('bravo.global.Render', {
    extend: 'Ext.app.Controller',

    alias: 'controller.render',
    
    requires: [
        
    ],
    
    
        
    
    //--------------------------------------
    // Render
    //--------------------------------------
    
    build: function (sp,items) {
    
        var data = this.main.data;
        
        //Get reference to main item being rendered.
        var a = sp.leaf.a;
        
        //By name (secondary).  Warning - if you use name for navigation it should be unique inside the app.
        //If is ok to use multiple items with same name for data, but not for navigation.  This will always just pull the first item
        //found with that name.
        if (!a) {
            if (data.names[sp.n]) a = data.names[sp.n][0].a;
        }
        
        if (a) {
            var page = data.map[a];
           
            if (page) {
                
                //Iterate over the descendants of page and attach to page.
                this.buildItem(page,sp);
               
                //Find the target.
                var view = this.getTargetView(sp);
               
                view.removeAll(true);
           
                var xtype = ((Ext.ClassManager.aliasToName["widget."+page.xtype])||(Ext.ClassManager.aliasToName[page.xtype])) ? page.xtype : 'widget.container';
           
                try {
                    var o = Ext.create(xtype,page);
                    view.add(o);
                } catch (e) {
                    console.log(e.message);
                }
           
                //Create a provider called activepage that can be used to refer to this page.
                this.main.data.providers['activepage'] = page.a;
           
                //Broadcast a topic that the activepage has changed.
                this.main.actions.perform({
                    cmd: 'topic',
                    topic: 'activepage'
                });
            }
        }
    },
    
    buildItem: function (item,sp) {
        
        //Insert all blueprint logic here.
        if (item.items) {
            for (var i=0,l=item.items.length;i<l;i++) {
                this.buildItem(item.items[i],sp);
            }
        }
    },
    
    
    reload: function () {
    
        var page = this.main.data.map[this.main.environment.get("activepage")]
        
        var view = this.main.view;
        
        this.buildItem(page);
        
        view.removeAll();
        view.add(Ext.create(page.xtype,page));
    },
    
    handleEvents: function (events,rsp) {
    
        //Events have come back from server response.  Render a response back to the user based on type of event/event settings.
        var messages = [];
        
        for (var i=0,l=events.length;i<l;i++) {
            if ((events[i].type=="error")&&(rsp.success=='success')) rsp.success = "error";
            if ((events[i].type=="notice")&&(rsp.success=='success')) rsp.success = "notice";
            messages.push(events[i].message);
        }
        
        if (rsp.success!="success") {
            if (messages.length>0) {
                //TODO: Must develop feedback class that encapsulates differences between classic and modern.
                this.main.feedback.say({
                    title: "Info",
                    message: messages.join(" ")
                });
            }
        }
    },
    
    getTargetView: function (sp) {
        if (sp.target) {
            //Target is the name of a component
            var selector = "*[name='" + sp.target + "']";
            var view = this.main.view.down(selector);
            return view||this.main.view;
        } else {
            return this.main.view;
        }
    }

});