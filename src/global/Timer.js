/**
 * This class is a helper class for the MainController.
 *
 * Creates an idle timer for the application.
 */

Ext.define('bravo.global.Timer', {
    extend: 'Ext.app.Controller',

    alias: 'controller.timer',
    
    requires: [
        
    ],
    
    config: {
		timer: null,
        count: 0,
        limit: 0
	},
    
    setup: function (limit) {
        this.setLimit(limit||this.main.environment.get("IDLE_SECONDS"));
        this.setTimer({
            run: function() {
                this.tick();
            },
            interval: 1000,
            scope: this
        });

        //start
        Ext.util.TaskManager.start(this.getTimer());
    },
    
    tick: function () {
        this._count++;
        if (this._count>=this._limit) {
            this.main.actions.perform({cmd:"idle"});
        }
    },
    
    reset: function () {
        this._count = 0;
    }

});