/**
 * This class is a helper class for the MainController.
 *
 * Data engine.  Variables stored in local memory that are used for all application logic and rendering.
 
 
 References to data:
 
 //Pull data directly from the objects in the query.
 main.data.filter(rsp.descendants,"cid EQ class");
 
 //Pull children from the children map.  Issue is that these are objects not arrays and are cached in the indexedDB cache.
 main.data.objectToArray(me.main.data.children['toolbox']);
 
 //Get array of children from an item without needing conversion.
 main.data.objects['toolbox'].children;
 
 */

Ext.define('bravo.global.Data', {
    extend: 'Ext.app.Controller',

    alias: 'controller.data',
    
    requires: [
        
    ],
    
    //--------------------------------------
    // Setup the iDB data engine and data environment.
    //--------------------------------------
    setup: function () {
    
        //Collections for data manipulation
        this.objects = {}; //primary storage by alias. -- used for lookups
        this.map = {};  //tree structure -- used for rendering UI.   has tree items
        this.parents = {}; //record of parent. -- used to lookup parent.  alias only
        this.stepparents = {}; //record of step parents -- used to lookup all things that item is linked to.
        this.children = {}; //record of children (unordered list) use map[a].children for ordered list.
        this.cores = {}; //lookup to sync changes between objects of different relationships.
        this.names = {};  //name lookup reference -- used to lookup name.
        this.classes = {};  //class reference -- used to get schema information.
        this.roles = {};  //permissions lookup
        this.providers = {}; //data providers.

    
    },
    
    //--------------------------------------
    // All Data Requests Start Here
    //--------------------------------------
    
    request: function (sp) {
        
        var obj = Ext.apply(sp,{
            component: sp.component||this.main.render,
            callback: sp.callback||Ext.bind(this.main.render.build,this.main.render)
        });
        obj = Ext.applyIf(obj,this.main.util.getRelationshipRequest(sp.relationship||"children"));
    
        // read/write combination actions.
        console.log("data request: " + (sp.redirectsource||sp.redirectalias||sp.source||sp.sourcealias||sp.n||sp.a));
        
        if (sp.redirectsource||sp.redirectalias||sp.source||sp.sourcealias||sp.n||sp.a) {
        
            var options = {
                options: {
                    action: 'read'
                }
            }
            
            //TODO: sp should have a flag for reload.  If that is true, then
            //need to completely unmap the item and it's children.
            //it should also have a cache flag that can tell the client to ignore built in cache minutes setting and use this one intead
            //should be put into an opts object attached to request.
            
            this.main.cache.get(this.validate(Ext.apply(sp,options)));
        
        } else {
            this.main.feedback.say({
                title: "Error",
                message: "Error occurred during request: no item specified"
            });
            //Exit to callback.
            Ext.callback(sp.callback,sp.component,[sp]);
        }
        
    },
    
    get: function (sp) {
        //get item from the server.
        
        var options = {
            options: {
                action: 'get'
            }
        }
        this.main.cache.buildRequest(Ext.apply(sp,options),[]);
    },
    
    task: function (sp) {
        //perform immediate task on server.
        var options = {
            options: {
                action: 'task'
            }
        }
        this.main.cache.buildRequest(Ext.apply(sp,options),[]);
    },
    
    save: function (sp) {
        //perform immediate task on server.
        var options = {
            options: {
                action: 'write'
            }
        }
        this.main.cache.buildRequest(Ext.apply(sp,options),[]);
    },
    
    
    
    validate: function (sp) {
        //All requests need a valid id, alias or name search.
        //If user specifies a string (non-numeric), check to see if it is a named item and convert to id instead.
        //Removed for now.
        //var i = sp.i;
        //if (!Ext.isNumeric(i)) {
           //var names = this.names[i];
           //if (names) sp.i = names[0].i;
        //}
        return sp;
    },
    
    
    
    //--------------------------------------
    // All Data Write Operations Start Here
    //--------------------------------------
    
    put: function (sp) {
    
        var objects = sp.objects;
        var result=false;
        
        //Commit data.
        for (var i=0,l=objects.length;i<l;i++) {
        
            var o = this.main.data.getObject(objects[i].a);

            if (o) {
           
                var arr = this.main.data.cores[objects[i].a];
           
                //Commit same data to each instance of the item.
                for (var j=0,l2=arr.length;j<l2;j++) {
           
                    //Get raw object.
                    var o2 = this.main.data.getObject(arr[j].a);
           
                    //Update core objects.
                    var result = this.updateObject(o2,sp);
           
                    //Update the tree record only if the base item being changed is the specific item matching that alias.
                    if (o2.a==o.a) {
                        var nr = this.updateTreeObject(o2,sp);
                        result = result||nr;
                    }
           
                    if (result) {
                        //Commit the change to the cache
                        this.main.cache.put(o2);
           
                
                    } else {
                        //Don't commit any changes if nothing is different.
                    }
           
                }
           
                //Event
                this.main.actions.perform(Ext.apply(Ext.apply({},sp),{cmd:"datachange",dataprovider:o}));
           
            } else {
                console.log("can't find object you are trying to change.");
            }
        }
    },
    
           
    createLink: function(sp) {
    
        //Starting place for all new relationship links created.
        
        if (!sp.data) sp.data = [{}];
        
        if ((sp.parent)&&(sp.child)) {
            //create a relationship between this parent and this child.
           
            //Single object in an array that will now become actual object in the system.
            for (var i=0,l=sp.data.length;i<l;i++) {
           
                //First apply all values from the child object to new aliases object.
                Ext.apply(sp.data[i],this.main.data.getObject(sp.child.a));
           
                //Default values
                Ext.apply(sp.data[i],{
                    pa: sp.parent.a,
                    ca: sp.child.a,
                    a: sp.a||(sp.parent.a + '~' + sp.child.a),
                    t: sp.t||1
                });
            }
           
            //Settings provider
            this.create(Ext.apply({},{
                callback: sp.callback||Ext.emptyFn,
                component: sp.component,
                scope: sp.scope||this,
                temporary: sp.temporary,
                data: sp.data
            }));
        } else {
            this.main.feedback.say({
                title: 'Error',
                message: 'Cannot create relationship without parent and child specified.'
            });
        }
    },
    
    create: function (sp) {
        //Create a new object.
        
        if (!sp) sp={};
        Ext.applyIf(sp,{
            callback: Ext.emptyFn,
            scope: this
        },sp);
        
        if (!sp.data) return;
        
        //Setup array to contain all created objects.
        var createdobjects = [];
        
        //Build objects to be created.
        for (var i=0,l=sp.data.length;i<l;i++) {
            var obj = sp.data[i];
           
            //Remove any complex functions or objects from the item being created.
            for (var item in obj) {
                if (this.main.util.isNotProperty(obj[item])) {
                    delete obj[item];
                }
            }
           
            //Make sure default template is enforced on all new objects.
            obj = (obj.t>0) ? this.buildRelationshipObject(obj) : this.buildObject(obj);
           
            //Add to list of new objects.
            createdobjects.push(obj);
           
            //Commit this object to the queue for next server request.  Temporary objects are only added to RAM map, not to cache.
            console.log("put new item in cache/queue.");
            if (!sp.temporary) this.main.cache.put(obj);
        }

        //Prepare objects for storage in app.
        this.prestore(createdobjects);

        //Add to local RAM for use in application.
        var rawobjects = this.inventory(createdobjects,sp);
           
        //Add to local RAM map that links everything together.
        var refinedobjects = this.refine(rawobjects,sp);
        
        //Add created objects to response so it is ready for callback.
        sp.objects = refinedobjects;
       
        //Exit to callback.
        Ext.callback(sp.callback,sp.component,[sp]);
        
    },
    
    getObjectAlias: function (o) {
        //Normal objects.
        //Need to create an alias based on user and timestamp.
        //class.userid.yyyy.mm.dd.hh.mm.ss.ttt
        var user = this.main.entity.getUser();
        var now = new Date();
        var ds = [now.getUTCFullYear(),now.getUTCMonth()+1,now.getUTCDate(),
        now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds()].join('.');
    
        return [(o.cid||'object'),user.i,ds].join('.')
    },
    
    buildObject: function (o) {
        return Ext.apply({
            l:o.l||'New Item',
            a: o.a||this.getObjectAlias(o),
            t:o.t||0,
            z:o.z||0,
            cid:o.cid||'object',
            cr: this.main.util.dateToISO8601(new Date())
        },o);
    },
    
    buildRelationshipObject: function (o) {
        return Ext.apply({
            t:o.t||1
        },o);
    },
    
    updateObject: function (o,sp) {
        var dirty = false;
        for (var item in sp) {
            if ((!this.main.util.isReservedProperty(item))&&(!this.main.util.isNotProperty(sp[item]))) {
                if ((!this.main.util.isTreeProperty(item))&&(!this.main.util.isMetaProperty(item))) {
                    if (o[item]!=sp[item]) {
                        o[item] = sp[item];
                        dirty = true;
                    }
                }
            }
        }
        return dirty;
    },
    
    updateTreeObject: function (o,sp) {
        var dirty = false;

        for (var item in sp) {
            if ((!this.main.util.isReservedProperty(item))&&(!this.main.util.isNotProperty(sp[item]))) {
                if ((this.main.util.isTreeProperty(item))||(this.main.util.isMetaProperty(item))) {
                    if (o[item]!=sp[item]) {
                        o[item] = sp[item];
                        dirty = true;
                    }
                }
            }
        }
        return dirty;
    },
    
    
    //--------------------------------------
    // Receive Data
    //--------------------------------------
    
    receive: function (serverobjects,rsp) {
        //Data has been received from the server.
        
        //Prepare objects for storage in app.
        this.prestore(serverobjects);
        
        //Storage
        if (serverobjects.length>0) {
       
            //Add to iDB offline storage. (Delayed so it will happen after inventory.)
            Ext.defer(function() {this.main.cache.store(serverobjects,rsp)}, 1200, this);
           
            if (rsp.inventory!=false) {
           
                //Add to local RAM for use in application.
                var rawobjects = this.inventory(serverobjects,rsp);
               
                //Add to local RAM map that links everything together.
                var refinedobjects = this.refine(rawobjects,rsp);
           
                //Add refined objects to the response so it is ready for the callback.
                this.build(refinedobjects,rsp);
           
            } else {
           
                this.build(serverobjects,rsp);
            }
       
        } else {
            //No objects in response.
        }
           
           
        //Exit to callback
        Ext.callback(rsp.callback,rsp.component,[rsp]);
        
    },
    
    restore: function (cacheobjects,rsp) {
        //Data has been received from the cache.

        if (cacheobjects.length>0) {
        
            if (rsp.inventory!=false) {
    
                //Add to local RAM for use in application.
                var rawobjects = this.inventory(cacheobjects,rsp);
               
                //Add to local RAM map that links everything together.
                var refinedobjects = this.refine(rawobjects,rsp);

           
                //Add refined objects to the response so it is ready for the callback.
                this.build(refinedobjects,rsp);
           
            } else {
           
                this.build(cacheobjects,rsp);
           
            }
       
        } else {
            //No objects in response.
        }
           
        //Exit to callback
        Ext.callback(rsp.callback,rsp.component,[rsp]);
        
    },
    
    
    //--------------------------------------
    // Prerender Processes
    //--------------------------------------
    
    prestore: function (objects) {
        //Convert objects into format used by application.
        //Everything inside the app including cache and local memory will understand the objects in this format.
        for (var i=0,l=objects.length;i<l;i++) {
            objects[i] = this.revalue(this.merge(objects[i]));
           
            //Role assignment from server requests.
            this.roles[objects[i].a] = objects[i].r;
        }
    },
    
    treestore: function (o) {
        //Add these properties for use in a tree component.
        o.text = o.l||'';
        o.leaf = true;
        return o;
    },
    
    merge: function (o) {
        //Build a new object that merges dt (custom properties) to the root object.
        //Also meta properties are on the core object but are prefaced by an underscore.
       
        var obj = Ext.apply({},o);
        
        //Normal properties
        obj = Ext.apply(obj,(o.dt||{}));
        delete obj.dt;
        
        //Meta properties.
        if (obj.mt) {
            for (var item in obj.mt) {
                obj['_'+item] = obj.mt[item];
            }
        }
        delete obj.mt;
        
        return obj;
    },
    
    revalue: function (o) {
        //Convert all values from strings to native datatypes.
        //Skip certain properties that should always be interpreted as string.
        var util = this.main.util;
        var properties = util.buildArray('a,n,l,cid,utc,ct,pa');
        var re = new RegExp("^(?:" + properties.join('|') + ")$","i");
        for (var item in o) {
            if (!re.test(item)) o[item] = util.revalue(o[item]);
        }
        return o;
    },
    

    inventory: function (objects,rsp) {
        
        //Creating a new copy of the objects separate from the ones that were stored as raw data in the cache.
        var arr = [];
    
        //Place all objects into RAM storage collections.
        for (var i=0,l=objects.length;i<l;i++) {
            var o = Ext.apply({},objects[i]);
            if (!this.objects[o.a]) {
                this.store(o);
            } else {
                if (o.utc>this.objects[o.a].utc) {
                     this.store(o);
                }
            }
            arr.push(o);
        }

        return arr;
    },
    
    refine: function (rawobjects,rsp) {
    
        //Store in refined objects based collections: map,children.  These are used for rendering ui, trees, dataviews, etc.
        var arr = [];
    
        //Create a basic map entry for each item.
        for (var i=0,l=rawobjects.length;i<l;i++) {
        
            //This is the core creation of all objects that are rendered/put into trees.
            //This statement makes sure that refined objects are separate from raw objects.
           
            //If this item has already been mapped, just merge objects onto it.
            //If this item has never been mapped, create an empty record.
            var o = this.map[rawobjects[i].a]||{};
           
            //Merge all properties from the raw object into the refined object.
            Ext.apply(o,rawobjects[i]);
           
            //refined objects have leaf/text nodes set for trees.
            o = this.treestore(o);
           
            //handle any special cases.
            o = this.register(o);
           
           
            //Add base object to map,children,classes,names,cores
            if (!this.map[o.a]) this.map[o.a] = o;
            if (!this.children[o.a]) this.children[o.a] = {};
           
            //names
            if (o.n) {
                if (!this.names[o.n]) this.names[o.n] = [];
                this.names[o.n].push(o);
            }
           
            //classes
            if (!this.classes[o.cid]) this.classes[o.cid] = {};
            this.classes[o.cid][o.a] = o;
           
            //cores
            if (!this.cores[o.ca]) this.cores[o.ca] = [];
            this.cores[o.ca].push(o);
           
            //Add to array that gets pushed to callback.
            arr.push(o);
        
            //Do not map this object unless its parent has been already recognized.
            if (this.objects[o.pa]) {
           
                //Type of relationship
                switch(o.t) {
                    case 7:
                        //Favorites
                        if (!this.main.entity.favorites) this.main.entity.favorites = {};
                        this.main.entity.favorites[o.a] = o;
                        break;
                        
                    default:
           
                        //Initialize
                        if (!this.map[o.pa]) {
                            this.map[o.pa] = {};
                            this.children[o.pa] = {};
                        }
                   
                        //Map children and prepare for use with rendering/tree store data.
                        if (!this.children[o.pa][o.a]) {
                        
                            //Items/Docked Items - Children that can be rendered on the DOM tree.
                            if ((Ext.ClassManager.aliasToName["widget."+o.cid])||(Ext.ClassManager.aliasToName[o.cid])) {
                                if ((o.dock)&&(/^(xpanel|xwindow|xcontainer)$/.test(this.map[o.pa].xtype))) {
                                    if (!this.map[o.pa].dockedItems) this.map[o.pa].dockedItems = [];
                                    this.map[o.pa].dockedItems.push(this.prerender(o));
                                } else {
                                    if (!this.map[o.pa].items) this.map[o.pa].items = [];
                                    try {
                                    this.map[o.pa].items.push(this.prerender(o));
                                    } catch (e) {
                                        //debugger;
                                    }
                                }
                            }
                   
                            //Chidren - Used for a tree store to render items in a tree structure even if there is no visible component.
                            if (!this.map[o.pa].children) this.map[o.pa].children = [];
                            this.map[o.pa].children.push(o);
                            this.children[o.pa][o.a] = o;
                        }
                        this.map[o.pa].leaf = false;
                }
            }
        }
        
        return arr;
    },
    

    build: function (arr,rsp) {
        //Attach objects and events to the response in groups for use by rendering components etc.
        
        var delimiter;
        rsp.anc = [];
        rsp.desc = [];

        for (var i=0,l=arr.length;i<l;i++) {
            var o = arr[i];
            var method = (rsp.redirectsource||rsp.source||rsp.n) ? 'n' : 'a';
            if (((method=='a')&&((rsp.redirectalias==o.a)||(rsp.sourcealias==o.a)||(rsp.a==o.a)))||((method=='n')&&(o.n)&&((rsp.redirectsource==o.n)||(rsp.source==o.n)||(rsp.n==o.n)))) {
                rsp.leaf = o;
                delimiter = true;
            } else {
                if (delimiter) {
                    rsp.desc.push(o);
                } else {
                    rsp.anc.push(o);
                }
            }
        }
    },
    
    store: function (o) {
        
        //Place into raw object based data collections: objects,classes,names,lookups,parents
        this.objects[o.a] = o;
        if (o.t==0) this.parents[o.a] = o.pa;
        if (o.t>0) {
            var a = (o.a.indexOf('~')>0) ? o.a.split('~')[1] : a;
            if (!this.stepparents[a]) this.stepparents[a] = {};
            this.stepparents[a][o.pa] = o.t;
        }
    },


    prerender: function (o) {
        //Set up the object with properties needed to support visual components.
        if (!o) return;
        
        
        //Custom layout if provided
        if (o.layouttype||o.layoutalign||o.layoutpack||o.layoutdirection) {
            o.layout = this.getLayout(o);
        }
        
        var arr = ["x-"+o.cid,"b-"+(o.n||o.a)];
        if (o.cls) arr.push(o.cls);

        //Default properties for any item rendered.  ApplyIf so that if the custom data has this property it won't override it.
        return (Ext.applyIf(o,{
            name: o.n||o.a,
            label: o.l||'',
            xtype: (o.cid||"container"),
            cls: arr
        }));
    },
    
    getLayout: function (o) {
        return {
            type: o.layouttype||"auto",
            align: o.layoutalign,
            pack: o.layoutpack,
            direction: o.layoutdirection
        };
    },
    
    register: function (o) {
        switch (o.cid) {
            case "rule":
                //All rules with actions that don't need components should be subscribed to. When the containing component is added to screen,
                //the event will be resubscribed with the component attached.
                if(/^(complete)$/.test(o.action)) this.main.rules.subscribe(o)
                break;
        }
        return o;
    },
    
    
    
    //--------------------------------------
    // Permissions
    //--------------------------------------
    
    getRole: function (mx) {
        var o = this.objects[(Ext.isObject(mx) ? mx.a : mx)];
        return this.roles[this.getRoleDomain(o).a];
    },
    
    getRoleDomain: function (mx) {
        //return the domain in the ancestry of item with the highest role assigned for this user.
        var o = this.objects[(Ext.isObject(mx) ? mx.a : mx)];
        if (!o) return 0;
        var role = this.roles[o.a]||0;
        var domain = mx;
        while (this.parents[o.a]) {
            o = this.objects[this.parents[o.a]];
            if (o) {
                if (this.roles[o.a]) {
                    if (this.roles[o.a]>role) {
                        role = this.roles[o.a];
                        domain = o;
                    }
                }
            } else {
                break;
            }
        }
        return domain;
    },
    

    /*
    getEditRole: function(mx) {
        //Find the role to the specific item or to the overall project. This is in case the user hasn't been explicitly assigned
        //to this item.
        var role = this.getRole(mx);
        var projectrole = this.getRole(this.main.environment.get("PROJECT"));
        return (projectrole>role) ? projectrole : role;
    },
    
    getRole: function (mx) {
        //Find the highest role assigned to any item in an item's ancestry.
        var o = this.objects[(Ext.isObject(mx) ? mx.a : mx)];
        if (!o) return 0;
        var role = this.roles[o.a]||0;
        while (this.parents[o.a]) {
            o = this.objects[this.parents[o.a]];
            if (o) {
                if (this.roles[o.a]) {
                    if (this.roles[o.a]>role) role = this.roles[o.a];
                }
            } else {
                break;
            }
        }
        return role;
    },
    */

    
    //--------------------------------------
    // Data Operations
    //--------------------------------------
    
    getObject: function (mx) {
        return this.objects[(Ext.isObject(mx)) ? mx.a : mx];
    },
    
    getObjectByName: function (mx) {
        //use name of object to find it.  Beware that names can be duplicated so do not call it unless you know there is only one or you
        //are willing to just accept the first one found.
        var o = this.names[(Ext.isObject(mx)) ? mx.n : mx];
        if (o) return o[0];
        return;
    },
    
    getClass: function(mx) {
        //lookup class object using the classid identifier.  Must use name to look it up.
        var o = this.names[(Ext.isObject(mx)) ? mx.cid : mx];
        if (o) return o[0];
        return;
    },
    
    getParent: function (mx) {
        return this.objects[this.parents[(Ext.isObject(mx)) ? mx.a : mx]];
    },
    
    getChildren: function (mx) {
        //Returns children of item sorted in z order.
        var zsort = function(a, b) {
            return (a.z - b.z);
        };
        if (!Ext.isObject(mx)) mx = this.getObject(mx);
        if (!mx) return [];
        if (this.map[mx.a]) {
            var arr = this.map[mx.a].children||[];
            return arr.sort(zsort);
        } else {
            return [];
        }
    },

    getAncestors: function (mx) {
        var arr = [];
        var parent = this.getParent(mx);
        while (parent) {
            arr.push(parent);
            parent = this.getParent(parent);
        }
        return arr;
    },
    
    getDescendants: function(mx,filter) {
        if (!Ext.isObject(mx)) mx = this.getObject(mx);
        if (!mx) return [];
        var arr = [];
        var children = [];
        
        //If exclude property is provided nothing in that descendancy will be included
        //If exclude property and a value is provided, if the item matches that it will be excluded from the descendancy.
        if ((filter)&&(filter.exclude)&&(filter.generation)) {
            if (filter.value) {
                if (!mx[filter.exclude]==val) children = this.getChildren(mx);
            } else {
                if (!mx[filter.exclude]) children = this.getChildren(mx);
            }
        } else {
            children = this.getChildren(mx);
        }
        
        //if class is provided, the final list will only include items in this class.
        if ((filter)&&(filter.cid)) {
            if (filter.cid.indexOf(mx.cid)>=0) arr.push(mx);
        } else {
            arr.push(mx);
        }
        
        //Recursive
        if (!filter) filter = {};
        if (!filter.generation) filter.generation = 0;
        filter.generation++;
        for (var i=0,l=children.length;i<l;i++) {
            arr = arr.concat(this.getDescendants(children[i],filter));
        }
        return arr;
    },
    
    
    
    filter: function(arr, string, overwrite) {
		//Given an array of objects and a filter string.  Return only the items that pass that filter.
		//TODO, currently can only create 1 filter per field.  Must use like, in or between to combine two conditions within a single field.
        if (Ext.isEmpty(string)) {
            return arr;
        }
		if (!arr) return null;

        var obj = {},
            pairs = string.split(' AND '),
            n=null,v;
		obj.ops = {};
		obj.pairs={};
		obj.validate = function (val) {
            //make sure that numbers evaluate as a number for comparison.
			if (Ext.isNumeric(val)) return parseFloat(val);
			if(/^(true|false)$/.test(val)) val = eval(val); //Force true and false to be evaluated as such.
			return val;
		};
			
        Ext.each(pairs, function(pair) {
			var p;
            p = pair.split(' EQ ');
			if (p.length>1) {
				n = obj.validate(p[0]);
				v = obj.validate(p[1]);
				obj.ops[n]="EQ";
            	obj.pairs[n] = overwrite || !obj.pairs[n] ? v : [].concat(obj.pairs[n]).concat(v);
			}
			p = pair.split(' NEQ ');
			if (p.length>1) {
				n = obj.validate(p[0]);
				v = obj.validate(p[1]);
				obj.ops[n]="NEQ";
            	obj.pairs[n] = overwrite || !obj.pairs[n] ? v : [].concat(obj.pairs[n]).concat(v);
			}
			p = pair.split(' LT ');
			if (p.length>1) {
				n = obj.validate(p[0]);
				v = obj.validate(p[1]);
				obj.ops[n]="LT";
            	obj.pairs[n] = overwrite || !obj.pairs[n] ? v : [].concat(obj.pairs[n]).concat(v);
			}
			p = pair.split(' GT ');
			if (p.length>1) {
				n = obj.validate(p[0]);
				v = obj.validate(p[1]);
				obj.ops[n]="GT";
            	obj.pairs[n] = overwrite || !obj.pairs[n] ? v : [].concat(obj.pairs[n]).concat(v);
			}
			p = pair.split(' IN ');
			if (p.length>1) {
				n = obj.validate(p[0]);
				v = obj.validate(p[1]);
				obj.ops[n]="IN";
            	obj.pairs[n] = overwrite || !obj.pairs[n] ? v : [].concat(obj.pairs[n]).concat(v);
			}
			p = pair.split(' LIKE ');
			if (p.length>1) {
				n = obj.validate(p[0]);
				v = obj.validate(p[1]);
				obj.ops[n]="LIKE";
            	obj.pairs[n] = overwrite || !obj.pairs[n] ? v : [].concat(obj.pairs[n]).concat(v);
			}
			p = pair.split(' BETWEEN ');
			if (p.length>1) {
				n = obj.validate(p[0]);
				v = obj.validate(p[1]);
				obj.ops[n]="BETWEEN";
            	obj.pairs[n] = overwrite || !obj.pairs[n] ? v : [].concat(obj.pairs[n]).concat(v);
			}
			
        },this);

        return arr.filter(function(o) {
			var valid=true;
			for (var item in obj.pairs) {
                var val = o[item]||null;
                //if (o.pr) val = val||o.pr[item];
                //if (o.meta) val = val||o.meta[item];
				
				if (o[item]==0) val = 0; //don't allow 0 to be interpreted as null;
				if (parseFloat(val)+''== val) val = parseFloat(val); //Force numbers to be evaluated as such.
		
				var op = this.ops[item];
				var q = this.pairs[item];
				
				//If val is undefined, the LT and GT comparisons don't work.  May need to add some functions to convert undefined to 0.
				try {
					if (op=='EQ') valid = valid && (val==q);
					if (op=='NEQ') valid = valid && (val!=q);
					if (op=='LT') valid = valid && (val<q);
					if (op=='GT') valid = valid && (val>q);
					if (op=='IN') valid = valid && (q.indexOf(val)>=0);
					if (op=='LIKE') valid = valid && (val.indexOf(q)>=0);
					if (op=='BETWEEN') {
						try {
						var v= q.split(",");
						v1 = v[0];
						v2 = (v.length>0) ? v[1] : null;
						valid = valid && ((val>=v1)&&(val<=v2));
						} catch(err) {
							valid=false;
						}
					}
				} catch (err) {valid=false;}
			}
			return valid;
		},obj);
		
    },
    
    //TODO-- fix this function.
    sort: function(arr,s) {
		var dir='asc';
		
		//Ascending
		var alphasort = function(a, b) {
			var c = a[pr]||'';
			var d = b[pr]||'';
			c = c.toString().toLowerCase();
			d = d.toString().toLowerCase();
			if (c<d) return -1;
			if (c>d) return 1;
			return 0;
		};
		var valuesort = function(a, b) {
			var c = a[pr]||'';
			var d = b[pr]||'';
			if (c<d) return -1;
			if (c>d) return 1;
			return 0;
		};
		//Descending
		var alphasortdesc = function(a, b) {
			var c = a[pr]||'';
			var d = b[pr]||'';
			c = c.toString().toLowerCase();
			d = d.toString().toLowerCase();
			if (c>d) return -1;
			if (c<d) return 1;
			return 0;
		};
		var valuesortdesc = function(a, b) {
			var c = a[pr]||'';
			var d = b[pr]||'';
			if (c>d) return -1;
			if (c<d) return 1;
			return 0;
		};
        
        //Sort
		var parr = s.split(',');
		for (var i=0,l=parr.length;i<l;i++) {
			var sortitem = parr[i];
			var so = sortitem.split(' ');
			if (so.length>1) {
				dir = so[so.length-1].toLowerCase();
				if ((dir!='asc')&&(dir!='desc')) {
					pr = so.join('');
				} else {
					so.pop();
					pr = so.join('');
				}
			} else {
				pr = so;
			}
			
            var sample = (arr[0]) ? arr[0][pr]||'a' : {};
           
			var sorttype= ((Ext.isNumeric(sample)||(Ext.isDate(sample)))) ? 'value' : 'alpha';
			if (dir=='asc') {
				//Ascending 0->9,a->z,early->late
				arr = (sorttype=='value') ? arr.sort(valuesort) : arr.sort(alphasort);
			} else {
				//Descending
				arr = (sorttype=='value') ? arr.sort(valuesortdesc) : arr.sort(alphasortdesc);
			}
		}
		return arr;
	},
    
        
    
    //--------------------------------------
    // Utilities
    //--------------------------------------
    
    getValue: function (mx) {
        var v = mx.toString();
        
        //{datasource$property} or {datasource}
        if ((v.substr(0,1)=='{')&&(v.substr(v.length-1)=='}')) {
        
            debugger;
        
            //Remove the brackets.
            v = v.slice(1,-1);
           
            //{provider$property} is a variable that looks up that property from that provider and applies it instead.
            var prv = v.split('$')[0];
            var pr = v.split('$')[1];
           
            switch (prv) {
           
                case "user":
                    //user
                    return this.main.entity.getUser()[pr||'a'];
                    break;
                case "environment":
                    //environment var
                    return this.main.environment[pr];
                    break;
           
                default:
                    //normal dataprovider
           
                    //Get the alias of the provider from the subscriptions.
                    var a = this.main.data.providers[prv];
                    if (a) {
                        //Look up the data object.
                        var obj = this.getObject(a);
                        if ((obj)&&(obj[pr])) return obj[pr];
                    }
                    break;
            }
        }
                        
        //If not recognized just return raw value.
        return mx;
    
    },
    
    k: function (v) {
        //Make sure items are referenced as objects not arrays.
        return v.toString();
    },
    
    parentKey: function (o) {
        return (o.p) ? o.p.toString() : o.p;
    },
    
    objectToArray: function (o) {
        var arr = [];
        for (item in o) {
            arr.push(o[item]);
        }
        return arr;
    }

});