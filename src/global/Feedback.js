/**
 * This class is a helper class for the MainController.
 *
 * Manages all feedback to the user.
 */

Ext.define('bravo.global.Feedback', {
    extend: 'Ext.app.Controller',

    alias: 'controller.feedback',
    
    requires: [
        
    ],
    
    config: {

	},
    
    setup: function () {
       
    },
    
    say: function (sp,cmp) {
        //title,message,buttons,icon,fn(callback)
        var obj = {
            title: sp.title||'Info',
            message: sp.message||'',
            buttons: sp.buttons||Ext.Msg.OK,
            icon: sp.icon||Ext.Msg.INFO,
            fn: sp.callback||Ext.emptyFn,
            scope: sp.scope||this
        };
        if (sp.buttonText) obj.buttonText = sp.buttonText;
        
        if (cmp) {
            var msgBox = cmp.add(new Ext.window.MessageBox());
            msgBox.show(obj);
        } else {
            Ext.Msg.show(obj);
        }

    },
    
    ask: function (sp,cmp) {
        
        if (cmp) {
            var msgBox = cmp.add(new Ext.window.MessageBox());
            msgBox.prompt(sp.title,sp.message,sp.callback);
        } else {
            Ext.Msg.prompt(sp.title,sp.message,sp.callback);
        }
        
    }
});