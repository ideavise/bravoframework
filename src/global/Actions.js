/**
 * This class is a helper class for the MainController.
 *
 * Maintains actions.
 */

Ext.define('bravo.global.Actions', {
    extend: 'Ext.app.Controller',

    alias: 'controller.actions',
    
    requires: [
        
    ],
    
    //--------------------------------------
    // All Actions Start Here
    //--------------------------------------
    
    perform: function (mysp) {
    
        console.log("perform " + mysp.cmd);
        
		//Reset the idle timer everytime something is done.
		this.main.timer.reset();
        
        //Log it.
        
        if (this.main.environment.get("LOG")) {
            this.main.cache.log(mysp);
        }
        
		//Build a notification provider
		var np = Ext.apply({},mysp);
        
        //Handler
        var h = mysp.cmd+"Handler";
		
		try {
            //Built-in Actions
            if (this[h]) this[h](mysp);
           
            //Custom Actions
            if (this.main.custom[h]) this.main.custom[h](mysp);
           
		} catch (e) {
            console.log(e.message);
        }
        
    },
    

    
    //--------------------------------------
    // Core Data Actions
    //--------------------------------------
    //These actions interact with the server and/or cache to get/put data.
    
    //Data Requests
    requestHandler: function (sp) {
        this.main.data.request(sp);
    },
    
    datachangeHandler: function (sp) {
        //Data component has changed its data.
        var cmp = sp.component;
        var prv = this.main.data.providers[cmp.provider];
        var o = this.main.data.getObject(prv);
        if (o) {
            var sp = {
                cmd:'put',
                objects:[o]
            };
            if (cmp.n) sp[cmp.n] = cmp.getValue();
            this.perform(sp);
        }
        
        //Broadcast events.
        this.rules.broadcast({
            component: cmp
        });
    },
    
    //Update data.
    putHandler: function (sp) {
        this.main.data.put(sp);
    },
    
    //Create new data.
    createHandler: function (sp) {
        this.main.data.create(sp);
    },
    
    //Commit data to server.
    saveHandler: function (sp) {
        this.main.data.save(sp);
    },
    
    //Perform server task.
    taskHandler: function (sp) {
        this.main.data.task(sp);
    },
    
    //Login
    loginHandler: function (sp) {
        //todo- route through perform.
        var obj = Ext.apply(sp,{
            method: 'login',
            component: sp.component||this.main.render,
            callback: sp.callback||Ext.bind(this.main.render.build,this.main.render),
            scope: sp.scope||this
        });
        this.main.data.task(obj);
    },
    
    
    //--------------------------------------
    // User Interface Actions
    //--------------------------------------
    
    actionHandler: function (sp) {
    
        //Perform action for this component.
        var cmp = sp.component;
        
        //Start with settings provider.
        
        //Apply all the core settings from the component.
        var ap = Ext.apply(sp,this.getSettingsProvider(cmp));
        
        //Apply the action.
        ap = Ext.apply(ap,{
            cmd: cmp.action
        });
        
        this.perform(ap);
    },
    
    gotoHandler: function (sp) {
        //From a list item selection go to a page.
        
        var cmp = sp.component;
        
        var ap = Ext.apply(sp,this.getSettingsProvider(cmp));
        
        ap = Ext.apply(ap,{
            cmd: 'request',
            source: sp.pagename
        });
        
        this.perform(ap);
    },
    
    setbusyHandler: function (sp) {
        if (sp.component) {
            sp.component.mask(sp.message||'Loading...');
        }
    },
    
    removebusyHandler: function (sp) {
        if (sp.component) sp.component.unmask();
    },
    
    sayHandler: function (sp) {
        this.main.feedback.say(sp);
    },
    
    askHandler: function (sp) {
        this.main.feedback.ask(sp);
    },
    
    
    //--------------------------------------
    // Rules
    //--------------------------------------
    
    //Introduce rule
    subscribeHandler: function(sp) {
        this.main.rules.subscribe(sp);
    },
    
    //After component is added to display stack.
    listenHandler: function(sp) {
        this.main.rules.listen(sp);
    },
    
    //After component is removed from display stack.
    ignoreHandler: function(sp) {
        this.main.rules.ignore(sp);
    },
    
    //After component is interacted with by user.
    broadcastHandler: function(sp) {
        this.main.rules.broadcast(sp);
    },
    
    //Programmatically broadcast a topic.
    topicHandler: function(sp) {
        this.main.rules.broadcastTopic(sp);
    },
    
    //After rule is no longer needed.
    unsubscribeHandler: function(sp) {
        this.main.rules.unsubscribe(sp);
    },
    
    //Load data for a component.
    loaddataHandler: function(sp) {
        this.main.rules.loadData(sp);
    },
    
    //Add data
    addHandler: function(sp) {
        this.main.rules.add(sp);
    },
    
    //Remove data
    removeHandler: function(sp) {
        this.main.rules.remove(sp);
    },
    
    primaryHandler: function(sp) {
        this.main.rules.primary(sp);
    },
    
    secondaryHandler: function (sp) {
        this.main.rules.secondary(sp);
    },
    
    loadsecondaryHandler: function(sp) {
        this.main.rules.loadsecondary(sp);
    },
    
    
    //--------------------------------------
    // Built-in Rules
    //--------------------------------------
    
    providerHandler: function (sp) {
        this.main.rules.provider(sp);
    },
    
    manageHandler: function (sp) {
        this.main.rules.manage(sp);
    },
    
    refreshmanagerHandler: function (sp) {
        this.main.rules.refreshManager(sp);
    },
    
    manageproviderHandler: function (sp) {
        this.main.rules.manageProvider(sp);
    },
    
    addproviderHandler: function (sp) {
        this.main.rules.addProvider(sp);
    },
    
    setproviderHandler: function (sp) {
        this.main.rules.setProvider(sp);
    },
    
    getproviderHandler: function (sp) {
        this.main.rules.getProvider(sp);
    },
    
    requireHandler: function (sp) {
        this.main.rules.require(sp);
    },
    
    disableHandler: function (sp) {
        this.main.rules.disable(sp);
    },
    
    hideHandler: function (sp) {
        this.main.rules.hide(sp);
    },
    
    getvalueHandler: function (sp) {
        this.main.rules.getValue(sp);
    },
    
    
    //--------------------------------------
    // Miscellaneous Actions
    //--------------------------------------
    
    idleHandler: function (sp) {
        //cache cleanup
        this.main.cache.expire();
        
        //save all changes at idle.
        this.main.data.save({});
        

        //update the location and address services at idle only if allowed by settings.
       	var locationsettings = this.main.environment.get("LOCATION_SERVICE");
        var addresssettings = this.main.environment.get("ADDRESS_SERVICE");

        if (locationsettings=="idle") this.main.environment.locate();
        if (addresssettings=="idle") this.main.environment.locateaddress();
    },
    
    dashboardHandler: function (sp) {
        //Show the content management system
        var o = Ext.getBody().getViewSize();
        //if (this.main.dashboard) {
            //this.main.view.remove(this.main.dashboard);
            //delete this.main.dashboard;
        //}
        var dashboard = Ext.create('widget.xdashboard',{width:o.width-50,height:o.height-50,main:this.main});
        this.main.view.add(dashboard);
        dashboard.show();

    },
    
    //Temp
    onlineHandler: function (sp) {
        //debugger;
    },
    
    //Location services
    
    locateHandler: function (sp) {
        this.main.environment.locate();
    },
    
    locateaddressHandler: function (sp) {
        this.main.environment.locateaddress(sp);
    },
    
    //--------------------------------------
    // Utilities
    //--------------------------------------
    
    getSettingsProvider: function (cmp) {
        //Get all properties from component interacted with and use them as a minimum for the action settings provider.
        
        var o = this.main.data.getObject(cmp);
        var sp = {};
        if (!o) return sp;
        
        //Transfer id of item to request from configurations to actual request.
        /*
        if (o.sourcealias) {
            sp.a = o.sourcealias;
        } else {
            if (o.source) {
                sp.n = o.source;
            }
        }
        */
           
        //Copy all other items to the new settings provider.
        for (var item in o) {
            if (/^(a|n|i|id|name|g|p|r|upd|upb|cr|crb|l|cid|utc|p|z|ct|pa|t|ca|ix1|ix2|ix3)$/i.test(String(item))) {
                //skip -- never include these in a request provider.
            } else {
                sp[item] = this.main.data.getValue(o[item]);
            }
        }
        return sp;
    }
    
});