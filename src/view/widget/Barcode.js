/**
 *
 */
Ext.define('bravo.widget.Barcode', {
    extend: 'Ext.Button',
    
    alias: 'barcodescanner',
    
    xtype: 'barcodescanner',
    
    constructor: function(config) {
        config.text = config.text||config.l;
        this.callParent(arguments);
    },
    
    initComponent: function () {

        this.enableBubble('cmd');
        
    
        Ext.defer(function() {
            this.fireEvent('cmd', {
                cmd:'listen',
                component: this
            });
        }, 100, this);

    },
    
    onClick: function () {
        var success = Ext.bind(this.successHandler,this);
        var failure = Ext.bind(this.failureHandler,this);
        try {
            cordova.plugins.barcodeScanner.scan(success,failure);
        } catch(e) {
            //Scanner is not available.
            this.failureHandler();
        }
    },
    
    successHandler: function (rsp) {
    
        //User cancelled the scan.
        if (rsp.cancelled) return;
        
        // Verify that the scanned code matches one of the expected formats
        var codecheck = false;
        if (this.hasOwnProperty("codeFormat")){
            var varr = this.codeFormat.split(",");
            if (varr.length>0){
                for (var i=0,l=varr.length;i<l;i++){
                    if (result.format == varr[i]){
                        codecheck = true;
                    }
                }
            }
        } else {
            // No default format set
            codecheck = true;
        }

        var val = (result.format=="QR_CODE") ? result.text.split(/\n\r?/g)[1] : result.text;

        if (codecheck) {
            this.fireEvent('cmd',{
                cmd: 'broadcast',
                component: this,
                value: val,
                format: result.format
            });
        } else {
            this.fireEvent('cmd',{
                cmd: 'say',
                component: this,
                title: 'Invalid Code',
                message: 'This code is not a valid format or scanned incorrectly. Please try again.'
            });
        }
    },
    
    failureHandler: function () {
        var cb = Ext.bind(this.manualHandler,this);
        this.fireEvent('cmd',{
            cmd: 'ask',
            title: 'Error',
            component: this,
            message: 'The barcode scanner is not available. Please enter code manually or try again.',
            callback: cb
        });
    
    },
    
    manualHandler: function (btn,s) {
        if (s&&btn=='ok') {
           this.fireEvent('cmd',{
                cmd: 'broadcast',
                component: this,
                value: s,
                format: ''
            });
        }
    },
    
    onRemove: function () {
        this.fireEvent('cmd', {
            cmd:'ignore',
            component: this
        });
    }

});

Ext.ClassManager.addNameAliasMappings({
    'bravo.widget.Barcode': ['barcodescanner']
});