/**

    Loginpanel:  Provides full support for user management including login, registration, password recovery, profile changes.
    
    Recover = create a short code using the larger code in the entities table.  Email it to the user.
    
    Reset = if the user supplies the correct code, change to registration is accepted, if not it is rejected.
    
    recover:  allows the recover panel to be shown.
    registration: allows the registration panel to be shown.
     
 *
 */
Ext.define('bravo.widget.Loginpanel', {
    extend: 'Ext.Panel',
    
    alias: 'loginpanel',
    controller: 'loginpanelcontroller',
    xtype: 'loginpanel',
    
    layout: {
        type: 'accordion',
        titleCollapse: true,
        animate: true,
        hideCollapseTool: true
    },
    
    requires: [
        'bravo.widget.LoginpanelController',
        'Ext.layout.container.Accordion'
    ],
    
    
    
    constructor: function(config) {
        this.callParent(arguments);
    },
    
    initComponent: function () {

        this.enableBubble('cmd');
        
        Ext.defer(function() {
        
            //Build this component.
        
            //Login Panel
            //-----------------------------------------------
            var loginpanel = Ext.create('Ext.form.Panel',{
                title: 'Sign In',
                scrollable: 'vertical'
            });
            this.add(loginpanel);
            
            var loginfieldset = Ext.create('Ext.form.FieldSet',{
                padding: '10 10 10 10',
                defaults: {
                    anchor: '100%',
                    width: '100%',
                    padding: '0 0 0 5'
                }
            });
            loginpanel.add(loginfieldset);
            
            loginfieldset.add(Ext.create('Ext.Component',{
                html: this.ct
            }));
            
            loginfieldset.add(Ext.create('Ext.form.field.Text',{
                fieldLabel: 'Username',
                name: 'loginusername'
            }));
            
            loginfieldset.add(Ext.create('Ext.form.field.Text',{
                fieldLabel: 'Password',
                inputType: 'password',
                name: 'loginpassword'
            }));
            
            loginfieldset.add(Ext.create('Ext.Button',{
                text: 'Sign In',
                name: 'loginbutton',
                flex: 1
            }));
            
            
            if (this.registration) {
            
                //Registration Panel
                //-----------------------------------------------
                var registrationpanel = Ext.create('Ext.form.Panel',{
                    scrollable: 'vertical',
                    title: 'Create Account'
                });
                this.add(registrationpanel);
                
                var registrationfieldset = Ext.create('Ext.form.FieldSet',{
                    padding: '10 10 10 10',
                    defaults: {
                        anchor: '100%',
                        width: '100%',
                        padding: '0 0 0 5'
                    }
                });
                registrationpanel.add(registrationfieldset);
                
                registrationfieldset.add(Ext.create('Ext.form.field.Text',{
                    fieldLabel: 'Email',
                    vtype: 'email',
                    name: 'registrationemail'
                }));
                
                registrationfieldset.add(Ext.create('Ext.form.field.Text',{
                    fieldLabel: 'Username',
                    name: 'registrationusername'
                }));
                
                registrationfieldset.add(Ext.create('Ext.form.field.Text',{
                    fieldLabel: 'Password',
                    inputType: 'password',
                    name: 'registrationpassword'
                }));
                
                registrationfieldset.add(Ext.create('Ext.form.field.Text',{
                    fieldLabel: 'Confirm Password',
                    name: 'registrationconfirm'
                }));
                
                registrationfieldset.add(Ext.create('Ext.Button',{
                    text: 'Create Account',
                    name: 'registrationbutton',
                    flex: 1
                }));
            
            }
            
            if (this.recover) {
                        
                //Recover Panel
                //-----------------------------------------------
                var recoverpanel = Ext.create('Ext.form.Panel',{
                    scrollable: 'vertical',
                    title: 'Forgot Password?'
                });
                this.add(recoverpanel);
                
                var recoverfieldset = Ext.create('Ext.form.FieldSet',{
                    padding: '10 10 10 10',
                    defaults: {
                        anchor: '100%',
                        width: '100%',
                        padding: '0 0 0 5'
                    }
                });
                recoverpanel.add(recoverfieldset);
                
                recoverfieldset.add(Ext.create('Ext.form.field.Text',{
                    fieldLabel: 'Email',
                    vtype: 'email',
                    name: 'recoveremail'
                }));
                
                recoverfieldset.add(Ext.create('Ext.Button',{
                    text: 'Email Reset Code',
                    name: 'recoverbutton',
                    flex: 1
                }));
                
                
                var resetfieldset = Ext.create('Ext.form.FieldSet',{
                    name: 'resetfieldset',
                    padding: '10 10 10 10',
                    defaults: {
                        anchor: '100%',
                        width: '100%',
                        padding: '0 0 0 5'
                    },
                    hidden: true
                });
                recoverpanel.add(resetfieldset);
                
                resetfieldset.add(Ext.create('Ext.form.field.Text',{
                    fieldLabel: 'Reset Code',
                    name: 'recovercode'
                }));
                
                resetfieldset.add(Ext.create('Ext.form.field.Text',{
                    fieldLabel: 'New Password',
                    inputType: 'password',
                    name: 'recoverpassword'
                }));
                
                resetfieldset.add(Ext.create('Ext.form.field.Text',{
                    fieldLabel: 'Confirm Password',
                    inputType: 'password',
                    name: 'recoverconfirm'
                }));
                
                resetfieldset.add(Ext.create('Ext.Button',{
                    text: 'Reset Password',
                    name: 'resetbutton',
                    flex: 1
                }));
            
            }
            
            //Set up all rules for this component.
            this.fireEvent('cmd', {
                cmd:'listen',
                component: this
            });
            
        }, 10, this);

        this.callParent(arguments);
    
    },
    

    
    onRemove: function () {
    
        //Disconnect from any rules.
        this.fireEvent('cmd', {
            cmd:'ignore',
            component: this
        });
    }

});

Ext.ClassManager.addNameAliasMappings({
    'bravo.widget.Loginpanel': ['loginpanel']
});