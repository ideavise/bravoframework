Ext.define('bravo.widget.LoginpanelController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.loginpanelcontroller',

    init: function() {
    
        //Listeners
        this.control({
            "button[name='loginbutton']": {
                click: this.loginHandler
            },
            "button[name='registrationbutton']": {
                click: this.registrationHandler
            },
            "button[name='recoverbutton']": {
                click: this.recoverHandler
            },
            "button[name='resetbutton']": {
                click: this.resetHandler
            }
        });
    },

    //Login
    loginHandler: function (cmp,ev) {
    
        var username = this.view.down("textfield[name='loginusername']");
        var password = this.view.down("textfield[name='loginpassword']");
        
        if ((username.isValid())&&(password.isValid())) {
    
            this.view.fireEvent('cmd',{
                cmd: 'login',
                component: cmp,
                username: username.value,
                password: password.value,
                callback: this.loginCallback,
                scope: this
            });
        
        }
        
    },
    
    //Registration
    registrationHandler: function (cmp,ev) {
    
        var username = this.view.down("textfield[name='registrationusername']");
        var password = this.view.down("textfield[name='registrationpassword']");
        var email = this.view.down("textfield[name='registrationemail']");
        var confirm = this.view.down("textfield[name='registrationconfirm']");
        
        if ((username.isValid())&&(password.isValid())&&(email.isValid())) {
            if (password.value==confirm.value) {
            
                this.view.fireEvent('cmd',{
                    a: email.value,
                    cmd: 'login',
                    method: 'register',
                    t: 0,
                    l: username.value,
                    pa: this.users||'bravo.everyone',
                    cid: 'person',
                    component: this,
                    username: username.value,
                    password: password.value,
                    callback: this.registrationCallback,
                    scope: this
                });
            
            } else {
            
                this.view.fireEvent('cmd',{
                    cmd: 'say',
                    title: 'Error',
                    message: 'Please confirm password.'
                });
               
                password.setValue('');
                confirm.setValue('');
            
            }
        }
    },
    
    //Recover
    recoverHandler: function (cmp,ev) {
    
        var email = this.view.down("textfield[name='recoveremail']");
        
        if (email.isValid()) {
        
            this.view.fireEvent('cmd',{
                cmd: 'login',
                method: 'recover',
                component: this,
                email: email.value,
                callback: this.recoverCallback,
                scope: this
            });
        
        }
        
    },
    
    //Reset
    resetHandler: function (cmp,ev) {
        var password = this.view.down("textfield[name='recoverpassword']");
        var code = this.view.down("textfield[name='recovercode']");
        var confirm = this.view.down("textfield[name='recoverconfirm']");
        
        
        if ((code.isValid())&&(password.isValid())) {
            if (password.value==confirm.value) {
            
                this.view.fireEvent('cmd',{
                    a: email.value,
                    cmd: 'login',
                    method: 'register',
                    t: 0,
                    l: username.value,
                    pa: this.users||'bravo.everyone',
                    cid: 'person',
                    component: this,
                    username: username.value,
                    password: password.value,
                    resetcode: code.value,
                    callback: this.resetCallback,
                    scope: this
                });
            
            } else {
            
                this.view.fireEvent('cmd',{
                    cmd: 'say',
                    title: 'Error',
                    message: 'Please confirm password.'
                });
               
                password.setValue('');
                confirm.setValue('');
            
            }
        }

    },
    
        
    loginCallback: function (rsp) {
        var me = rsp.scope||this;

        if (rsp.success=='success') {
        
            //Navigate to the location specified by this component
            var sp = {cmd: 'request'};
            if (me.view.redirectsource) sp.redirectsource = me.view.redirectsource;
            if (me.view.redirectalias) sp.redirectalias = me.view.redirectalias;
            if (me.view.target) sp.target = me.view.target;
        
            me.view.fireEvent('cmd',sp);
           
            //Rules fire only after successful login.
            me.view.fireEvent('cmd',{
                cmd: 'broadcast',
                component: me.view
            });
        
        }
    },
    
    
    registrationCallback: function (rsp) {
        var me = rsp.scope||this;
    
        if (rsp.success=='success') {
        
            me.view.fireEvent('cmd',{
                cmd: 'say',
                title: 'Info',
                message: 'Your account has been created.  Please sign in with your username/password.'
            });
        
        }
    
    },
    
    recoverCallback: function (rsp) {
        var me = rsp.scope||this;
        
        if (rsp.success=='success') {
        
            me.view.fireEvent('cmd',{
                cmd: 'say',
                title: 'Info',
                message: 'A reset code has been emailed to your email address.  Please enter this code and your new password.'
            });
           
            var fs = me.view.down("fieldset[name='resetfieldset']");
            fs.show();
        
        }

    
    },
    
    resetCallback: function (rsp) {
        var me = rsp.scope||this;
        
        if (rsp.success=='success') {
        
            me.view.fireEvent('cmd',{
                cmd: 'say',
                title: 'Info',
                message: 'Your password has been changed.  Please sign in again.'
            });
        
        }
    }
    
});
    
    
