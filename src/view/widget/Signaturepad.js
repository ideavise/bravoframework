/**
 *
 */
Ext.define('bravo.widget.Signaturepad', {
    extend: 'Ext.Container',
    
    alias: 'signaturepad',
    
    xtype: 'signaturepad',
    
    html: 'signature pad',
    
    constructor: function(config) {
        this.callParent(arguments);
    },
    
    initComponent: function () {

        this.enableBubble('cmd');

        Ext.defer(function() {
            this.fireEvent('cmd', {
                cmd:'listen',
                component: this
            });
        }, 100, this);

    },
    
    onRemove: function () {
        this.fireEvent('cmd', {
            cmd:'ignore',
            component: this
        });
    }

});

Ext.ClassManager.addNameAliasMappings({
    'bravo.widget.Signaturepad': ['signaturepad']
});