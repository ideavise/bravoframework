/**

Button

    action: this action will be performed when this button is clicked.  It will add all properties of this button to the action.  For example, request could also use alias to request a specific item by alias, source would request item by name.
     
    alias: the alias of item to perform action on.

    source: the name of item to perform action on.
 
 *
 */
Ext.define('bravo.layout.Button', {
    extend: 'Ext.Button',
    
    alias: 'xbutton',
    
    xtype: 'xbutton',
    
    constructor: function(config) {
        config.text = config.text||config.l;
        this.callParent(arguments);
    },
    
    initComponent: function () {

        this.enableBubble('cmd');

        Ext.defer(function() {
            this.fireEvent('cmd', {
                cmd:'listen',
                component: this
            });
        }, 100, this);
        
        this.callParent(arguments);

    },
    
    onRemove: function () {
        this.fireEvent('cmd', {
            cmd:'ignore',
            component: this
        });
    }

});

Ext.ClassManager.addNameAliasMappings({
    'bravo.layout.Button': ['xbutton']
});