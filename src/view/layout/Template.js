/**
 *  Template provides an html content that applies a template to a config.
 */
Ext.define('bravo.layout.Template', {
    extend: 'Ext.container.Container',
    
    alias: 'template',
    
    xtype: 'template',
    
    requires: [
        'Ext.container.Container'
    ],
    
    constructor: function(config) {
    
    	this.callParent(arguments);
    },
    
    initComponent: function () {
    
        this.enableBubble('cmd');
        
        Ext.defer(function() {
            this.fireEvent('cmd', {
                cmd:'loaddata',
                component: this
            });
        }, 10, this);
        
        Ext.defer(function() {
            this.fireEvent('cmd', {
                cmd:'listen',
                component: this
            });
        }, 10, this);
        
        this.callParent(arguments);

    },
    
    onRemove: function () {
        this.fireEvent('cmd', {
            cmd:'ignore',
            component: this
        });
    }

});

Ext.ClassManager.addNameAliasMappings({
    'bravo.layout.Template': ['template']
});