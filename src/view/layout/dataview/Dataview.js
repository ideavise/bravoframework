/**

    Dataview:  Provides basic support for displaying data.  Panel with docked toolbar to provide additional functionality
    
    paging: paging - show paging functions
    editing: show add/remove butons
    
 
    
    provider: put a topic name here.  It should match another container component with a provider name.  When an item is selected from this dataview, it will be marked as the active component with this provider name.  All components inside the container component
    will send their data to whatever dataprovider is active for this topic. 
    
    parentprovider: put a topic name here.  This will give the list the context for assigning the active parent if something new is created using this list.  The parentprovider would be an item that was selected in another previously used dataview.
    resolves in this order if something is specified -- a provider, an environment value, the current user
    if nothing is specified an error is raised and new item will not be created.
    
    action: this action will be performed and the active item selected will be passed as an argument to this action.  For example, request would request whatever item is active.
            
        goto:  navigates to a page on the screen.  requires that pagename (name/alias of item you want to load) and target (place you want to load it) are specified.
     
    sourcealias: the alias source of the data for this list.
        or
    source: the name source of the data for this list.
        or
    vals/captions: comma-delimited source of data for this list.
        or
    names/labels: comma-delimited source of data for this list.
        or
    aliases/labels: comma-delimited source of data for this list.
 
    $property:  add properties to filter the results by a certain value e.g. $cid, $n, $LastName, etc.
     
    relationship: e.g. children, descendants, ancestors, etc.
    
    contents: The XTemplate for each item is in the contents of this list.
    
    template:  A json object that gives a set of properties to add to a new item if it is created with this dataview.
    
    {provider$property}: this is a variable format that you can use in a settings provider to apply a variable value.
    
 
    
    Notes: must set flex to 1 and must be inside fit layout panel in order for scrolling to work.
     
 *
 */
Ext.define('bravo.layout.Dataview', {
    extend: 'Ext.Panel',
    
    alias: 'xdataview',
    controller: 'dataviewcontroller',
    xtype: 'xdataview',
    
    layout: {
        type: 'fit'
    },
    
    requires: [
        'bravo.layout.DataviewController'
    ],
    
    constructor: function(config) {
        config.pagesize = config.pagesize||50;
        config.page = config.page||1;
        
        
        this.callParent(arguments);
    },
    
    initComponent: function () {

        this.enableBubble('cmd');
        
        
        
        Ext.defer(function() {
        
            //Dataview
            var dvw = Ext.create('widget.dataview',{
                scrollable: this.scrollable||'vertical',
                itemSelector: this.itemSelector||'div',
                tpl: this.tpl||'<tpl for=".">' + (this.ct||'<div>{l}</div>') + '</tpl>',
                flex: 1,
                deferEmptyText: false,
                emptyText: this.emptyText||'',
                store: {}
            });
            
            this.add(dvw);
            
            //Toolbar
            if (this.paging||this.editing) {
            
                var tbr = Ext.create('widget.toolbar',{
                    dock: 'bottom'
                });
                
                //Paging functions
                if (this.paging) {
                    tbr.add(Ext.create('widget.button',{
                        iconCls: 'fa fa-step-backward',
                        name: 'firstbutton'
                    }));
                    
                    tbr.add(Ext.create('widget.button',{
                        iconCls: 'fa fa-chevron-left',
                        name: 'prevbutton'
                    }));
                    
                    tbr.add(Ext.create('widget.tbseparator',{
                    }));
                    
                    tbr.add(Ext.create('widget.tbtext',{
                        html: 'Page 1',
                        name: 'page'
                    }));
                  
                    tbr.add(Ext.create('widget.combobox',{
                        name: 'pagesize',
                        labelWidth: 0,
                        width: 80,
                        store: {
                            data: [
                                {text:'10',value:10},
                                {text:'25',value:25},
                                {text:'50',value:50},
                                {text:'75',value:75},
                                {text:'100',value:100},
                                {text:'200',value:200},
                                {text:'250',value:250}
                            ]
                        },
                        queryMode: 'local',
                        displayField: 'text',
                        valueField: 'value',
                        value: this.pagesize||50,
                        editable: false
                    }));
                  
                    tbr.add(Ext.create('widget.tbtext',{
                        html: 'Per Page'
                    }));
                    
                    tbr.add(Ext.create('widget.tbseparator',{
                    }));
                    
                    tbr.add(Ext.create('widget.button',{
                        iconCls: 'fa fa-chevron-right',
                        name: 'nextbutton'
                    }));
                  
                }
                
                //Editing functions
                if ((this.editing)&&(this.$cid)) {
                    tbr.add(Ext.create('widget.tbspacer',{
                        flex: 1
                    }));
                  
                    tbr.add(Ext.create('widget.button',{
                        iconCls: 'fa fa-plus',
                        name: 'newbutton'
                    }));
                    
                    tbr.add(Ext.create('widget.button',{
                        iconCls: 'fa fa-minus',
                        name: 'removebutton'
                    }));
                }
                
                this.addDocked(tbr);
            
            }
            
            //Create a custom dataprovider rule if a provider is specified.
            if (this.provider) {
                this.fireEvent('cmd',{
                    cmd: 'addprovider',
                    component: this
                });
            }
            
            //Set up all rules for this component.
            this.fireEvent('cmd', {
                cmd:'listen',
                component: this
            });
            
            //Set this component to busy until data loads.
            this.fireEvent('cmd',{
                cmd: 'setbusy',
                component: this
            });
            
            //Load the data for this component.
            this.fireEvent('cmd',{
                cmd: 'loaddata',
                component: this,
                callback: this.fill
            });
            
        }, 10, this);

        this.callParent(arguments);
    
    },
    
    fill: function (rsp) {

        var me = (rsp.scope||this);
        
        var dvw = me.down('dataview');
        
        if (rsp.desc.length==0) {
        
            //dvw.addEmptyText();
           
            /*me.fireEvent('cmd',{
                cmd: 'say',
                title: 'Info',
                message: 'No data found for ' + me.l
            });*/
        
        } else {
        
            //Date filtering.  Placed here so that data is loaded for tree/cms operations but not displayed in user interface unless it
            //meets the date criteria.
            var arr = [];
            var now = new Date();
            for (var i=0,l=rsp.desc.length;i<l;i++) {
                var o = rsp.desc[i];
                if (o.sd) {
                    if (o.ed) {
                        //both
                        if ((o.sd<now)&&(o.ed>now)) arr.push(o);
                    } else {
                        //only sd
                        if (o.sd<now) arr.push(o);
                    }
                } else {
                    if (o.ed) {
                        //only ed
                        if (o.ed>now) arr.push(o);
                    } else {
                        //neither
                        arr.push(o);
                    }
                }
            }
           
            dvw.store.loadData(arr);
           
            if (me.paging) {
                me.down("button[name='firstbutton']").setDisabled(me.page<=1);
                me.down("button[name='prevbutton']").setDisabled(me.page<=1);
                me.down("button[name='nextbutton']").setDisabled(rsp.desc.length<me.pagesize);
                me.down("tbtext[name='page']").setHtml('Page ' + me.page);
            }
           
        }
    
        me.fireEvent('cmd',{
            cmd: 'removebusy',
            component: this
        });
    },
    
    onRemove: function () {
    
        this.fireEvent('cmd', {
            cmd:'ignore',
            component: this
        });
    }

});

Ext.ClassManager.addNameAliasMappings({
    'bravo.layout.Dataview': ['xdataview']
});