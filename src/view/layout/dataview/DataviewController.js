Ext.define('bravo.layout.DataviewController',{
    extend: 'Ext.app.ViewController',
    
    alias: 'controller.dataviewcontroller',

    init: function() {
    
        //Listeners
        this.control({
            "button[name='firstbutton']": {
                click: this.firstHandler
            },
            "button[name='prevbutton']": {
                click: this.prevHandler
            },
            "button[name='nextbutton']": {
                click: this.nextHandler
            },
            "button[name='newbutton']": {
                click: this.addHandler
            },
            "button[name='removebutton']": {
                click: this.removeHandler
            },
            "combobox[name='pagesize']": {
                change: this.pagesizeHandler
            },
            "dataview": {
                itemclick: this.itemclickHandler
            }
        });
    },
    
    //Dataview
    itemclickHandler: function (cmp,rec,el,idx,ev,opts) {
        //This function is called anytime a list item is selected.
        
        //Rules must be called before any actions so that providers are set before the action is performed.
        
        //Rules
        this.view.fireEvent('cmd',{
            cmd: 'broadcast',
            component: this.view,
            provider: rec.get("a")
        });

        //Action
        var sp = Ext.apply({},rec.data);
        if (this.view.action) {
            this.view.fireEvent('cmd',Ext.apply(sp,{
                cmd: 'action',
                component: this.view,
                idx: idx
            }));
        }
           
        
    },
    
    
    firstHandler: function (cmp,ev) {
        this.view.page=1;
        this.loadData();
    },
    
    prevHandler: function (cmp,ev) {
        var pg = this.view.page;
        if (pg>1) this.view.page = pg-1;
        this.loadData();
    },
    
    nextHandler: function (cmp,ev) {
        var pg = this.view.page;
        this.view.page = pg+1;
        this.loadData();
    },
    
    addHandler: function (cmp,ev) {

        this.view.fireEvent('cmd',{
            cmd: 'add',
            component: this.view,
            callback: this.addedHandler
        });
    },
    
    addedHandler: function (o) {
    
    },
    
    removeHandler: function (cmp,ev) {
       
        this.view.fireEvent('cmd',{
            cmd: 'remove',
            component: this.view,
            callback: this.removedHandler
        });
    },
    
    removedHandler: function (o) {
        //Returns the item that was removed.
        
        var dvw = this.view.down('dataview');
        dvw.store.remove(dvw.store.findRecord("a",o.a));
        
     
        //Refresh this component.
        this.view.fireEvent('cmd',{
            cmd: 'loaddata',
            component: this.view,
            callback: this.view.fill
        });
     
    },
    
    pagesizeHandler: function (cmp,nv) {
        this.view.pagesize=nv;
        this.loadData();
    },
    
    
    loadData: function () {
        this.view.fireEvent('cmd',{
            cmd: 'loaddata',
            component: this.view,
            callback: this.view.fill
        });
    }

    
});
