/**
 *
 */
Ext.define('bravo.layout.Container', {
    extend: 'Ext.container.Container',

    alias: 'xcontainer',
    
    xtype: 'xcontainer'
    
});

Ext.ClassManager.addNameAliasMappings({
    'bravo.layout.Container': ['xcontainer']
});