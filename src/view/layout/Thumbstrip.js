/**

    Thumbstrip:  Provides basic support for displaying data in a horizontal list control that
    scrolls horizontally.
    
    itemwidth:  determines the itemwidth of each cell.
 
    itemheight: determines the fixed height of each cell and this component.
    
    provider: put a topic name here.  It should match another container component with a provider name.  When an item is selected from this dataview, it will be marked as the active component with this provider name.  All components inside the container component
    will send their data to whatever dataprovider is active for this topic. 
    
    action: this action will be performed and the active item selected will be passed as an argument to this action.  For example, request would request whatever item is active.
     
    alias: the alias source of the data for this list.
        or
    source: the name source of the data for this list.
        or
    vals/captions: comma-delimited source of data for this list.
        or
    names/labels: comma-delimited source of data for this list.
        or
    aliases/labels: comma-delimited source of data for this list.
     
    $property:  add properties to filter the results by a certain value e.g. $cid, $n, $LastName, etc.
     
    relationship: e.g. children, descendants, ancestors, etc.
    
    contents: The XTemplate for each item is in the contents of this list.
     
 *
 */
Ext.define('bravo.layout.Thumbstrip', {
    extend: 'Ext.grid.Panel',
    
    alias: 'thumbstrip',
    
    xtype: 'thumbstrip',
    
    hideHeaders: true,
    
    editable: false,
    
    scrollable: {
        direction: 'horizontal'
    },
    
    rowLines: false,
    
    columnLines: false,
    
    //syncRowHeight: false,  //responsibility falls to content to be same height.  improves performance
    
    constructor: function(config) {
        config.tpl = '<tpl for=".">' + (config.ct||'<div>{l}</div>') + '</tpl>';
        config.height = config.itemheight||100;
        
        this.callParent(arguments);

    },
    
    initComponent: function () {

        this.enableBubble('cmd');
        this.store = new Ext.data.Store({});
        if (!this.itemSelector) this.itemSelector = 'div';
        
        Ext.defer(function() {
            
            //Set this component to busy until data loads.
            this.fireEvent('cmd',{
                cmd: 'setbusy',
                component: this
            });
            
            //Load the data for this component.
            this.fireEvent('cmd',{
                cmd: 'loaddata',
                component: this,
                callback: this.fill
            });
            
            //Set up all rules for this component.
            this.fireEvent('cmd', {
                cmd:'listen',
                component: this
            });
            
            //Create a custom dataprovider rule if a provider is specified.
            if (this.provider) {
                this.fireEvent('cmd',{
                    cmd: 'addprovider',
                    component: this
                });
            }
            
        }, 100, this);
        
        this.callParent(arguments);

    },
    
    fill: function (rsp) {
    
        var me = (rsp.scope||this);
        
        if (rsp.desc.length==0) {
            me.fireEvent('cmd',{
                cmd: 'say',
                title: 'Info',
                message: 'No data found for ' + me.l
            });
        
        } else {
            //debugger;
           
            //Store
            var o = {};
            for (var i=0,l=rsp.desc.length;i<l;i++) {
                o['col'+i] = me.tpl.apply(rsp.desc[i]);
            }
            var store = new Ext.data.Store({
                data: [o]
            });
           
            //Columns
            var cols = [];
            for (var i=0,l=rsp.desc.length;i<l;i++) {
                cols.push({
                    dataIndex: 'col'+i,
                    variableRowHeight: true,
                    width: me.itemwidth||200,
                    editable: false,
                    hideable: false
                });
            }
           
            me.reconfigure(store,cols);
            //me.store.loadData(rsp.desc);
        }
    
        me.fireEvent('cmd',{
            cmd: 'removebusy',
            component: this
        });
    },
    
    onRemove: function () {
        this.fireEvent('cmd', {
            cmd:'ignore',
            component: this
        });
    }

});

Ext.ClassManager.addNameAliasMappings({
    'bravo.layout.Thumbstrip': ['thumbstrip']
});