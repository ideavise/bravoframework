/**
 *
 */
Ext.define('bravo.layout.ToolbarTextItem', {
    extend: 'Ext.toolbar.TextItem',
    
    alias: 'xtbtext',
    
    xtype: 'xtbtext',
    
    constructor: function(config) {

        var tpl = new Ext.XTemplate(config.ct);
        config.text = tpl.apply(config);

    	this.callParent(arguments);
    },
    
    initComponent: function () {
        
        this.callParent(arguments);

    },
    
    onRemove: function () {

    }

});

Ext.ClassManager.addNameAliasMappings({
    'bravo.layout.ToolbarTextItem': ['xtbtext']
});