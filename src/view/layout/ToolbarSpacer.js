/**
 *
 */
Ext.define('bravo.layout.ToolbarSpacer', {
    extend: 'Ext.toolbar.Spacer',
    
    alias: 'xtbspacer',
    
    xtype: 'xtbspacer',
    
    constructor: function(config) {
        this.fieldLabel = config.title||config.l;
    	this.callParent(arguments);
    },
    
    initComponent: function () {
        
        this.callParent(arguments);

    },
    
    onRemove: function () {

    }

});

Ext.ClassManager.addNameAliasMappings({
    'bravo.layout.ToolbarSpacer': ['xtbspacer']
});