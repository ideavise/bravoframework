/**
 * ComboNavigator: Component that can set a data provider and provide actions.
 
 Use Combobox instead if you want to use it for data entry.
 
 
     provider: put a topic name here.  It should match another container component with a provider name.  When an item is selected from this dataview, it will be marked as the active component with this provider name.  All components inside the container component
    will send their data to whatever dataprovider is active for this topic. 
    
    action: this action will be performed and the active item selected will be passed as an argument to this action.  For example, request would request whatever item is active.
     
    alias: the alias source of the data for this list.
        or
    source: the name source of the data for this list.
        or
    vals/captions: comma-delimited source of data for this list.
        or
    names/labels: comma-delimited source of data for this list.
        or
    aliases/labels: comma-delimited source of data for this list.
     
    $property:  add properties to filter the results by a certain value e.g. $cid, $n, $LastName, etc.
     
    relationship: e.g. children, descendants, ancestors, etc.
 
 
 */
Ext.define('bravo.layout.ComboNavigator', {
    extend: 'Ext.form.field.ComboBox',
    
    alias: 'xcombonavigator',
    
    xtype: 'xcombonavigator',
    
    constructor: function(config) {
    
        Ext.apply(config,{
            boxLabel: (config.title||config.l),
            valueField: (config.valueField||'a'),
            displayField: (config.displayField||'l'),
            queryMode: 'local',
            editable: false
        });
        
        this.callParent(arguments);
    },
    
    initComponent: function () {

        this.enableBubble('cmd');
        
        this.store = new Ext.data.Store({});
        
        Ext.defer(function() {
            
            //Set this component to busy until data loads.
            this.fireEvent('cmd',{
                cmd: 'setbusy',
                component: this
            });
            
            //Load the data for this component.
            this.fireEvent('cmd',{
                cmd: 'loaddata',
                component: this,
                callback: this.fill
            });
            
            //Set up all rules for this component.
            this.fireEvent('cmd', {
                cmd:'listen',
                component: this
            });
            
            //Create a custom dataprovider rule if a provider is specified.
            if (this.provider) {
                this.fireEvent('cmd',{
                    cmd: 'addprovider',
                    component: this
                });
            }
            
        }, 100, this);
        
        this.callParent(arguments);
        
    },
    
    fill: function (rsp) {
    
        var me = (rsp.scope||this);
        
        if (rsp.desc.length==0) {
            me.fireEvent('cmd',{
                cmd: 'say',
                title: 'Info',
                message: 'No data found for ' + me.l
            });
        
        } else {
            me.store.loadData(rsp.desc);
        }
    
        me.fireEvent('cmd',{
            cmd: 'removebusy',
            component: this
        });
    },
    
    onRemove: function () {
        this.fireEvent('cmd', {
            cmd:'ignore',
            component: this
        });
    }

});

Ext.ClassManager.addNameAliasMappings({
    'bravo.layout.ComboNavigator': ['xcombonavigator']
});