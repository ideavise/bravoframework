/**
 *
 */
Ext.define('bravo.layout.Panel', {
    extend: 'Ext.Panel',

    alias: 'xpanel',
    
    xtype: 'xpanel',
    
    constructor: function(config) {
    
        //Template
        var tpl = new Ext.XTemplate(config.ct);
        config.html = tpl.apply(config);
        
        //Layout
        switch (config.layout) {
            case "fit":
                config.layout = {
                    type: 'fit'
                };
                break;
            case "auto":
                config.layout = {
                    type: 'auto'
                };
                break;
            case "center":
                config.layout = {
                    type: 'center'
                };
                break;
            case "vbox":
                config.layout = {
                    type: 'vbox',
                    align: config.layoutalign||'center',
                    pack: config.layoutpack||'middle'
                };
                break;
            case "hbox":
                config.layout = {
                    type: 'hbox',
                    align: config.layoutalign||'center',
                    pack: config.layoutpack||'middle'
                };
                break;
            default:
                config.layout = {
                    type: 'fit'
                };
                break;
        }
           

    	this.callParent(arguments);
    },
    
    initComponent: function () {

        this.enableBubble('cmd');
        
        if (this.manage) {
            Ext.defer(function() {
                this.fireEvent('cmd', {
                    cmd:'manage',
                    component: this
                });
            }, 100, this);
        }
        
        Ext.defer(function() {
            this.fireEvent('cmd', {
                cmd:'listen',
                component: this
            });
        }, 100, this);
        
        this.callParent(arguments);

    },
    
    onRemove: function () {
        this.fireEvent('cmd', {
            cmd:'ignore',
            component: this
        });
    }
    
});

Ext.ClassManager.addNameAliasMappings({
    'bravo.layout.Panel': ['xpanel']
});