/**
 *  Dashboard
 *  Contains all code for the dashboard.
 */

Ext.define('bravo.DashboardController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.dashboard',


    init: function(application) {
    
        //Listeners
        this.control({
            //general
            "button[name='loginbutton']": {
                click: this.loginHandler
            },
            "tabpanel[name='bravotabs']": {
                tabchange: this.tabchangeHandler
            },
            "tabpanel[name='bravopropertiestabs']": {
                tabchange: this.propertiestabchangeHandler
            },
            //browser
            "dataview[name='bravobrowserchooser']": {
                itemclick: this.chooseclassHandler,
                itemlongpress: this.pressclassHandler
            },
            "button[name='bravobrowsermenubutton']": {
                click: this.bravobrowsermenuHandler
            },
            "button[name='bravobrowsersearchbutton']": {
                click: this.bravobrowsersearchHandler
            },
            "button[name='bravobrowserfavoritesbutton']": {
                click: this.bravobrowserfavoritesHandler
            },
            "button[name='bravobrowserfirstbutton']": {
                click: this.browsefirstHandler
            },
            "button[name='bravobrowserprevbutton']": {
                click: this.browseprevHandler
            },
            "button[name='bravobrowsernextbutton']": {
                click: this.browsenextHandler
            },
            "button[name='bravobrowsernewbutton']": {
                click: this.browsenewHandler
            },
            "button[name='bravosearchbutton']": {
                click: this.bravosearchHandler
            },
            "button[name='bravosearchcancelbutton']": {
                click: this.bravosearchcancelHandler
            },
            "combobox[name='bravobrowserpagesize']": {
                change: this.browsepagesizeHandler
            },
            "grid[name='bravosearchgrid']": {
                cellclick: this.browsesearcheditHandler
            },
            "button[name='bravobrowserdownloadbutton']": {
                click: this.browsedownloadHandler
            },
            //explorer
            "dataview[name='bravotreechooser']": {
                itemclick: this.chooseitemHandler,
                itemlongpress: this.pressitemHandler,
                itemdblclick: this.exploreitemHandler,
                droprecord: this.connectHandler,
                organize: this.organizeHandler
            },
            "button[name='bravoexplorerfirstbutton']": {
                click: this.explorefirstHandler
            },
            "button[name='bravoexplorerprevbutton']": {
                click: this.exploreprevHandler
            },
            "button[name='bravoexplorernextbutton']": {
                click: this.explorenextHandler
            },
            "button[name='bravoexplorernewbutton']": {
                click: this.explorenewHandler
            },
            "combobox[name='bravoexplorerpagesize']": {
                change: this.explorepagesizeHandler
            },
            "textfield[name='bravoexplorersearch']": {
                change: Ext.Function.createBuffered(this.exploresearchHandler, 2000, this)
            },
            "xupload[name='bravoexploreruploadbutton']": {
                upload: this.exploreuploadHandler
            },
            "button[name='bravoexplorerdownloadbutton']": {
                click: this.exploredownloadHandler
            },
            //toolbox - not implemented?
            //"button[name='bravotoolboxmenubutton']": {
                //click: this.toolboxmenuHandler
            //},
            //editor
            
            
            
            //contents panel  optional: htmleditor
            "fieldset[name='bravocontentspanel'] textareafield": {
                blur: this.dirtyHandler
            },
            //about panel
            "fieldset[name='bravoaboutpanel'] textfield": {
                blur: this.dirtyHandler
            },
            //basic properties
            "panel[name='bravobasic'] textfield": {
                blur: this.dirtyHandler
            },
            "panel[name='bravobasic'] textareafield": {
                blur: this.dirtyHandler
            },
            "panel[name='bravobasic'] datefield": {
                change: this.dirtyHandler
            },
            "panel[name='bravobasic'] radiofield": {
                change: this.dirtyHandler
            },
            "panel[name='bravobasic'] combobox": {
                change: this.dirtyHandler
            },
            //advanced properties
            "grid[name='bravocustompropertiesgrid']": {
                cellclick: this.custompropertieseditHandler
            },
            "grid[name='bravocustompropertiesgrid'] textfield": {
                blur: this.commitHandler
            },
            //editor toolbox
            "button[name='editorfavoritesbutton']": {
                click: this.addFavoritesHandler
            },
            "button[name='editortrashbutton']": {
                click: this.trashHandler
            },
            "button[name='editorrefreshbutton']": {
                click: this.refreshHandler
            },
            "button[name='editorwrenchbutton']": {
                click: this.learnpropertiesHandler
            },
            "button[name='editortreebutton']": {
                click: this.exploreeditoritemHandler
            },
            //connector
            "dataview[name='bravoconnectchooser']": {
                itemclick: this.chooseconnectitemHandler,
                itemdblclick: this.connectitemHandler,
                droprecord: this.connectHandler,
                organize: this.organizeHandler
            },
            "button[name='bravoconnectorfirstbutton']": {
                click: this.connectfirstHandler
            },
            "button[name='bravoconnectorprevbutton']": {
                click: this.connectprevHandler
            },
            "button[name='bravoconnectornextbutton']": {
                click: this.connectnextHandler
            },
            "combobox[name='bravoconnectorpagesize']": {
                change: this.connectpagesizeHandler
            },
            "textfield[name='bravoconnectorsearch']": {
                change: Ext.Function.createBuffered(this.connectsearchHandler, 2000, this)
            },
            "button[name='bravoconnectorsortbutton']": {
                click: this.connectsortHandler
            },
            //favorites
            "dataview[name='bravofavoriteschooser']": {
                itemclick: this.choosefavoriteitemHandler,
                itemlongpress: this.pressfavoriteitemHandler,
                itemdblclick: this.explorefavoriteitemHandler,
                droprecord: this.connectfavoriteHandler
            }
        });
        
        Ext.defer(function() {
        
            //Reference to bravo so you can call functions directly.
            this.main = this.view.main;
            
            //Ability to communicate custom events to bravo.
            this.view.enableBubble('cmd');
            
            //Init
            this.view.setEdits({});
            this.view.setBrowseOptions({});
            this.view.setConnectSource({});
            this.view.setConnectDestination({});
            this.view.setExplore({
                a: this.main.environment.get("DEFAULT_PROJECT")
            });
            
            //check login.
            if (this.main.entity.getUser().i==8) {
                this.view.down("form[name='loginform']").show();
            } else {
                this.setup();
            }
            
        }, 100, this);
           
    },
    
    
    //---------------------------------------------------------------------------------------
    // Shared Functions
    //---------------------------------------------------------------------------------------
    
    setup: function() {
        //Make an initial request to the main project to get default permissions.
        
        this.view.fireEvent('cmd',{
            cmd: "request",
            a: this.main.environment.get("PROJECT"),
            relationship: "children",
            callback: this.setupCallback,
            scope: this
        });
    },
    
    setupCallback: function (rsp) {
        var me = (rsp.scope||this);
        
        //Set the "top" of this user's environment.
       // var role = me.main.data.getRole("bravo.top");
        //if (role>2) {
            //me.view.setTop("bravo.top");
        //} else {
            //me.view.setTop(this.main.environment.get("TOP")||this.main.environment.get("PROJECT"));
        //}
        
        
        var panel = me.view.down("panel[name='leftpanel']");
        panel.removeAll();
        
        //Create the tabpanel
        var tabpanel = Ext.create('Ext.tab.Panel',{
            name: 'bravotabs',
            flex: 1,
            items: [
                //Tab 1: Class based search.
                {
                    xtype: 'container',
                    name: 'bravobrowser',
                    iconCls: 'fa fa-th',
                    layout: 'card',
                    items: [
                        //Panel 1: Choose a class.
                        {
                            xtype: 'panel',
                            layout: 'fit',
                            name: 'bravobrowserchooserpanel',
                            items: [
                                {
                                    xtype: 'dataview',
                                    name: 'bravobrowserchooser',
                                    store: {
                                        sorters: [{
                                             property: 'l',
                                             direction: 'ASC'
                                        }]
                                    },
                                    scrollable: true,
                                    tpl: [
                                        '<tpl for=".">',
                                            '<div class="draggable">',
                                                '<tpl if="iconCls">',
                                                    '<span class="fa fa-{iconCls} draggable_icon"></span>',
                                                '<tpl else>',
                                                    '<span class="fa fa-cube draggable_icon"></span>',
                                                '</tpl>',
                                                '<p>',
                                                    '<tpl if="plural">',
                                                        '{plural}',
                                                    '<tpl else>',
                                                        '{l}',
                                                    '</tpl>',
                                                '</p>',
                                            '</div>',
                                        '</tpl>'
                                    ],
                                    itemSelector : 'div.draggable',
                                    flex: 1
                                }
                            ]
                        },
                        //Panel 2: Grid with item list.
                        {
                            xtype: 'panel',
                            name: 'bravobrowsereditorpanel',
                            layout: 'fit',
                            dockedItems: [
                                {
                                    xtype: 'toolbar',
                                    name: 'bravobrowsertoolbar',
                                    dock: 'top',
                                    flex: 1,
                                    items: [
                                        {
                                            text: 'Back to Menu',
                                            name: 'bravobrowsermenubutton',
                                            iconCls: 'fa fa-th'
                                        },
                                        {
                                            xtype: 'tbspacer',
                                            flex: 1
                                        },
                                        {
                                            xtype: 'tbtext',
                                            name: 'bravobrowsetitle',
                                            flex: 1,
                                            html: ''
                                        },
                                        {
                                            xtype: 'tbspacer',
                                            flex: 1
                                        },
                                        {
                                            text: 'Search',
                                            name: 'bravobrowsersearchbutton',
                                            iconCls: 'fa fa-search'
                                        }
                                    ]
                                },
                                {
                                    xtype:'toolbar',
                                    name:'bravobrowserpagingtoolbar',
                                    dock: 'bottom',
                                    items: [
                                        {
                                            xtype: 'button',
                                            iconCls: 'fa fa-step-backward',
                                            name: 'bravobrowserfirstbutton'
                                        },
                                        {
                                            xtype: 'button',
                                            iconCls: 'fa fa-chevron-left',
                                            name: 'bravobrowserprevbutton'
                                        },
                                        '-',
                                        {
                                            xtype: 'tbtext',
                                            html: 'Page 1',
                                            name: 'bravobrowserpage'
                                        },
                                        '-',
                                        {
                                            xtype: 'button',
                                            iconCls: 'fa fa-chevron-right',
                                            name: 'bravobrowsernextbutton'
                                        },
                                        {
                                            xtype: 'button',
                                            iconCls: 'fa fa-plus',
                                            name: 'bravobrowsernewbutton'
                                        },
                                        
                                        {
                                            xtype: 'tbspacer',
                                            flex: 1
                                        },
                                        {
                                            xtype: 'button',
                                            iconCls: 'fa fa-download',
                                            name: 'bravobrowserdownloadbutton'
                                        },
                                        {
                                            xtype: 'combobox',
                                            name: 'bravobrowserpagesize',
                                            fieldLabel: 'Rows',
                                            labelWidth: 40,
                                            width: 150,
                                            store: {
                                                data: [
                                                    {text:'10',value:10},
                                                    {text:'25',value:25},
                                                    {text:'50',value:50},
                                                    {text:'75',value:75},
                                                    {text:'100',value:100},
                                                    {text:'200',value:200},
                                                    {text:'250',value:250}
                                                ]
                                            },
                                            queryMode: 'local',
                                            displayField: 'text',
                                            valueField: 'value',
                                            value: 50,
                                            editable: false
                                        }
                                    ]
                                }
                            ],
                            items: [
                                {
                                    xtype: 'grid',
                                    name: 'bravobrowsergrid',
                                    scrollable: true,
                                    viewConfig: {
                                        markDirty: false
                                    }
                                }
                            ]
                        },
                        
                        //Panel 3: Search for specific items.
                        {
                            xtype: 'panel',
                            name: 'bravobrowsersearchpanel',
                            layout: 'fit',
                            items: [
                            ],
                            dockedItems: [
                                {
                                    xtype: 'toolbar',
                                    name: 'bravosearchtoptoolbar',
                                    dock: 'top',
                                    flex: 1,
                                    items: [
                                        {
                                            xtype: 'tbtext',
                                            name: 'bravosearchtitle',
                                            flex: 1,
                                            html: 'Search'
                                        }
                                    ]
                                },
                                {
                                    xtype: 'toolbar',
                                    name: 'bravosearchbottomtoolbar',
                                    dock: 'bottom',
                                    flex: 1,
                                    items: [
                                        {
                                            text: 'Cancel',
                                            name: 'bravosearchcancelbutton',
                                            iconCls: 'fa fa-ban'
                                        },
                                        {
                                            xtype: 'tbspacer',
                                            flex: 1
                                        },
                                        {
                                            text: 'Search',
                                            name: 'bravosearchbutton',
                                            iconCls: 'fa fa-search'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        });
        panel.add(tabpanel);
    
        //Default Tab: Browser
        me.setupBrowser();

        //Advanced Tabs: Explorer/Favorites.
        var role = me.main.data.getRole(me.main.environment.get("PROJECT"));
        if (role>4) {
            me.setupExplorer();
            me.setupFavorites();
        }
            
    },
    
    tabchangeHandler: function (cmp,ntab,otab,opts) {
        var panel = this.view.down("panel[name='leftpanel']");
        switch (ntab.name) {
            case "bravobrowser":
                panel.setIconCls("fa fa-th");
                panel.setTitle("Browse");
                break;
            case "bravofavorites":
                panel.setIconCls("fa fa-heart");
                panel.setTitle("Favorites");
                this.favorites(this.main.entity.getUser());
                break;
            case "bravotree":
                //Load default tree
                panel.setIconCls("fa fa-sitemap");
                panel.setTitle("Explore");
                this.explore(this.view.getExplore());
                break;
            default:
        }
        this.view.setActiveTab(ntab.name);
    },
    
    
    propertiestabchangeHandler: function (cmp,ntab,otab,opts) {
        this.view.setActivePropertiesTab(ntab.name);
    },
    
    loginHandler: function (cmp,ev) {
        this.view.fireEvent('cmd',{
            cmd: "login",
            component: cmp,
            username: this.view.down("textfield[name='usernametext']").value,
            password: this.view.down("textfield[name='passwordtext']").value,
            callback: this.loginCallback,
            scope: this
        });
    },
    
    loginCallback: function (rsp) {
        var me = (rsp.scope||this);
        
        if (me.main.entity.getUser().i!=8) me.setup();
    },
    
    //---------------------------------------------------------------------------------------
    // Browser
    //---------------------------------------------------------------------------------------
    
    //Start with a menu of classes.
    setupBrowser: function () {
    
        //Clear parameters.
        this.view.setBrowseOptions({});
        
        //Change the title of the left panel
        var panel = this.view.down("panel[name='leftpanel']");
        panel.setTitle('Browse');

        //Get the default toolbox to load.
        var top = this.main.data.getRoleDomain(this.main.environment.get("PROJECT"));
        
        //Request toolbox.
        //TODO: roles 5,6 = toolbox, roles 3,4 = default owner toolbox
        this.view.fireEvent('cmd',{
            cmd: "request",
            a: top.a,
            cid: 'class',
            relationship: "descendants",
            callback: this.browserCallback,
            scope: this
        });
        
    },
    
    browserCallback: function (rsp) {
        
        //Display class list.
        var me = (rsp.scope||this);
        var arr = me.main.data.objectToArray(me.main.data.classes['class']);
        
        //Special exception - use the icon from the class NOT the generic class icon.
        for (var i=0,l=arr.length;i<l;i++) {
            arr[i].iconCls = arr[i].icon;
        }
           
        var cmp = me.view.down("dataview[name='bravobrowserchooser']");
        cmp.getStore().loadData(arr);
        me.view.down("container[name='bravobrowser']").getLayout().setActiveItem(0);

        //Build a dashboard of custom widgets for general use.
        var panel = me.view.down("panel[name='rightpanel']");
        panel.show();
        panel.removeAll();
        
        var widget = Ext.create('Ext.Component',{
            padding: 10,
            //html: 'Insert general widgets here for owner to do general tasks'
            html:''
        });
        panel.add(widget);
        
        me.view.down("toolbar[name='bravoeditortoolbar']").hide();
        
    },
 
    
    chooseclassHandler: function (cmp,rec,el,idx,ev) {
        //User clicks on a class. Load a grid full of the first page of data from this class.
        if (cmp.lastevent!='itempress') {
            var n = rec.get("n");
            cmp.mask("Loading...");
            var c = this.main.data.getClass(n); //names[cn];
           
            if (c.n) {
           
                //Load schema no matter what.
                this.view.fireEvent('cmd',{
                    cmd: "request",
                    n: c.n,
                    relationship: "descendants",
                    callback: this.browseSchemaCallback,
                    scope: this
                });
           
            } else {
                this.main.feedback.say({
                    title: "Info",
                    message: "Please give this class a name first."
                });
            }
        }
        cmp.lastevent = 'itemclick';
    },
    
    browseSchemaCallback: function (rsp) {
        var me = (rsp.scope||this);
        var c = me.main.data.getClass(rsp.n);
        me.browse(c);
    },
    
    pressclassHandler: function (cmp,rec,el,idx,ev) {
        cmp.lastevent = 'itempress';
        console.log("press " + rec.get("n"));
    },
    
    //TODO: roles 5,6 = top, roles 3,4 = default owner
    browse: function (c) {
    
        var cmp = this.view.down("panel[name='bravobrowsereditorpanel']");
        cmp.mask("Loading...");
    
        //Record this class as the active item.
        this.view.setBrowse(c);
        this.view.setBrowseProperties(this.buildBrowseProperties(c));
        
        var title = this.view.down("tbtext[name='bravobrowsetitle']");
        title.setHtml((this.view.getBrowse().plural||this.view.getBrowse().l));
        
        var top = this.main.data.getRoleDomain(this.main.environment.get("PROJECT"));
        
        //Get page of data of this class.
        this.view.fireEvent('cmd',Ext.apply({
            cmd: "request",
            a: top.a,
            $cid: c.n,
            relationship: "objects",
            callback: this.loadClassCallback,
            page: this.view.getBrowsePage(),
            pagesize: this.view.getBrowsePageSize(),
            usecache: false,
            inventory: true,
            scope: this
        },this.view.getBrowseOptions()));
    },
    
    loadClassCallback: function(rsp) {
        //Load a grid with this class data.
        var me = (rsp.scope||this);
        
        var arr = rsp.desc||[];
        var cmp = me.view.down("dataview[name='bravobrowserchooser']");
        var cmp2 = me.view.down("panel[name='bravobrowsereditorpanel']");
        
        if (arr.length>0) {
        
            //Build columns
            var properties = me.view.getBrowseProperties();
            var cols = me.buildColumns(properties);
            
            var grid = me.view.down("grid[name='bravobrowsergrid']");
            var store = new Ext.data.Store({
                data: rsp.desc
            });
            grid.reconfigure(store,cols);
            
            //record # of rows returned.
            me.view.setBrowseItems(arr.length);
            
            me.view.down("container[name='bravobrowser']").getLayout().setActiveItem(1);
            
            //Buttons.
            me.view.down("button[name='bravobrowserfirstbutton']").setDisabled(me.view.getBrowsePage()<=1);
            me.view.down("button[name='bravobrowserprevbutton']").setDisabled(me.view.getBrowsePage()<=1);
            me.view.down("button[name='bravobrowsernextbutton']").setDisabled(me.view.getBrowseItems()<me.view.getBrowsePageSize());
            me.view.down("button[name='bravobrowsernewbutton']").setHidden(!me.view.getBrowse().defaultparent);
        
        } else {
            me.main.feedback.say({
                title: 'Info',
                message: 'No items found.'
            },me.view);
        
        }
        
        cmp.unmask();
        cmp2.unmask();
    
    },
    
    buildBrowseProperties: function (c) {
        //Build a set of properties for this class.
        //Used for properties columns, search fields, input fields, download field lists.
        //Label is default property.
        var arr = [{l:'Label',n:'l'}];
        if (!c) return arr;
        var exists = {};
        var cn = c.n;
        
        //Defined properties
        var properties = this.main.data.getDescendants(c,{cid:'property'});
        
        //Properties from first example of this class that has been loaded.
        var advancedproperties = [];
        if (this.main.data.classes[cn]) {
            var k = Object.keys(this.main.data.classes[cn])[0];
            var example = this.main.data.getObject(this.main.data.classes[cn][k]);
            advancedproperties = this.buildDynamicProperties(example);
        }
        
        //Combine both sets into single set of properties, remove duplicates.
        for (var i=0,l=properties.length;i<l;i++) {
            if (!exists[properties[i].n]) arr.push(properties[i]);
            exists[properties[i].n] = true;
        }
        
        for (var i=0,l=advancedproperties.length;i<l;i++) {
            if (!exists[advancedproperties[i].n]) arr.push(advancedproperties[i]);
            exists[advancedproperties[i].n] = true;
        }
        
        return arr;
    },
    
    buildDynamicProperties: function (o) {
        var arr = [];
        for (var item in o) {
            if (/^(n|a|l|cid|utc|p|z|sd|ed|ct|pa|ca|t|cr|crb|upd|upb|i|g|ix1|ix2|ix3|r|text|leaf|id|children|iconCls|idx|bravoaliasfield)$/i.test(String(item))) {
                //Skip built-in properties.
            } else {
                arr.push({l:item,n:item});
            }
        }
        return arr;
    },
    
    buildColumns: function (arr) {
        //Given an array of properties, build an arr of columns.
        var cols = [];
        
        cols.push({
            xtype:'actioncolumn',
            hideable: false,
            sortable: false,
            width: 30,
            items: [
                {
                    xtype: 'button',
                    cls: 'detailsbutton',
                    iconCls: 'fa fa-pencil',
                    tooltip: 'Edit',
                    handler: 'griddetailsHandler'
                }
            ]
        });
        
        for (var i=0,l=arr.length;i<l;i++) {
            cols.push({
                text: arr[i].l,
                dataIndex: arr[i].n,
                width: arr[i].columnwidth||110,
                hideable: true
            })
        }
        return cols;
    },
    
    griddetailsHandler: function (cmp,row,col,btn,ev,rec) {
        //open this item in the editor.
        this.edit({a: rec.get("a")});
    },
    
    bravobrowsermenuHandler: function () {
        this.setupBrowser();
    },
    
    bravobrowsersearchHandler: function () {
        this.view.down("container[name='bravobrowser']").getLayout().setActiveItem(2);
        this.search();
    },
    
    bravobrowserfavoritesHandler: function () {
        this.view.down("container[name='bravobrowser']").getLayout().setActiveItem(3);
    },
    
    browsefirstHandler: function () {
        this.view.setBrowsePage(1);
        this.browse(this.view.getBrowse());
        this.view.down("tbtext[name='bravobrowserpage']").setHtml('Page ' + this.view.getBrowsePage());
    },
    
    browseprevHandler: function () {
        this.view.setBrowsePage(this.view.getBrowsePage()-1);
        this.browse(this.view.getBrowse());
        this.view.down("tbtext[name='bravobrowserpage']").setHtml('Page ' + this.view.getBrowsePage());
    },
    
    browsenextHandler: function () {
        this.view.setBrowsePage(this.view.getBrowsePage()+1);
        this.browse(this.view.getBrowse());
        this.view.down("tbtext[name='bravobrowserpage']").setHtml('Page ' + this.view.getBrowsePage());
    },
    
    browsenewHandler: function () {
        //Invalidate cache so that items must be refreshed from server.
        //this.main.cache.invalidate({a:this.view.getTop()});
        
        var singular = (this.main.data.getObject(this.view.getBrowse()["n"]).singular||this.view.getBrowse()["l"]);
        
        this.view.fireEvent('cmd',{
            cmd: "create",
            callback: this.browsenewCallback,
            component: this,
            scope: this,
            data: [{
                cid: this.view.getBrowse()["n"],
                l: "New " + singular,
                pa: this.view.getBrowse()["defaultparent"],
                t: 0,
                z: 0,
                usecache: false
            }]
        });
    },
    
    browsenewCallback: function (rsp) {
        var me = (rsp.scope||this);
        
        //Go to first page, the new items will always have single space (no label)
        me.view.setBrowsePage(1);
        me.browse(me.view.getBrowse());
    },
    
    browsepagesizeHandler: function (cmp,newVal,oldVal,eOpts) {
        this.view.setBrowsePageSize(newVal);
        this.browse(this.view.getBrowse());
    },
    
    browsedownloadHandler: function (cmp) {
    
        //build a field list to define the csv file.
        var arr = this.view.getBrowseProperties();
        var fields = [];
        for (var i=0,l=arr.length;i<l;i++) {
            if (arr[i].cid=='property') fields.push(arr[i].n);
        }
        
        var f = (fields.join(',') || this.main.data.getClass(this.view.getBrowse()).fields ||''); //names[this.view.getBrowse().n]
        
        var top = this.main.data.getRoleDomain(this.main.environment.get("PROJECT"));
        
        this.main.data.get({
            method: "download",
            direction: "out",
            format: "table",
            cid: this.view.getBrowse().n,
            source: top.a,
            descendants: 100,
            page: 1,
            pagesize: 5000,
            fields: f,
            delimiter: ",",
            path: this.view.getBrowse().n + "_export.csv"
        });
    },
    
    
    //---------------------------------------------------------------------------------------
    // Explorer
    //---------------------------------------------------------------------------------------
    
    setupExplorer: function () {
    
        var tabpanel = this.view.down("tabpanel[name='bravotabs']");
        var cmp = this.view.down("container[name='bravotree']");
        if (cmp) tabpanel.remove(cmp);
        
        var bravotree = Ext.create('Ext.Panel',{
            xtype: 'panel',
            name: 'bravotree',
            iconCls: 'fa fa-sitemap',
            layout: 'fit',
            padding: 0
        });
        
        tabpanel.add(bravotree);
        
        var bravoexplorer = Ext.create('Ext.Panel',{
            flex: 1,
            padding: 0,
            name: 'bravoexplorer',
            layout: 'fit'
        });
        
        bravotree.add(bravoexplorer);
        
        var tbrExplorer = Ext.create('Ext.Toolbar',{
            dock: 'top',
            name: 'bravotreeactive',
            overflowHandler: 'menu',
            flex: 1
        });
        bravoexplorer.addDocked(tbrExplorer);
        
        var tbrPagingToolbar = Ext.create('Ext.Toolbar',{
            name: 'bravoexplorepagingtoolbar',
            dock: 'bottom',
            //overflowHandler: 'menu',
            items: [
                {
                    xtype: 'button',
                    iconCls: 'fa fa-step-backward',
                    name: 'bravoexplorerfirstbutton'
                },
                {
                    xtype: 'button',
                    iconCls: 'fa fa-chevron-left',
                    name: 'bravoexplorerprevbutton'
                },
                '-',
                {
                    xtype: 'tbtext',
                    html: 'Page 1',
                    name: 'bravoexplorerpage'
                },
                '-',
                {
                    xtype: 'button',
                    iconCls: 'fa fa-chevron-right',
                    name: 'bravoexplorernextbutton'
                },
                {
                    xtype: 'button',
                    enableToggle: true,
                    iconCls: 'fa fa-plus',
                    name: 'bravoexplorernewbutton'
                },
                {
                    xtype: 'tbspacer',
                    flex: 1
                },
                {
                    xtype: 'xupload',
                    fieldLabel: '',
                    name: 'bravoexploreruploadbutton',
                    buttonOnly: true,
                    width: 40,
                    buttonText: '',
                    buttonConfig: {
                        iconCls: 'fa fa-upload'
                    }
                },
                {
                    xtype: 'button',
                    iconCls: 'fa fa-download',
                    name: 'bravoexplorerdownloadbutton'
                },
                {
                    xtype: 'textfield',
                    width: 100,
                    name: 'bravoexplorersearch'
                },
                {
                    xtype: 'combobox',
                    fieldLabel: 'Items',
                    name: 'bravoexplorerpagesize',
                    labelWidth: 40,
                    width: 150,
                    store: {
                        data: [
                            {text:'10',value:10},
                            {text:'25',value:25},
                            {text:'50',value:50},
                            {text:'75',value:75},
                            {text:'100',value:100},
                            {text:'200',value:200},
                            {text:'250',value:250}
                        ]
                    },
                    queryMode: 'local',
                    displayField: 'text',
                    valueField: 'value',
                    value: 50,
                    editable: false
                }

            ]
        });
        bravoexplorer.addDocked(tbrPagingToolbar);
        
        var dvwExplorer = Ext.create('bravo.Selector',{
            name: 'bravotreechooser',
            ddGroup: 'explore',
            flex: 1
        });
        bravoexplorer.add(dvwExplorer);
    
    },

    
        
    explore: function (o) {
    
        var cmp = this.view.down("dataview[name='bravotreechooser']");
        cmp.mask("Loading..");
        
        this.view.fireEvent('cmd',{
            cmd: "request",
            a: o.a,
            relationship: "children",
            page: this.view.getExplorePage(),
            pagesize: this.view.getExplorePageSize(),
            callback: this.bravotreeCallback,
            usecache: false,
            inventory: true,
            scope: this
        });
    },
    
    bravotreeCallback: function (rsp) {
        var me = (rsp.scope||this);
        
        //Ancestor Toolbar
        var anc = me.view.down("toolbar[name='bravotreeactive']");
        anc.setHtml('');
        anc.show();
        
        var arr = rsp.anc||[];
        
        anc.removeAll();
        
        if (rsp.leaf) {
            anc.add(new Ext.Button({
                text: rsp.leaf.l,
                iconCls: me.getIcon(rsp.leaf),
                textAlign: 'left',
                control:me,
                a: rsp.leaf.a,
                handler: function () {
                    this.control.view.setExplorePage(1);
                    this.control.explore(this);
                    if (this.control.view.getEditMode()=='single') this.control.edit(this);
                }
            }));
           me.view.setExplore(rsp.leaf);
        }
        
        if (arr.length>0) {
            for (var i=arr.length-1;i>=0;i--) {
                anc.add(new Ext.Button({
                    text: arr[i].l,
                    iconCls: me.getIcon(arr[i]),
                    textAlign: 'left',
                    control:me,
                    a: arr[i].a,
                    handler: function () {
                        this.control.view.setExplorePage(1);
                        this.control.explore(this);
                        if (this.control.view.getEditMode()=='single') this.control.edit(this);
                    }
                }));
            }
        }
        
        
        
        //Children Chooser
        var cmp = me.view.down("dataview[name='bravotreechooser']");
        cmp.getStore().removeAll();
        var arr = rsp.desc||[];
        
        for (var i=0,l=arr.length;i<l;i++) {
            arr[i].iconCls = me.getIcon(arr[i]);
        }

        if (arr.length>0) {

            cmp.getStore().loadData(arr);
           
            //record # of rows returned.
            me.view.setExploreItems(rsp.desc.length);
           
            //Buttons.
            me.view.down("button[name='bravoexplorerfirstbutton']").setDisabled(me.view.getExplorePage()<=1);
            me.view.down("button[name='bravoexplorerprevbutton']").setDisabled(me.view.getExplorePage()<=1);
            me.view.down("button[name='bravoexplorernextbutton']").setDisabled(me.view.getExploreItems()<me.view.getExplorePageSize());
           
        }

        cmp.unmask();
        
    },

    explorefirstHandler: function () {
            this.view.setExplorePage(1);
            this.explore(this.view.getExplore());
            this.view.down("tbtext[name='bravoexplorerpage']").setHtml('Page ' + this.view.getExplorePage());
    },
    
    exploreprevHandler: function () {
        this.view.setExplorePage(this.view.getExplorePage()-1);
        this.explore(this.view.getExplore());
        this.view.down("tbtext[name='bravoexplorerpage']").setHtml('Page ' + this.view.getExplorePage());
    },
    
    explorenextHandler: function () {
        this.view.setExplorePage(this.view.getExplorePage()+1);
        this.explore(this.view.getExplore());
        this.view.down("tbtext[name='bravoexplorerpage']").setHtml('Page ' + this.view.getExplorePage());
    },
    
    explorenewHandler: function (cmp) {
        if (cmp.pressed) {
            //add a toolbox.
            this.toolbox();
        } else {
            //remove the toolbox
            var toolbox = this.view.down("container[name='bravotoolboxcontainer']");
            this.view.down("panel[name='bravotree']").removeDocked(toolbox);
        }
    },
    
    exploresearchHandler: function (cmp) {
    
        var cmp2 = this.view.down("dataview[name='bravotreechooser']");
    
        if (cmp.getValue()) {
        
            cmp2.mask("Loading..");
            
            this.view.fireEvent('cmd',{
                cmd: "request",
                a: this.view.getExplore().a,
                relationship: "descendants",
                component: cmp,
                $l: cmp.getValue(),
                //page: this.view.getExplorePage(),
                //pagesize: this.view.getExplorePageSize(),
                page: 1,
                pagesize: 250,
                callback: this.bravotreesearchCallback,
                scope: this
            });
            
        }
    },
    
    explorepagesizeHandler: function (cmp,newVal,oldVal,eOpts) {
        this.view.setExplorePageSize(newVal);
        this.explore(this.view.getExplore());
    },
    
    exploredownloadHandler: function (cmp) {
        var o = this.view.getExplore();
        this.main.data.get({
            method:"download",
            direction:"out",
            format:"json",
            source: o.a,
            ancestors: 0,
            descendants: 100,
            page: 1,
            pagesize: 10000,
            path: o.l + "_export.json"
        });
    },
    
    
    exploreuploadHandler: function (cmp,file) {
        //Insert custom action here to take when file has been uploaded.
        //Uploads are in base64 format, but need to send json in plain text.
        
        if (file.data) {
            var data = file.data.split(',')[1];
            data = Ext.util.Base64.decode(data);
            
            this.main.data.task({
                method:"inline",
                format: "json",
                direction: "in",
                contents: data,
                callback: this.exploreuploadCallback,
                scope: this
            });
        }
    },
    
    exploreuploadCallback: function (rsp) {
        var me = (rsp.scope||this);
        me.refresh('explorer');
    },
    
    chooseitemHandler: function (cmp,rec,el,idx,ev) {
    
        //User clicks on a class. Load a grid full of the first page of data from this class.
        if (cmp.lastevent!='itempress') {
            if (this.view.getEditMode()=='single') this.edit({a:rec.get("a")});
        }
        cmp.lastevent = 'itemclick';
    },
    
    exploreitemHandler: function (cmp,rec,el,idx,ev) {
    
        //User clicks on an item. Load a grid full of the first page of data from this item.
        this.view.setExplorePage(1);
        if (cmp.lastevent!='itempress') {
            this.explore({a: rec.get("a")});
        }
        cmp.lastevent = 'itemclick';
    },
    
    
    pressitemHandler: function (cmp,rec,el,idx,ev) {
        cmp.lastevent = 'itempress';
        if (this.view.getEditMode()=='single') {
            this.view.setEditMode('multi');
            this.setupConnector();
           
            Ext.defer(function () {
                this.connect(rec.data);
            },100,this);
           
           
        } else {
        
            this.view.setEditMode('single');
            this.edit({a:rec.get("a")});
        }
    },
    
    bravotreesearchCallback: function (rsp) {
        var me = (rsp.scope||this);
        
        //Ancestor Toolbar
        var anc = me.view.down("toolbar[name='bravotreeactive']");
        anc.removeAll();
        anc.setHtml('Search Results');
        
        //Leaf: set as active item.
        if (rsp.leaf) me.view.setExplore(rsp.leaf);
        
        
        //Children Chooser
        var cmp = me.view.down("dataview[name='bravotreechooser']");
        cmp.getStore().removeAll();
        var arr = rsp.desc||[];
        
        if (arr.length>0) {

            cmp.getStore().loadData(arr);
           
            //record # of rows returned.
            me.view.setExploreItems(arr.length);
           
            rsp.component.setFieldLabel("");
            rsp.component.setValue("");
        
        } else {
            //rsp.component.setFieldLabel("no results");
            //TODO: set style here for no results.
        }
        
        cmp.unmask();
        
    },
    
    //---------------------------------------------------------------------------------------
    // Toolbox
    //---------------------------------------------------------------------------------------
    
    
    toolbox: function () {
    
        this.view.fireEvent('cmd',{
            cmd: "request",
            a: this.main.environment.get("DEFAULT_TOOLBOX"),
            relationship: "descendants",
            callback: this.toolboxCallback,
            scope: this
        });
    
    },
    
    toolboxCallback: function(rsp) {
        //Load toobox with classlist.
        var me = (rsp.scope||this);
        var arr = me.main.data.objectToArray(me.main.data.classes['class']);
        
        var store = Ext.create('Ext.data.Store',{
            data: arr
        });
        
        var toolbox = Ext.create('Ext.Container',{
            width: 210,
            style: 'background-color: #EEEEFF;',
            dock: 'left',
            name: 'bravotoolboxcontainer',
            layout: 'fit',
            items: [
                {
                    xtype: 'selector',
                    name: 'bravotoolboxchooser',
                    ddGroup: 'explore',
                    store: store
                }
            ]
        });
           
        me.view.down("panel[name='bravotree']").addDocked(toolbox);
    
    },

    
    //---------------------------------------------------------------------------------------
    // Connector
    //---------------------------------------------------------------------------------------
    
    setupConnector: function () {

        var form = this.view.down("panel[name='rightpanel']");
        form.removeAll();
        form.setTitle('Relationships');
        form.setIconCls('fa fa-exchange');
        
        var bravoconnector = Ext.create('Ext.Panel',{
            flex: 1,
            padding: 0,
            name: 'bravoconnector',
            layout: 'fit'
        });
        
        form.add(bravoconnector);
        
        var tbrConnector = Ext.create('Ext.Toolbar',{
            dock: 'top',
            name: 'bravoconnectactive',
            overflowHandler: 'menu',
            flex: 1
        });
        bravoconnector.addDocked(tbrConnector);
        
        var tbrPagingToolbar = Ext.create('Ext.Toolbar',{
            name: 'bravoconnectorpagingtoolbar',
            dock: 'bottom',
            //overflowHandler: 'menu',
            items: [
                {
                    xtype: 'button',
                    iconCls: 'fa fa-step-backward',
                    name: 'bravoconnectorfirstbutton'
                },
                {
                    xtype: 'button',
                    iconCls: 'fa fa-chevron-left',
                    name: 'bravoconnectorprevbutton'
                },
                '-',
                {
                    xtype: 'tbtext',
                    html: 'Page 1',
                    name: 'bravoconnectorpage'
                },
                '-',
                {
                    xtype: 'button',
                    iconCls: 'fa fa-chevron-right',
                    name: 'bravoconnectornextbutton'
                },
                {
                    xtype: 'button',
                    iconCls: 'fa fa-sort-alpha-asc',
                    name: 'bravoconnectorsortbutton'
                },
                {
                    xtype: 'tbspacer',
                    flex: 1
                },
                {
                    xtype: 'textfield',
                    width: 100,
                    name: 'bravoconnectorsearch'
                },
                {
                    xtype: 'combobox',
                    fieldLabel: 'Items',
                    name: 'bravoconnectorpagesize',
                    labelWidth: 40,
                    width: 150,
                    store: {
                        data: [
                            {text:'10',value:10},
                            {text:'25',value:25},
                            {text:'50',value:50},
                            {text:'75',value:75},
                            {text:'100',value:100},
                            {text:'200',value:200},
                            {text:'250',value:250}
                        ]
                    },
                    queryMode: 'local',
                    displayField: 'text',
                    valueField: 'value',
                    value: 50,
                    editable: false
                }
            ]
        });
        bravoconnector.addDocked(tbrPagingToolbar);
        
        var dvwChooser = Ext.create('bravo.Selector',{
            name: 'bravoconnectchooser',
            ddGroup: 'explore'
        });
        bravoconnector.add(dvwChooser);
    
    },
    
    
    connect: function (o) {
        var cmp = this.view.down("dataview[name='bravoconnectchooser']");
        if (cmp) {
            cmp.mask("Loading..");
               
            this.view.fireEvent('cmd',{
                cmd: "request",
                a: o.a,
                relationship: "children",
                page: this.view.getConnectPage(),
                pagesize: this.view.getConnectPageSize(),
                callback: this.bravoconnectCallback,
                usecache: false,
                inventory: true,
                scope: this
            });
        }
    },
    
    
    bravoconnectCallback: function (rsp) {
        var me = (rsp.scope||this);
        
        //Ancestor Toolbar
        var anc = me.view.down("toolbar[name='bravoconnectactive']");
        anc.setHtml('');
        
        var arr = rsp.anc;
        var leaf = rsp.leaf;
        anc.removeAll();
        
        anc.add(new Ext.Button({
            text: leaf.l,
            iconCls: me.getIcon(leaf),
            textAlign: 'left',
            control:me,
            a: leaf.a,
            handler: function () {
                this.control.view.setConnectPage(1);
                this.control.connect(this);
            }
        }));
        
        
        if (arr.length>0) {
            for (var i=arr.length-1;i>=0;i--) {
                anc.add(new Ext.Button({
                    text: arr[i].l,
                    iconCls: me.getIcon(arr[i]),
                    textAlign: 'left',
                    control:me,
                    a: arr[i].a,
                    handler: function () {
                        this.control.view.setConnectPage(1);
                        this.control.connect(this);
                    }
                }));
            }
        }
        
        //Leaf
        if (rsp.leaf) me.view.setConnect(rsp.leaf);
        
        //Children Chooser
        var cmp = me.view.down("dataview[name='bravoconnectchooser']");
        cmp.getStore().removeAll();
        var arr = rsp.desc;
        
        for (var i=0,l=arr.length;i<l;i++) {
            arr[i].iconCls = me.getIcon(arr[i]);
        }
        
        if (arr.length>0) {

            cmp.getStore().loadData(arr);
            
            //record # of rows returned.
            me.view.setConnectItems(rsp.desc.length);
            
            //Buttons.
            me.view.down("button[name='bravoconnectorfirstbutton']").setDisabled(me.view.getConnectPage()<=1);
            me.view.down("button[name='bravoconnectorprevbutton']").setDisabled(me.view.getConnectPage()<=1);
            me.view.down("button[name='bravoconnectornextbutton']").setDisabled(me.view.getConnectItems()<me.view.getConnectPageSize());
        }
            
        //Toolbar
        me.view.down("toolbar[name='bravoeditortoolbar']").hide();
        
        cmp.unmask();
        
    },

    connectfirstHandler: function () {
        this.view.setConnectPage(1);
        this.connect(this.view.getConnect());
        this.view.down("tbtext[name='bravoconnectorpage']").setHtml('Page ' + this.view.getConnectPage());
    },
    
    connectprevHandler: function () {
        this.view.setConnectPage(this.view.getConnectPage()-1);
        this.connect(this.view.getConnect());
        this.view.down("tbtext[name='bravoconnectorpage']").setHtml('Page ' + this.view.getConnectPage());
    },
    
    connectsortHandler: function () {
    
        this.main.feedback.say({
            title: this.view.getConnect().l,
            message: 'Do you want to resort these items alphabetically?  The current sort order will be overwritten.',
            buttons: Ext.Msg.OKCANCEL,
            callback: this.alphabetizeCallback,
            scope:this
        },this.view);

    },
    
    alphabetizeCallback: function (s,b,sp) {
    
        if (s!='cancel') {
    
            //Insert code here to sort the entire tree by alphabetical.  (change all z's to 0)
            //Reorganize the local store.
            var cmp = this.view.down("dataview[name='bravoconnectchooser']");
            var store = cmp.store;
            
            //Reorder the entire set of data
            for (var i=0,l=store.data.items.length;i<l;i++) {
                var rec = store.data.items[i];
                this.view.fireEvent('cmd',{
                    cmd: "put",
                    objects: [this.main.data.getObject(rec.get("a"))],
                    z: 0
                });
            }
            
            //Invalidate cache so that items must be refreshed from server.
            this.main.cache.invalidate(this.main.data.getObject(this.view.getConnect()));
            
            
            //Refresh the view.
            Ext.defer(function() {
                this.refresh('explorer,connector');
            }, 100, this);
        
        }
    
    },
    
    connectnextHandler: function () {
        this.view.setConnectPage(this.view.getConnectPage()+1);
        this.connect(this.view.getConnect());
        this.view.down("tbtext[name='bravoconnectorpage']").setHtml('Page ' + this.view.getConnectPage());
    },
    
    connectpagesizeHandler: function (cmp,newVal,oldVal,eOpts) {
        this.view.setConnectPageSize(newVal);
        this.connect(this.view.getConnect());
    },
    
    chooseconnectitemHandler: function (cmp,rec,el,idx,ev) {
    
        //User clicks on a class. Load a grid full of the first page of data from this class.
        if (cmp.lastevent!='itempress') {
            this.connect({a:rec.get("a")});
        }
        cmp.lastevent = 'itemclick';
    },
    
    connectitemHandler: function (cmp,rec,el,idx,ev) {
    
        //User clicks on an item. Load a grid full of the first page of data from this item.
        this.view.setConnectPage(1);
        if (cmp.lastevent!='itempress') {
            this.connect({a: rec.get("a")});
        }
        cmp.lastevent = 'itemclick';
    },
    
    connectsearchHandler: function (cmp) {
    
        var cmp2 = this.view.down("dataview[name='bravoconnectchooser']");
    
        if (cmp.getValue()) {
        
            cmp2.mask("Loading..");
            
            this.view.fireEvent('cmd',{
                cmd: "request",
                a: this.view.getConnect().a,
                relationship: "descendants",
                component: cmp,
                l: cmp.getValue(),
                //page: this.view.getConnectPage(),
                //pagesize: this.view.getConnectPageSize(),
                page: 1,
                pagesize: 250,
                callback: this.bravoconnectsearchCallback,
                scope: this
            });
            
        }
    },
    
    bravoconnectsearchCallback: function (rsp) {
        var me = (rsp.scope||this);
        
        //Ancestor Toolbar
        var anc = me.view.down("toolbar[name='bravoconnectactive']");
        anc.removeAll();
        anc.setHtml('Search Results');
        
        //Leaf
        if (rsp.leaf) me.view.setConnect(rsp.leaf);
        
        
        //Children Chooser
        var cmp = me.view.down("dataview[name='bravoconnectchooser']");
        cmp.getStore().removeAll();
        var arr = rsp.desc||[];
        
        if (arr.length>0) {

            cmp.getStore().loadData(arr);
           
            //record # of rows returned.
            me.view.setConnectItems(arr.length);

            rsp.component.setFieldLabel("");
            rsp.component.setValue("");
        
        } else {
        
            //rsp.component.setFieldLabel("no results");
            //TODO: set style here for no results.
        }
        
        cmp.unmask();
        
    },
    
    connectHandler: function (cmp,src,dest,ev) {
        //This is triggered from any selector afer a recorddrop event is raised.
        
        src.dp = src.data.data;
        if (dest.component.name == "bravotreechooser") dest.dp = this.view.getExplore();
        if (dest.component.name == "bravoconnectchooser") dest.dp = this.view.getConnect();
        
        this.view.setConnectSource(src);
        this.view.setConnectDestination(dest);
        
        if ((dest.data)&&(dest.data.get("pa")==src.data.get("pa"))) {
            //Organize
            this.organizeHandler(src,dest);
        } else {
            /*  Remove temporarily.  Just use keyboard shortucts.  Move the relate handler to another location
            if (ev.shiftKey||ev.altKey||ev.ctrlKey) {
                //Shortcuts
                //user is modifying the drag with one of the key selections.
                this.relateHandler(src,dest);
            } else {
                //New, Move or Copy
                if (src.component.name=="bravotoolboxchooser") {
                    this.newHandler(src,dest);
                } else {
                    this.movecopyHandler(src,dest);
                }
            }
            */
            if (ev.altKey) {
                if (ev.shiftKey) {
                    this.deepHandler(src,dest);
                } else {
                    this.shallowHandler(src,dest);
                }
           
            } else {
                if (ev.ctrlKey) {
                    this.relateHandler(src,dest);
                } else {
                    if (src.component.name=="bravotoolboxchooser") {
                        this.newHandler(src,dest);
                    } else {
                        this.moveHandler(src,dest);
                    }
                }
            }
           
        }
    },
    
    relateHandler: function (src,dest) {
        //Start with a type 1 link.  TODO: alter for permissions.
        
        //Invalidate cache so that items must be refreshed from server.
        this.main.cache.invalidate(dest.dp);
        
        var sp = {
            callback: this.relateCallback,
            component: this,
            scope: this
        };
        this.main.data.createLink({
            parent: dest.dp,
            child: src.dp,
            t: 1,
            z: dest.idx||0
        },sp);

       
    },
    
    relateCallback: function (rsp) {
         Ext.defer(function() {
            this.refresh('explorer,connector');
        }, 100, this);
    },
    
    organizeHandler: function (src,dest) {

        //Reorganize the local store.
        var srcStore = src.component.store;
        srcStore.remove(src.data);
        var destStore = dest.component.store;
        if (isNaN(dest.idx)) dest.idx = 0;
        destStore.insert(dest.idx, src.data);
        
        var offset=0;
        if (dest.component.name=='bravotreechooser') {
            offset = (this.view.getExplorePage()-1)*this.view.getExplorePageSize();
        } else {
            offset = (this.view.getConnectPage()-1)*this.view.getConnectPageSize();
        }
        if (!Ext.isNumber(offset)) offset = 0;
        
        //Reorder the entire set of data
        for (var i=0,l=destStore.data.items.length;i<l;i++) {
            var rec = destStore.data.items[i];
            this.view.fireEvent('cmd',{
                cmd: "put",
                objects: [this.main.data.getObject(rec.get("a"))],
                z: i+offset
            });
        }
        
        //Invalidate cache so that items must be refreshed from server.
        this.main.cache.invalidate(dest.dp);
        
        
        //Refresh the view.
        Ext.defer(function() {
            this.refresh('explorer,connector');
        }, 100, this);
        
    },
    
    newHandler: function (src,dest) {
    
        //Reorganize the local store.
        var srcStore = src.component.store;
        //Ext.apply(src.data.data,{
            //l: "New " + (src.dp["singular"]||"Item")
        //});
        var destStore = dest.component.store;
        destStore.insert(dest.idx, src.data);
        
        //Reorder the entire set of data, except for the new one being created.
        for (var i=0,l=destStore.data.items.length;i<l;i++) {
            var rec = destStore.data.items[i];
            var data = this.main.data.getObject(rec.get("a"));
            if (data) {
                this.view.fireEvent('cmd',{
                    cmd: "put",
                    objects: data,
                    z: i
                });
            }
        }
        
        //Invalidate cache so that items must be refreshed from server.
        this.main.cache.invalidate(dest.dp);
        
        if (isNaN(dest.idx)) dest.idx = 0;
        
        this.view.fireEvent('cmd',{
            cmd: "create",
            callback: this.newCallback,
            component: this,
            source: src,
            destination: dest,
            scope: this,
            data: [{
                cid: src.dp["n"],
                l: "New " + (src.dp["singular"]||(src.dp.l)||"Item"),
                pa: dest.dp["a"],
                t: 0,
                z: dest.idx
            }]
        });

    },
    
    newCallback: function (rsp) {
        //This function fires after a new item is created.
        var me = (rsp.scope||this);
        //Refresh the view.
        Ext.defer(function() {
            me.refresh('explorer,connector');
        }, 100, me);
    
    },
    
    movecopyHandler: function (src,dest) {
        this.main.feedback.say({
            title: dest.dp.l,
            message: 'Do you want to move ' + src.dp["l"] + ' to ' + dest.dp["l"] + ' or make a copy?',
            buttons: Ext.Msg.YESNOCANCEL,
            buttonText: {yes: 'Move It',no:'Make Copy',cancel:'Cancel'},
            callback: this.movecopyCallback,
            scope:this
        },this.view);

    },

    movecopyCallback: function (s,b,sp) {
        //Choice : Move, Copy or More
        switch (s) {
            case "yes":
                //Move
                sp.scope.moveHandler();
                break;
            case "no":
                //Copy
                sp.scope.copyHandler();
                break;
        }
    },
    
    moveHandler: function (src,dest) {
    
        //Get items involved in the move.
        var s = this.main.data.getObject(this.view.getConnectSource().dp["a"]);
        var d = this.main.data.getObject(this.view.getConnectDestination().dp["a"]);
        
        //Invalidate cache so that items must be refreshed from server.
        this.main.cache.invalidate(this.main.data.getObject(s.pa));  //needs to be the parent of the source
        this.main.cache.invalidate(d); //the destination has already been adjusted to be the parent.
        
        if (isNaN(dest.idx)) dest.idx = 0;
        
        //Update the item.
        this.view.fireEvent('cmd',{
            cmd: "put",
            objects: [s],
            pa: d.a,
            z: ((d.z) ? dest.idx+1 : 0)
        });
        
        //Insert z-order
        
        //Refresh the view.
        Ext.defer(function() {
            this.refresh('explorer,connector');
        }, 100, this);
    },
    
    copyHandler: function () {
        this.main.feedback.say({
            title: this.view.getConnectDestination().dp["l"],
            message: 'Include all the contents of ' + this.view.getConnectSource().dp["l"] + '?',
            buttons: Ext.Msg.YESNO,
            buttonText: {yes: 'Include All Contents',no:"Only " + this.view.getConnectSource().dp["l"]},
            callback: this.copyCallback,
            scope: this
        },this.view);
    },
    
    copyCallback: function (s,b,sp) {
        switch (s) {
            case "yes":
                sp.scope.deepHandler();
                break;
            case "no":
                this.shallowHandler();
                break;
        }
    },
    
    shallowHandler: function () {
        console.log("going to shallow copy " + this.view.getConnectSource().dp["l"] + " to " + this.view.getConnectDestination().dp["l"]);
    },
    
    deepHandler: function () {
        console.log("going to deep copy " + this.view.getConnectSource().dp["l"] + " to " + this.view.getConnectDestination().dp["l"]);
    },
    
    
    //---------------------------------------------------------------------------------------
    // Editor
    //---------------------------------------------------------------------------------------
    
    edit: function (mx) {
    
        var o = this.main.data.getObject(mx);
        
        if (o) {
            this.view.setEditor(o);
        
            var c = (this.main.data.getClass(o));
            
            if (c) {
                //Class schema is already loaded.
                this.view.fireEvent('cmd',{
                    cmd: "request",
                    a: c.a,
                    relationship: "descendants",
                    callback: this.editCallback,
                    scope: this
                });
            } else {
                //Class schema needs to be loaded.
                this.view.fireEvent('cmd',{
                    cmd: "request",
                    n: o.cid,
                    relationship: "descendants",
                    callback: this.schemaCallback,
                    scope: this
                });
            }
        } else {
            //The item not has been loaded.
            this.view.fireEvent('cmd',{
                cmd: "request",
                a: a,
                relationship: "item",
                callback: this.editprerequisiteCallback,
                scope: this
            });
        }
    },
    
    editprerequisiteCallback: function (rsp) {
        var me = (rsp.scope||this);
        me.edit({a:rsp.a});
    },
    
    schemaCallback:function (rsp) {
        var me = (rsp.scope||this);
        var o = me.main.data.names[rsp.n][0];
        if (o) {
            me.view.fireEvent('cmd',{
                cmd: "request",
                a: me.main.data.names[rsp.n][0].a,
                relationship: "descendants",
                callback: me.editCallback,
                scope: me
            });
        } else {
            me.editCallback(rsp);
        }
    },
    
    editCallback: function (rsp) {
        
        var me = (rsp.scope||this);
    
        var o = me.view.getEditor();
        var c = (me.main.data.getClass(o)||{l:o.cid});
        
        var role = me.main.data.getRole(o);
        
        var panel = me.view.down("panel[name='rightpanel']");
        panel.removeAll();
        panel.removeDocked();
        panel.setTitle('Edit');
        panel.setIconCls('fa fa-pencil');
        
        //Form
        var form = Ext.create('Ext.form.Panel',{
            scrollable: 'vertical'
        });
        panel.add(form);
        
        
        
        //Properties Tab
        //--------------------------------------------
        var tabProperties = Ext.create('Ext.tab.Panel',{
            name: 'bravopropertiestabs',
            padding: 10,
            tabBarHeaderPosition: 2,
            activeTab: (me.view.getActivePropertiesTab()=="bravoadvanced") ? 1 : 0,
            activeItem: (me.view.getActivePropertiesTab()=="bravoadvanced") ? 1 : 0
        });
        form.add(tabProperties);
        
        tabProperties.setTitle(o.l.toString());
        tabProperties.setIconCls(o.iconCls||'fa fa-cube');
        
        
        //---------------------------------------------------------
        //Basic Properties
        //---------------------------------------------------------
        //need to iterate over tree and build into groups.
       
        var tabBasicProperties = Ext.create('Ext.form.Panel',{
            iconCls: 'fa fa-file-text-o',
            name: 'bravobasic'
        });
        tabProperties.add(tabBasicProperties);
    
        //Fieldset
        grpLabel = Ext.create('Ext.form.FieldSet',{
            title: c.l,
            collapsible: false
        });
        tabBasicProperties.add(grpLabel);
        
        //Label Field
        var inp = 'widget.textfield';
        if (role<3) inp = 'widget.displayfield';
        var fldLabel = Ext.create(inp,{
            name: 'bravolabelfield',
            fieldLabel: 'Title',
            labelWidth: 45,
            name: 'l',
            width: '100%',
            value: o.l
        });
        grpLabel.add(fldLabel);
        
        //Build either properties or folders.  Only allow the nesting to be properties or folder/properties.
        var ch = me.main.data.getChildren(c);
    
        for (var i=0,l=ch.length;i<l;i++) {
           
            switch (ch[i].cid) {
                case "property":
                    var inp = ch[i].input||'textfield';
                    if (role<3) inp = 'displayfield';
                    tabBasicProperties.add(Ext.create("widget."+inp,{
                        fieldLabel: ch[i].l,
                        name: ch[i].n,
                        width: '100%',
                        value: o[ch[i].n]
                    }));
                    break;
                case "folder":
                    var fst = Ext.create('Ext.form.FieldSet',{
                        title: ch[i].l,
                        collapsible: true,
                        collapsed: false,
                        padding: 10
                    });
                    tabBasicProperties.add(fst);

                    //Add properties to this folder.
                    var gch = me.main.data.getChildren(ch[i]);
                    for (var j=0,l2=gch.length;j<l2;j++) {
                        var inp = gch[j].input||'textfield';
                        if (role<3) inp = 'displayfield';
                        fst.add(Ext.create("widget."+inp,{
                            fieldLabel: gch[j].l,
                            name: gch[j].n,
                            width: '100%',
                            value: o[gch[j].n]
                        }));
                    }
                    break;
                default:
                    break;
            }
        }

        
        //---------------------------------------------------------
        // Advanced properties
        //---------------------------------------------------------
        
        //Build list of basic and advanced properties.
        var reserved = me.main.util.buildArray('n,a,l,cid,utc,p,z,sd,ed,ct,pa,ca,t,i,g,cr,crb,upd,upb,ix1,ix2,ix3,r,id,children,leaf,xtype,cls,label,name,text,mt,dt');
        var properties = {};
        var inputs = [];
        
        //Default properties -- removed.
        //properties['label'] = '';
        
        //Defined properties.
        var arr = me.main.data.getDescendants(c,{cid:'property'});
        
        for (var i=0,l=arr.length;i<l;i++) {
            properties[arr[i].n] = '';
        }
        
        //All properties attached to this item, but don't allow any built-in properties to be edited unless they are defined.
        var re = new RegExp("^(?:" + reserved.join('|') + ")$","i");
        for (var item in o) {
            if (!re.test(item)) properties[item]= '';
        }
        //Build list of inputs.
        for (var item in properties) {
            if (Ext.isDate(o[item])) {
                v = me.main.util.dateToISO8601(o[item]);
            } else {
                v = o[item];
            }
            inputs.push({property:item,value:v});
        }
        //Blank line for first new entry.
        inputs.push({property:'',value:''});
        
        
        var tabAdvancedProperties = Ext.create('Ext.Container',{
            iconCls: 'fa fa-table',
            name: 'bravoadvanced'
        });
        tabProperties.add(tabAdvancedProperties);
        
        var strCustom = Ext.create('Ext.data.Store',{
            data: inputs
        });
        
        var grdCustom;
        
        if (role>2) {
        
            grdCustom = Ext.create('Ext.grid.Panel',{
                name: 'bravocustompropertiesgrid',
                hideHeaders: false,
                markDirty: false,
                scrollable: 'vertical',
                plugins: {
                    ptype: 'cellediting',
                    clicksToEdit: 1
                },
                viewConfig: {
                    markDirty: false
                },
                columns: [
                    {
                        text:'Property',
                        dataIndex:'property',
                        width:'50%',
                        editor: {
                            xtype: 'textfield'
                        },
                        editable: true
                    },
                    {
                        text:'Value',
                        dataIndex:'value',
                        width:'50%',
                        editor: {
                            xtype: 'textfield',
                            name: 'value'
                        },
                        editable: true
                    }
                ],
                store: strCustom
            });
        } else {
            grdCustom = Ext.create('Ext.grid.Panel',{
                name: 'bravocustompropertiesgrid',
                hideHeaders: false,
                markDirty: false,
                scrollable: 'vertical',
                viewConfig: {
                    markDirty: false
                },
                columns: [
                    {
                        text:'Property',
                        dataIndex:'property',
                        width:'50%'
                    },
                    {
                        text:'Value',
                        dataIndex:'value',
                        width:'50%'
                    }
                ],
                store: strCustom
            });
        }
        tabAdvancedProperties.add(grdCustom);
        
        Ext.defer(function() {
            tabProperties.setActiveTab((me.view.getActivePropertiesTab()=="bravoadvanced") ? 1 : 0);
        }, 1, me);
        
        //Widgets
        //--------------------------------------------
        //Insert custom widgets here.
        if (role>=3) {
            var desc = me.main.data.getDescendants(c);
            
            //Build groups and inputs.
            for (var i=0,l=desc.length;i<l;i++) {
                if (!/^(folder|property)$/i.test(String(desc[i].cid))) {
                    if (Ext.ClassManager.aliasToName["widget."+desc[i].cid]) {
                        var wdg = Ext.create("widget."+desc[i].cid,{
                            dataprovider: o,
                            fieldLabel: arr[i].l,
                            name: arr[i].n,
                            width: '100%',
                            value: o[arr[i].n]
                        });
                        form.add(wdg);
                    }
                }
            }
            
            //Temp
            var wdg = Ext.create('Ext.Component',{
                        //html: 'Insert custom widget here to do special functions on ' + (c.plural||c.l)
                    });
                    form.add(wdg);
        
        }
        
        //Contents Panel
        //--------------------------------------------
        var grpContents = Ext.create('Ext.form.FieldSet',{
            title: 'Contents',
            name: 'bravocontentspanel',
            collapsible: true,
            collapsed: true,
            padding: 0,
            items: [
                {
                    xtype: 'container',
                    padding: 10
                }
            ]
        });
        form.add(grpContents);
        
        if (role>2) {
            var fldContents = Ext.create('Ext.form.field.TextArea',{
                name: 'bravocontentsfield',
                width: '100%',
                height: 300,
                name: 'ct',
                value: o.ct
            });
        } else {
            var fldContents = Ext.create('Ext.Panel',{
                name: 'bravocontentsfield',
                width: '100%',
                height: 300,
                name: 'ct',
                html: o.ct
            });
        }
        grpContents.items.getAt(0).add(fldContents);
        
        
        
                
        //About Panel
        //--------------------------------------------
        var grpAbout = Ext.create('Ext.form.FieldSet',{
            title: 'About',
            name: 'bravoaboutpanel',
            collapsible: true,
            collapsed: false,
            padding: 0,
            items: [
                {
                    xtype: 'container',
                    padding: 10
                }
            ]
        });
        form.add(grpAbout);
        
        var inp = 'widget.textfield';
        if (role<3) inp = 'widget.displayfield';
        
        //Ancestry
        var anc = [o.l];
        var obj = me.main.data.getParent(o);
        while (obj) {
            anc.push(obj.l);
            obj = me.main.data.getParent(obj);
        }
       
        var dspAncestry = Ext.create('widget.displayfield',{
            value: anc.reverse().join(' > ')
        });
        grpAbout.items.getAt(0).add(dspAncestry);
        
        //Alias
        var fldAlias = Ext.create('widget.displayfield',{
            name: 'bravoaliasfield',
            fieldLabel: 'Alias',
            value: o.a,
            width: '100%',
            maxLength: 255,
            flex: 1
        });
        grpAbout.items.getAt(0).add(fldAlias);
        
        //Name
        var fldName = Ext.create(inp,{
            name: 'bravonamefield',
            fieldLabel: 'Name',
            name: 'n',
            width: '100%',
            maxLength: 255,
            value: o.n
        });
        grpAbout.items.getAt(0).add(fldName);
        
        if (role>4) {
        
            //Class Selector
            var clist = new Ext.data.Store({
                data: me.main.data.objectToArray(me.main.data.classes['class']),
                sorters: [{
                     property: 'l',
                     direction: 'ASC'
                 }]
            });
            var cboClasses = Ext.create('Ext.form.field.ComboBox',{
                //id: 'bravoclassfield',
                store: clist,
                queryMode: 'local',
                displayField: 'l',
                valueField: 'n',
                name: 'cid',
                fieldLabel: 'Class',
                editable: false,
                width: '100%',
                value: o.cid
            });
            grpAbout.items.getAt(0).add(cboClasses);
            
            
            //Type Selector
            var tlist = new Ext.data.Store({
                data: [
                    {value: 1, text: 'Shortcut'},
                    {value: 2, text: 'Access'},
                    {value: 3, text: 'Limited Edit'},
                    {value: 4, text: 'Full Edit'},
                    {value: 5, text: 'Admin'},
                    {value: 6, text: 'Tech'},
                    {value: 7, text: 'Favorite'}
                ]
            });
            var cboTypes = Ext.create('Ext.form.field.ComboBox',{
                store: tlist,
                queryMode: 'local',
                displayField: 'text',
                valueField: 'value',
                name: 't',
                fieldLabel: 'Relationship Type',
                editable: false,
                width: '100%',
                value: o.t
            });
            if (o.t>0) grpAbout.items.getAt(0).add(cboTypes);
        
        }
        
        //Details
        var tpl = new Ext.Template('<p>Created: {cr}</p><p>Last Updated: {upd}</p>');
        var cmpDetails = Ext.create('Ext.Component',{
            html: tpl.apply(o)
        });
        grpAbout.items.getAt(0).add(cmpDetails);

        //Toolbar customizations.
        Ext.defer(function() {
            var o = me.getFavoriteObject(me.view.getEditor());
            me.view.down("button[name='editorfavoritesbutton']").setIconCls((o) ? "fa fa-heart": "fa fa-heart-o");
            me.view.down("toolbar[name='bravoeditortoolbar']").show();
            
            //Don't need tree button if explorer is active.
            me.view.down("button[name='editortreebutton']").setDisabled(me.view.getActiveTab()=="bravotree");
            
        }, 100, me);
    },
    
    commitHandler: function (cmp) {
        //Deferred to give the store a chance to update before the commit is processed.
        Ext.defer(function() {
            var o = this.view.getEditor();
            var me = this;
            
            if (cmp.name=='value') {
                var cmp2 = this.view.down("grid[name='bravocustompropertiesgrid']");
                var store = cmp2.getStore();
                var sp = {}
                store.each(function (rec) {
                    var p = rec.get("property");
                    var v = rec.get("value");
                    if (p&&v) {
                    
                        //Special handling for dates to allow raw date strings to be typed into advanced properties fields.
                        v = me.main.util.stringToDate(v)||v;
                        
                        //if it is a date then refresh the component with the date string.
                        if (Ext.isDate(v)) {
                            rec.set(p,me.main.util.dateToISO8601(v));
                        }
                        sp[p] = v;
                    }
                });
                cmp2.reconfigure(store);
                Ext.defer(function() {this.refresh('browser');}, 500, this);
                
                this.view.fireEvent('cmd',Ext.apply({
                    cmd: "put",
                    objects: [this.view.getEditor()]
                },sp));
            }
        }, 100, this);
    },
    
    dirtyHandler: function (cmp) {
        if (!cmp.isEditorComponent) {
            var sp = {};
            var p = cmp.name;
            var v = cmp.getValue();
            var o = this.view.getEditor();
            if (p&&v) {
           
                if (p=='a') {
                    //handle alias changes differently
                } else {
           
                    sp[p] = v;
                    this.view.setDirty(true);
                    this.view.fireEvent('cmd',Ext.apply({
                        cmd: "put",
                        objects: [o]
                    },sp));
           
                    //Special consideration for labels.  Redraw the UI so that cms is up to date.
                    //Delay it slightly so the cache has a chance to be updated.
                    if (p=='l') {
                        Ext.defer(function() {
                            var tabpanel = this.view.down("tabpanel[name='bravotabs']");
                            if (tabpanel.getActiveTab().name=='bravotree') {
                                this.refresh('explorer');
                            } else {
                                this.refresh('browers');
                            }
                            
                            var panel = this.view.down("panel[name='rightpanel']");
                            var tabs = this.view.down("tabpanel[name='bravopropertiestabs']");
                            panel.setTitle(v);
                            tabs.setTitle(v);
                        }, 1000, this);
                    } else {
                        //disabled until determine effect on performance and alternatives to keep components in sync
                        //Ext.defer(function() {this.refresh('browser');}, 1000, this);
                    }
                }
            }
        }
    },
    
    refresh: function (s) {
        //Force these requests to come from the server.
        if (s.indexOf('editor')>=0) this.edit(this.view.getEditor());
        if (s.indexOf('explorer')>=0) this.explore(this.view.getExplore(),true);
        if (s.indexOf('browser')>=0) this.browse(this.view.getBrowse(),true);
        if (s.indexOf('connector')>=0) this.connect(this.view.getConnect(),true);
    },
    
    getIcon: function (o) {
        if (o.iconCls) return o.iconCls;
        var c = this.main.data.getClass(o);
        if (!c) return 'fa fa-cube';
        return (c.icon) ? 'fa fa-'+c.icon : 'fa fa-cube';
    },
    
    custompropertieseditHandler: function (grid,td,colIndex,rec,tr,rowIndex,ev,eOpts) {
        var total = grid.getStore().getCount();
        if (rowIndex==total-1) {
            //add a new line
            grid.getStore().add({property:'',value:''});
        }
    },
    
    refreshHandler: function () {
        //TODO: This didn't work.  Need to test/fix.
        this.main.cache.reset();
    },
    
    learnpropertiesHandler: function () {
        //Using all properties in advanced handler make sure that the class contains a definition for each property.
        var o = this.view.getEditor();
        var c = this.main.data.getClass(o);
        var newproperties = {};
        var properties = {};
        var list = [];
        
        //Defined properties
        var arr = this.main.data.getDescendants(c,{cid:'property'});
        
        for (var i=0,l=arr.length;i<l;i++) {
            properties[arr[i].n] = '';
        }
        
        //Undefined properties
        var re = new RegExp("^(?:n|a|l|cid|utc|p|z|sd|ed|ct|pa|t|i|g|cr|crb|upd|upb|ix1|ix2|ix3|r|id|children|leaf|xtype|cls|label|name|text|mt|dt|ca)$","i");
        for (var item in o) {
            if (!re.test(item)) newproperties[item]= '';
        }
        
        for (var item in newproperties) {
            //If it is not already defined, then create a new property.
            if (!properties.hasOwnProperty(item)) {
                list.push(item);
                this.view.fireEvent('cmd',{
                    cmd: "create",
                    callback: Ext.emptyFn,
                    component: this,
                    scope: this,
                    data: [{
                        a: c.a + "." + item,
                        l: item,
                        input: 'textfield',
                        n: item,
                        cid: 'property',
                        t: 0,
                        pa: c.a
                    }]
                });
           
            }
        }
        
        if (list.length>0) {
            this.main.feedback.say({
                title: "Info",
                message: "Added these properties to class " + c.l + ": " + list.join(", ")
            });
        } else {
            this.main.feedback.say({
                title: "Info",
                message: "All of these properties are already defined for this class."
            });
        }
        
        
        
    },
    

    exploreeditoritemHandler: function () {
        //explore the item currently active in the editor.
        var tabpanel = this.view.down("tabpanel[name='bravotabs']");
        tabpanel.setActiveTab(1);
        this.explore(this.main.data.getParent(this.view.getEditor()));
        this.view.down("button[name='editortreebutton']").setDisabled(true);
    },
    
    trashHandler: function () {
        //Get items involved in the move.
        var src = this.main.data.getObject(this.view.getEditor());
        var dest = this.main.data.getObject(this.environment.get("DEFAULT_RECYCLEBIN"));
        
        //Invalidate cache so that items must be refreshed from server.
        this.main.cache.invalidate(this.main.data.getObject(src.pa));  //needs to be the parent of the source
        this.main.cache.invalidate(dest); //the destination has already been adjusted to be the parent.
        
        //Update the item.
        this.view.fireEvent('cmd',{
            cmd: "put",
            objects: [src],
            pa: this.environment.get("DEFAULT_TOOLBOX")
        });
        
        //Refresh the view.
        Ext.defer(function() {
            this.refresh('explorer');
        }, 100, this);
    },
    
    
    //---------------------------------------------------------------------------------------
    // Favorites
    //---------------------------------------------------------------------------------------
    
    setupFavorites: function () {
        var tabpanel = this.view.down("tabpanel[name='bravotabs']");
        var cmp = this.view.down("container[name='bravofavorites']");
        if (cmp) tabpanel.remove(cmp);
        
        var tpl = new Ext.XTemplate(
            '<tpl for=".">',
                '<div class="draggable">',
                    '<tpl if="iconCls">',
                        '<span class="fa fa-{iconCls} draggable_icon"></span>',
                    '<tpl else>',
                        '<span class="fa fa-cube draggable_icon"></span>',
                    '</tpl>',
                    '<p>',
                        '<tpl if="plural">',
                            '{plural}',
                        '<tpl else>',
                            '{l}',
                        '</tpl>',
                    '</p>',
                '</div>',
            '</tpl>'
        );
        
        
            /*'<tpl for=".">',
                '<div class="draggable" style="float:left;width:100px;height:100px;padding:2px;margin:2px;text-align:center;padding-top:15px;overflow:hidden;">',
                    '<span class="fa fa-{iconCls}" style="font-size:1.5em;font-family:FontAwesome;text-decoration:none;color:#666;padding:5px;-webkit-box-shadow: 2px 2px 3px 2px #ccc; -moz-box-shadow: 2px 2px 3px 2px #ccc; box-shadow: 2px 2px 3px 2px #ccc;"></span>',
                    '<p>{l}</p>',
                '</div>',
            '</tpl>'*/
        
        
        var bravofavorites = Ext.create('Ext.Container',{
            name: 'bravofavorites',
            iconCls: 'fa fa-heart',
            layout: 'fit',
            items: [
                {
                    xtype: 'dataview',
                    name: 'bravofavoriteschooser',
                    itemSelector : 'div.draggable',
                    scrollable: true,
                    store: {},
                    flex: 1,
                    tpl : tpl
                }
            ]
        });
        
        tabpanel.add(bravofavorites);
        
    },
    
    favorites: function (user) {
        this.view.fireEvent('cmd',{
            cmd: "request",
            a: user.a,
            relationship: "favorites",
            callback: this.loadFavoritesCallback,
            scope: this
        });
    },
    
    explorefavoriteitemHandler: function (cmp,rec,el,idx,ev) {
    
        //User clicks on an item. Load a grid full of the first page of data from this item.
        this.view.setExplorePage(1);
        this.view.down("tabpanel[name='bravotabs']").getLayout().setActiveItem(this.view.down("panel[name='bravotree']"));
        
        if (cmp.lastevent!='itempress') {
            cmp.mask("Loading..");
            this.explore({a: rec.get("a")});
        }
        cmp.lastevent = 'itemclick';
    },
    
    
    choosefavoriteitemHandler: function (cmp,rec,el,idx,ev) {
    
        //User clicks on a class. Load a grid full of the first page of data from this class.
        if (cmp.lastevent!='itempress') {
            if (this.view.getEditMode()=='single') this.edit({a:rec.get("a")});
        }
        cmp.lastevent = 'itemclick';
    },
    

    pressfavoriteitemHandler: function (cmp,rec,el,idx,ev) {
        /*
        cmp.lastevent = 'itempress';
        if (this.view.getEditMode()=='single') {
            this.view.setEditMode('multi');
            this.buildConnector();
            this.connect(rec.data);
        } else {
            this.view.setEditMode('single');
            this.edit(rec.get("a"));
        }
        */
    },

    
    loadFavoritesCallback: function (rsp) {
        var me = (rsp.scope||this);
        
        var arr = rsp.desc;
        
        var cmp = me.view.down("dataview[name='bravofavoriteschooser']");
        cmp.store.removeAll();
        cmp.store.loadData(arr);

    },
    
    addFavoritesHandler: function() {
    
        var user = this.main.entity.getUser();
        
        var src = this.main.data.getObject(this.view.getEditor());
        var dest = this.main.data.getObject(user.a);
        
        var sp = {
            callback: this.favoritesCallback,
            component: this,
            scope: this
        };
        this.main.data.createLink({
            parent: dest.dp,
            child: src.dp,
            t: 7,
            z: 0
        },sp);
        
    },
    
    favoritesCallback: function (rsp) {
        var me = (rsp.scope||this);
        
        //Refresh the view.
        Ext.defer(function() {
            me.refresh('favorites');
        }, 100, me);
        
        //Feedback to user.
        me.main.feedback.say({
            title: me.view.getEditor().l,
            message: 'Added to favorites.'
        });
        
        
    },

    removeFavoritesHandler: function () {
        var user = this.main.entity.getUser();
        var src = this.main.data.getObject(this.view.getEditor().a);
    
        //Update the item.
        this.view.fireEvent('cmd',{
            cmd: "put",
            objects: [src],
            pa: this.environment.get("DEFAULT_RECYCLEBIN")
        });
        
        //Refresh the view.
        Ext.defer(function() {
            this.refresh('favorites');
        }, 100, this);
        
        //Feedback to user.
        this.main.feedback.say({
            title: this.view.getEditor().l,
            message: 'Removed from favorites.'
        });
        
        
    },
    
    getFavoriteObject: function (o) {
        //Favorites are a link. Need to lookup the link using the reference from the object.
        //You can favorite an item, but you are only favoriting the actual object not the specific link.
        for (var item in this.main.entity.favorites) {
            if (this.main.entity.favorites[item].g==o.g) return this.main.entity.favorites[item];
        }
    },
    
    //propertiesgridremoveHandler: function (grid, rowIndex, colIndex,cmp,ev) {
        //grid.getStore().removeAt(rowIndex);
    //},
        
    //---------------------------------------------------------------------------------------
    // Search
    //---------------------------------------------------------------------------------------
    
    search: function() {
        var title = this.view.down("tbtext[name='bravosearchtitle']");
        title.setHtml((this.view.getBrowse().plural||this.view.getBrowse().l)+' Search');
        
        var arr = [{property:'',operator:'',value:''}];
        var store = new Ext.data.Store({
            data: arr
        });
        var cols = this.buildSearchColumns();
        
        
        var panel = this.view.down("panel[name='bravobrowsersearchpanel']");
        panel.removeAll();
        
        var grid = Ext.create('Ext.grid.Panel',{
            xtype: 'grid',
            name: 'bravosearchgrid',
            hideHeaders: false,
            markDirty: false,
            scrollable: true,
            plugins: {
                ptype: 'cellediting',
                clicksToEdit: 1
            },
            viewConfig: {
                markDirty: false
            },
            store: store,
            columns: cols
        });
        panel.add(grid);
    },
    
    buildSearchColumns: function () {
        
        //Properties
        var store = new Ext.data.Store({
            data: this.view.getBrowseProperties()
        });
        
        var operators = new Ext.data.Store({
            data: [
                {text:'contains',value:'contains'},
                {text:'starts with',value:'startswith'},
                {text:'is empty',value:'isempty'},
                {text:'is not',value:'isnot'}//,
                //{text:'is between',value:'is between'},
                //{text:'is greater than',value:'is greater than'},
                //{text:'is less than',value:'is less than'}
            ]
        });
        
        var cols = [];
        cols.push({
            text: 'Property',
            dataIndex: 'property',
            width: '35%',
            editor: {
                xtype: 'combobox',
                store: store,
                queryMode: 'local',
                displayField: 'l',
                valueField: 'l',
                editable: true
            },
            editable: true,
            hideable: false
        });
        cols.push({
            text: '',
            dataIndex: 'operator',
            width: '25%',
            editor: {
                xtype: 'combobox',
                store: operators,
                queryMode: 'local',
                displayField: 'text',
                valueField: 'text'
            },
            editable: true,
            hideable: false
        });
        cols.push({
            text: 'Value',
            dataIndex: 'value',
            width: '35%',
            editor: {
                xtype: 'textfield'
            },
            editable: true,
            hideable: false
        });
        cols.push({
            xtype:'actioncolumn',
            hideable: false,
            sortable: false,
            width: '5%',
            align: 'center',
            items: [
                {
                    xtype: 'button',
                    cls: 'removebutton',
                    iconCls: 'fa fa-minus-circle',
                    handler: 'searchgridremoveHandler'
                }
            ]
        });
        return cols;
    },
    
    browsesearcheditHandler: function (grid,td,colIndex,rec,tr,rowIndex,ev,eOpts) {
        var total = grid.getStore().getCount();
        if (rowIndex==total-1) {
            //add a new line
            grid.getStore().add({property:'',operator:'',value:''});
        }
    },
    
    searchgridremoveHandler: function (grid, rowIndex, colIndex,cmp,ev) {
        grid.getStore().removeAt(rowIndex);
    },
    
    bravosearchHandler: function () {
        //Perform the actual search.
        var store = this.view.down("grid[name='bravosearchgrid']").getStore();
        var sp = {};
        var operators = {
            contains: '',
            startswith: '^',
            isempty: 'null',
            isnot: '!'//,
            //isbetween: '~',
            //isgreaterthan: '>',
            //islessthan: '<'
        };
        var properties = this.view.getBrowseProperties();
        store.each(function (rec) {
            var p = '';
            //lookup property name from object list.
            for (var i=0,l=properties.length;i<l;i++) {
                if (properties[i].l==rec.get("property")) {
                    p = '$' + properties[i].n;
                    break;
                }
            }
            //or just assume user typed in exact property name.
            if (!p) p = '$' + rec.get("property");
            
            var v = rec.get("value");
            var op = rec.get("operator")||"contains";
            op = op.replace(/ /g, '');
            var op = operators[op];
            if (op=='null') v = '';
            if (p&&v) sp[p] = op + v;
        },this);
        
        //save search parameters.
        this.view.setBrowseOptions(sp);
        
        var panel = this.view.down("panel[name='bravobrowsersearchpanel']");
        panel.mask("Loading...");
        
        //insert search code here.
        //always start searches with page 1
        var top = this.main.data.getRoleDomain(this.main.environment.get("PROJECT"));
        this.view.fireEvent('cmd',Ext.apply({
            cmd: "request",
            a: top.a,
            cid: this.view.getBrowse().n,
            relationship: "descendants",
            callback: this.loadClassCallback,
            page: 1,
            pagesize: this.view.getBrowsePageSize(),
            scope: this
        },this.view.getBrowseOptions()));
        
    },
    
    bravosearchcancelHandler: function () {
        //Go back to results page without searching.
        this.view.down("container[name='bravobrowser']").getLayout().setActiveItem(1);
    }
    
    
    
});