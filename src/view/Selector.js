Ext.define('bravo.Selector', {
    extend : 'Ext.view.View',
    alias: 'widget.selector',

    requires : [
        'Ext.ux.DataView.Draggable'
    ],

    mixins: {
        draggable: 'Ext.ux.DataView.Draggable'
    },
    
    uses: ['Ext.view.DragZone','Ext.view.DropZone'],

    itemSelector : 'div.draggable',
    
    scrollable: true,
    
    ddReorder: true,
    
    multiSelect: true,
    
    //data-qtip="..."

    tpl : [
    
        '<tpl for=".">',
            '<div class="draggable">',
                '<tpl if="iconCls">',
                    '<span class="fa fa-{iconCls} draggable_icon"></span>',
                '<tpl else>',
                    '<span class="fa fa-cube draggable_icon"></span>',
                '</tpl>',
                '<p>',
                    '<tpl if="t!=0">',
                        '<i>',
                    '</tpl>',
                    '{l}',
                    '<tpl if="t!=0">',
                        '</i>',
                    '</tpl>',
                '</p>',
            '</div>',
        '</tpl>'
    ],
           
        /*
        '<tpl for=".">',
            '<div class="draggable" style="float:left;width:100px;height:100px;padding:2px;margin:2px;text-align:center;padding-top:15px;overflow:hidden;">',
                '<span class="fa fa-{iconCls}" style="font-size:1.5em;font-family:FontAwesome;text-decoration:none;color:#666;padding:5px;-webkit-box-shadow: 2px 2px 3px 2px #ccc; -moz-box-shadow: 2px 2px 3px 2px #ccc; box-shadow: 2px 2px 3px 2px #ccc;"></span>',
                '<p>',
                '<tpl if="t!=0"><i></tpl>',
                '{l}',
                '<tpl if="t!=0"></i></tpl>',
                '</p>',
            '</div>',
        '</tpl>'
        */

    
    store: {},
    
    initComponent: function() {

        this.mixins.draggable.init(this, {
            ddConfig: {
                ddGroup: this.ddGroup||this.id
            }
        });
        
        this.enableBubble(['droprecord']);
        

        this.callParent(arguments);
    },
    
    afterRender: function(){
        var me = this;
        
        me.callParent();
           
        if (me.ddReorder && !me.dragGroup && !me.dropGroup){
            me.dragGroup = me.dropGroup = this.ddGroup
        }
 
        if (me.draggable || me.dragGroup){
            me.dragZone = Ext.create('Ext.view.DragZone', {
                view: me,
                ddGroup: me.dragGroup
            });
        }
        if (me.droppable || me.dropGroup){
            me.dropZone = Ext.create('Ext.view.DropZone', {
                view: me,
                ddGroup: me.dropGroup,
                handleNodeDrop: function(data, dropRecord, position) {
                    
                    var source = {
                        component: data.view,
                        data: data.records[0]
                    }
                    var event = data.event;
                    var destination = {
                        component: this.view,
                        data: dropRecord,
                        idx: this.view.store.indexOf(dropRecord),
                        position: position
                    }
                    
                    
                    //Send custom event.
                    me.fireEvent('droprecord',this,source,destination,event);
                        
                    //if (source.component.name==destination.component.name)

                    /* Change the local component if needed.
                    
                    // remove the Models from the source Store
                    data.view.store.remove(records);

                    index = store.indexOf(dropRecord);
                    if (position === 'after') {
                        index++;
                    }
                    store.insert(index, records);
                    view.getSelectionModel().select(records);
                    me.fireEvent('drop', me, records);
                    */
                    
                    
                    //var srcStore = source.component.store;
                    //srcStore.remove(source.data);
                    //index = dest.idx;
                    
                    //var destStore = destination.component.store;
                    //destStore.insert(index, records);
                    //view.getSelectionModel().select(records);
                    //me.fireEvent('drop', me, records);
                    
                }
            });
        }  
    },
    
    onDestroy: function(){
        var me = this;
        me.bindStore(null);
        me.dragZone.proxy.destroy();
        me.dragZone.proxy = null;
        Ext.destroy(me.dragZone,me.dropZone);
        me.callParent();
    }
    
});

Ext.ClassManager.addNameAliasMappings({
    'bravo.Selector': ['widget.selector']
});