/**
 *  Dashboard
 *  Contains all view structure for the dashboard.
 */
Ext.define('bravo.Dashboard', {
    extend: 'Ext.window.Window',
    controller: 'dashboard',
    alias: 'widget.xdashboard',
    xtype: 'xdashboard',
    
    requires: [
        'Ext.window.Window',
        'Ext.Component',
        'Ext.form.Panel',
        'Ext.form.FieldSet',
        'Ext.form.field.Text',
        'Ext.form.field.Display',
        'Ext.form.field.Date',
        'Ext.form.field.Radio',
        'Ext.form.field.Checkbox',
        'Ext.form.field.TextArea',
        'Ext.grid.Panel'
    ],
    
    title: 'Dashboard',
    
    iconCls: 'fa fa-wrench',

    floatable: true,
    
    layout: 'border',
    
    config: {
        top: '', //The "top" of this user's environment.  Could be top for admin users or project for other users.
        browse: '',  //active class being browsed
        explore: '', //active item in the tree.
        editor: '', //active item in the editor.
        connect: '', //active item in the connector
        classes: '',
        browseProperties: '',
        browseOptions: '',
        exploreProperties: '',
        browseItems: 0,
        exploreItems: 0,
        connectItems: 0,
        activeTab: '', //the user's current explore/browse/favorite view
        activePropertiesTab: '', //the user's current properties view
        browsePage: 1,
        browsePageSize: 50,
        explorePage: 1,
        explorePageSize: 50,
        connectPage: 1,
        connectPageSize: 50,
        dirty: false,
        edits: '',
        connectSource: '',
        connectDestination: '',
        editMode: 'single',
        role: 2
    },
    
    bodyBorder: false,
    
    defaults: {
        collapsible: true,
        split: true,
        bodyPadding: 5
    },
    
    items: [
        {
            xtype: 'panel',
            title: 'Bravo Content Management System',
            name: 'leftpanel',
            collapsible: false,
            collapsed: false,
            headerPosition: 'left',
            region: 'center',
            iconCls: '',
            layout: {
                type: 'fit'
            },
            items: [
                {
                    xtype: 'panel',
                    layout: 'center',
                    items: [
                        {
                            xtype: 'form',
                            title: 'Login',
                            hidden: true,
                            name: 'loginform',
                            frame: true,
                            width: 350,
                            items: [
                                {
                                    margin: 10,
                                    xtype: 'panel',
                                    items: [
                                        {
                                            name: 'usernametext',
                                            fieldLabel: 'username',
                                            xtype: 'textfield',
                                            width: '100%'
                                        },
                                        {
                                            name: 'passwordtext',
                                            fieldLabel: 'password',
                                            xtype: 'textfield',
                                            inputType: 'password',
                                            width: '100%'
                                        },
                                        {
                                            xtype: 'button',
                                            text: 'Login',
                                            name: 'loginbutton',
                                            width: '100%'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'panel',
            title: '',
            name: 'rightpanel',
            collapsible: true,
            collapsed: false,
            scrollable: true,
            hidden: true,
            headerPosition: 'left',
            width: '45%',
            region: 'east',
            iconCls: 'fa fa-pencil',
            layout: 'fit',
            items: [
            
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    name: 'bravoeditortoolbar',
                    dock: 'bottom',
                    overflowHandler: 'menu',
                    flex: 1,
                    hidden: true,
                    items: [
                        {
                            xtype: 'button',
                            iconCls: 'fa fa-heart-o',
                            name: 'editorfavoritesbutton',
                            tooltip: 'Add to Favorites'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'fa fa-refresh',
                            name: 'editorrefreshbutton',
                            tooltip: 'Refresh'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'fa fa-wrench',
                            name: 'editorwrenchbutton',
                            tooltip: 'Learn properties'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'fa fa-sitemap',
                            name: 'editortreebutton',
                            tooltip: 'Show in Explorer'
                        },
                        '-',
                        {
                            xtype: 'tbtext',
                            flex: 1,
                            textAlign: 'center',
                            name: 'editorstatustext'
                        },
                        '-',
                        {
                            xtype: 'button',
                            iconCls: 'fa fa-trash-o',
                            name: 'editortrashbutton',
                            tooltip: 'Move to Recycle Bin'
                        }
                    ]
                }
            ]
        }
    ]

});

Ext.ClassManager.addNameAliasMappings({
    'bravo.Dashboard': ['widget.xdashboard']
});