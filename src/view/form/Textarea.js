/**
 *  Textfield:  Basic input for text.
 
 Options for maskRe for forcing a certain pattern:
 
    "maskRe":"^[0-9]*$"   -- Only allows numbers to be typed.
 
 Options for vtypes for showing invalid value in field.
 
    "vtype": "email" -- Email address format
    "vtype": "alpha" -- only alpha
    "vtype": "alphanum" -- only alpha and numbers
    "vtype": "url" -- must be in url format.
 
 
 */
Ext.define('bravo.form.Textarea', {
    extend: 'Ext.form.field.TextArea',
    
    alias: 'xtextarea',
    
    xtype: 'xtextarea',
    
    constructor: function(config) {
        config.fieldLabel = config.title||config.l;
        config.allowBlank = !config.required;
        if (config.maskRe) config.maskRe = new RegExp(config.maskRe);
    	this.callParent(arguments);
    },
    
    initComponent: function () {

        this.enableBubble('cmd');
        
        Ext.defer(function() {
            
            //Automatic rule: Get the provider context for this component.
            this.fireEvent('cmd', {
                cmd:'provider',
                component: this
            });
            
            //Automatic rule: Set up the required context for this component.
            this.fireEvent('cmd', {
                cmd:'require',
                component: this
            });
            
            //Listen to all custom rules assigned to this textfield.
            this.fireEvent('cmd', {
                cmd:'listen',
                component: this
            });
            
        }, 100, this);
        
        this.callParent(arguments);

    },
    
    onRemove: function () {
        this.fireEvent('cmd', {
            cmd:'ignore',
            component: this
        });
    }

});

Ext.ClassManager.addNameAliasMappings({
    'bravo.form.Textarea': ['xtextarea']
});