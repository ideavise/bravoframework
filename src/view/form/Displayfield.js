/**
 *
 */
Ext.define('bravo.form.Displayfield', {
    extend: 'Ext.form.field.Display',
    
    alias: 'xdisplayfield',
    
    xtype: 'xdisplayfield',
    
    constructor: function(config) {
        this.fieldLabel = config.title||config.l;
    	this.callParent(arguments);
    },
    
    initComponent: function () {

        this.enableBubble('cmd');
        
        Ext.defer(function() {
        
            //Listen to all custom rules assigned to this textfield.
            this.fireEvent('cmd', {
                cmd:'listen',
                component: this
            });
            
            //Automatic rule: Get the provider context for this component.
            this.fireEvent('cmd', {
                cmd:'provider',
                component: this
            });
            
        }, 100, this);
        
        this.callParent(arguments);

    },
    
    onRemove: function () {
        this.fireEvent('cmd', {
            cmd:'ignore',
            component: this
        });
    }

});

Ext.ClassManager.addNameAliasMappings({
    'bravo.form.Displayfield': ['xdisplayfield']
});