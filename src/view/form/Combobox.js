/**
 * Combobox
 
 This component is used for data entry.
 
 Use ComboNavigator instead to use the same component as a selector that publishes an action.
 
 
     provider: put a topic name here.  It should match another container component with a provider name.  When an item is selected from this dataview, it will be marked as the active component with this provider name.  All components inside the container component
    will send their data to whatever dataprovider is active for this topic. 
    
    action: this action will be performed and the active item selected will be passed as an argument to this action.  For example, request would request whatever item is active.
     
    alias: the alias source of the data for this list.
        or
    source: the name source of the data for this list.
        or
    vals/captions: comma-delimited source of data for this list.
        or
    names/labels: comma-delimited source of data for this list.
        or
    aliases/labels: comma-delimited source of data for this list.
     
    $property:  add properties to filter the results by a certain value e.g. $cid, $n, $LastName, etc.
     
    relationship: e.g. children, descendants, ancestors, etc.
    
 */
Ext.define('bravo.form.Combobox', {
    extend: 'Ext.form.field.ComboBox',
    
    alias: 'xcombobox',
    
    xtype: 'xcombobox',
    
    constructor: function(config) {
    
        Ext.apply(config,{
            fieldLabel: (config.title||config.l),
            valueField: (config.valueField||'a'),
            displayField: (config.displayField||'l'),
            queryMode: 'local'
        });
        
        this.callParent(arguments);
    },
    
    initComponent: function () {

        this.enableBubble('cmd');
        
        this.store = new Ext.data.Store({});
        
        //Set this component to busy until data loads.
        Ext.defer(function() {
            
            //Automatic rule: Get the provider context for this component.
            this.fireEvent('cmd', {
                cmd:'provider',
                component: this
            });
            
            //Set up all rules for this component.
            this.fireEvent('cmd', {
                cmd:'listen',
                component: this
            });
            
            //Load the data for this component.
            this.loadData();
            
            
        }, 100, this);
        
        this.callParent(arguments);
        
    },

    loadData: function () {
        this.fireEvent('cmd',{
            cmd: 'setbusy',
            component: this
        });
           
        this.fireEvent('cmd',{
            cmd: 'loaddata',
            component: this,
            callback: this.fill
        });
    },
    
    fill: function (rsp) {
    
        var me = (rsp.scope||this);
        
        if (rsp.desc.length==0) {
            me.fireEvent('cmd',{
                cmd: 'say',
                title: 'Info',
                message: 'No data found for ' + me.l
            });
        
        } else {
            me.store.loadData(rsp.desc);
        }
    
        me.fireEvent('cmd',{
            cmd: 'removebusy',
            component: this
        });
    },
    
    onRemove: function () {
        this.fireEvent('cmd', {
            cmd:'ignore',
            component: this
        });
    }

});

Ext.ClassManager.addNameAliasMappings({
    'bravo.form.Combobox': ['xcombobox']
});