/**
 *
 */
Ext.define('bravo.form.Fieldset', {
    extend: 'Ext.form.FieldSet',
    
    alias: 'xfieldset',
    
    xtype: 'xfieldset',
    
    constructor: function(config) {
        this.callParent(arguments);
    },
    
    initComponent: function () {

        this.enableBubble('cmd');

        Ext.defer(function() {
            this.fireEvent('cmd', {
                cmd:'listen',
                component: this
            });
        }, 100, this);
        
        this.callParent(arguments);

    },
    
    onRemove: function () {
        this.fireEvent('cmd', {
            cmd:'ignore',
            component: this
        });
    }

});

Ext.ClassManager.addNameAliasMappings({
    'bravo.form.Fieldset': ['xfieldset']
});