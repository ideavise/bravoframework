/**
 *
 */
Ext.define('bravo.form.Select', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'xselect'
});

Ext.ClassManager.addNameAliasMappings({
    'bravo.form.Select': ['xselect']
});