/**
 *
 */
Ext.define('bravo.form.Radio', {
    extend: 'Ext.form.field.Radio',
    
    alias: 'xradio',
    
    xtype: 'xradio',
    
    constructor: function(config) {
        this.fieldLabel = config.title||config.l;
    	this.callParent(arguments);
    },
    
    initComponent: function () {

        this.enableBubble('cmd');
        
        Ext.defer(function() {
            this.fireEvent('cmd', {
                cmd:'listen',
                component: this
            });
            
            this.fireEvent('cmd', {
                cmd:'provider',
                component: this
            });
        }, 100, this);
        
        this.callParent(arguments);

    },
    
    onRemove: function () {
        this.fireEvent('cmd', {
            cmd:'ignore',
            component: this
        });
    }

});

Ext.ClassManager.addNameAliasMappings({
    'bravo.form.Radio': ['xradio']
});