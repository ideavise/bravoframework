/**
 *
 */
Ext.define('bravo.form.Timefield', {
    extend: 'Ext.form.field.Time',
    
    alias: 'xtimefield',
    
    xtype: 'xtimefield',
    
    constructor: function(config) {
        config.fieldLabel = config.title||config.l;
        config.increment = config.increment||1;
    	this.callParent(arguments);
    },
    
    initComponent: function () {

        this.enableBubble('cmd');
        
        Ext.defer(function() {
            
            this.fireEvent('cmd', {
                cmd:'provider',
                component: this
            });
            
            this.fireEvent('cmd', {
                cmd:'listen',
                component: this
            });
        }, 100, this);
        
        this.callParent(arguments);

    },
    
    onRemove: function () {
        this.fireEvent('cmd', {
            cmd:'ignore',
            component: this
        });
    }

});

Ext.ClassManager.addNameAliasMappings({
    'bravo.form.Timefield': ['xtimefield']
});