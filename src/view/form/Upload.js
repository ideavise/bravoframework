/**
bravo.form.Upload
Extends Ext.Button


//Must assign a custom action that will actually create the object and manage the upload.

Example:

//Must create a custom action that will actually create the object.

 *
 */
Ext.define('bravo.form.Upload', {
    extend: 'Ext.form.field.File',
    
    alias: 'widget.xupload',
    
    xtype: 'xupload',

    constructor: function(c) {
        var showtitle = c.showtitle || "true";
    	if (showtitle.toString()!=="false") c.text = c.l;
    	this.callParent(arguments);
    },

    initComponent: function () {
        this.enableBubble(['upload','cmd']);
        this.callParent(arguments);
    },

    onFileChange: function(cmp,ev,fn) {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            var afile = (ev.dataTransfer) ? ev.dataTransfer.files[0] : ev.target.files[0];
            if (afile) {
                var reader = new FileReader();
                var cb = Ext.bind(this.uploadHandler,this,[afile],true);
                reader.onload = function(ev) {
                    cb(ev.target.result);
                };
                reader.readAsDataURL(afile);
            }
        } else {
            alert('Uploading files is not supported in this browser.');
        }
    },

    uploadHandler: function (b64,afile) {
        
        var sp = Ext.applyIf(afile,{data:b64});
        
        //Fire an event to any listeners.
        this.fireEvent('upload',this,sp);
        
        //If there is a custom action assigned to this button perform it.
        if (this.action) {
            this.fireEvent('cmd', Ext.applyIf(sp,{cmd:this.action,cmp:this}));
        }
    }
        
});

Ext.ClassManager.addNameAliasMappings({
    'bravo.form.Upload': ['widget.xupload']
});