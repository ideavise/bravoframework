Ext.define('bravo.Main', {
	extend: 'Ext.Container',
	xtype: 'bravo',

	layout: {
		type: 'fit'
	},
    
    html: '',

	controller: 'bravo-main',

	listeners: {
		"cmd" : function(sp) {
			this.getController().actionHandler(sp);
		}
	}
    
    /* 
    
    The content gets loaded dynamically here using DEFAULT_WINDOW config and loads that item from the
    database.
    
    */
    
});